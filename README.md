# Test Driven Development pada Pengembangan Aplikasi AMIKOS
Flask-Admin-Ext with Flask-Security-Too (for user management & security), Flask-Mail (for sending email), Flask-Babel (for internationalize), Bootstrap-Flask (for template), Flask-SQLAlchemy (for database ORM) with pytest, flake8, tox configured.

## Mengapa harus menulis test ?
Secara umum, pengujian membantu memastikan bahwa aplikasi Anda akan berfungsi seperti yang diharapkan untuk pengguna akhir _(end user)_.

Proyek perangkat lunak dengan cakupan tes yang tinggi _(high coverage)_ tidak menjamin bahwasannya perangkat lunak sempurna, akan tetapi dapat menjadi indikator awal yang baik dari kualitas perangkat lunak tersebut. Selain itu, kode yang dapat diuji umumnya merupakan tanda arsitektur perangkat lunak yang baik, itulah sebabnya _advance developer_ mempertimbangkan pengujian di seluruh siklus pengembangan.

Tes dapat dipertimbangkan pada tiga tingkatan:
- _Unit_
- _Functional_ (atau _integration_)
- _End-to-end_

### _Unit Test_
Menguji fungsionalitas unit kode individual yang diisolasi dari dependensinya. Mereka adalah garis pertahanan pertama terhadap kesalahan dan inkonsistensi dalam basis kode. Mereka menguji dari dalam ke luar _(inside out)_, dari sudut pandang programmer.

### _Functional Test_
Menguji beberapa komponen produk perangkat lunak untuk memastikan komponen bekerja sama dengan benar. Biasanya, tes ini difokuskan pada fungsionalitas yang akan digunakan pengguna akhir. Mereka menguji dari luar ke dalam _(outside in)_, dari sudut pandang pengguna akhir.

Kedua pengujian _Unit_ dan _Functional_ adalah bagian mendasar dari proses _Test Driven Development_ (TDD).

## Pengujian meningkatkan pemeliharaan kode

Pemeliharaan mengacu pada membuat perbaikan bug atau peningkatan pada kode Anda atau pengembang lain yang perlu memperbarui kode Anda di beberapa titik di masa mendatang.

Pengujian harus dikombinasikan dengan proses Continuous Integration (CI) untuk memastikan bahwa pengujian Anda terus-menerus dijalankan, idealnya pada setiap _commit_ ke repositori. Serangkaian pengujian yang solid dapat menjadi penting untuk menemukan _bug_ dengan cepat dan di awal proses pengembangan, sebelum pengguna akhir menemukannya dalam mode _production_.

## Apa yang harus di uji ?
_Unit Test_ harus fokus pada pengujian unit kecil kode secara terpisah. Misalnya, dalam aplikasi Flask, kita dapat menggunakan pengujian unit untuk menguji:
- Model basis data
- Fungsi utilitas yang dipanggil oleh fungsi tampilan

_Functional Test_ harus fokus pada bagaimana fungsi tampilan beroperasi. Sebagai contoh:
- Kondisi nominal (GET, POST, dll.) untuk fungsi tampilan
- Metode HTTP yang tidak valid ditangani dengan benar untuk fungsi tampilan
- Data yang tidak valid diteruskan ke fungsi tampilan

Fokus pada skenario pengujian yang akan berinteraksi dengan pengguna akhir. Pengalaman yang dimiliki pengguna produk Anda adalah yang terpenting!

## Dengan Apa Saya Melakukan Test ?
Pytest adalah kerangka kerja pengujian untuk Python yang digunakan untuk menulis, mengatur, dan menjalankan kasus uji. Setelah menyiapkan struktur pengujian dasar Anda, pytest membuatnya sangat mudah untuk menulis pengujian dan memberikan banyak fleksibilitas untuk menjalankan pengujian. pytest memenuhi aspek kunci dari lingkungan pengujian yang baik:

- tes itu menyenangkan untuk ditulis
- tes dapat ditulis dengan cepat dengan menggunakan fungsi pembantu (perlengkapan)
- tes dapat dijalankan dengan satu perintah
- tes berjalan cepat

## Proses Pengujian
Seluruh kasus uji akan ditempatkan didalam folder tests yang berada di direktori root dan dipisahkan berdasarkan unit dan functional.

```
└── tests
    ├── conftest.py
    ├── functional
    │   ├── __init__.py
    │   ├── test_stocks.py
    │   └── test_users.py
    └── unit
        ├── __init__.py
        └── test_models.py
```

## Contoh Unit Test


## Contoh Functional Test

## Jalankan Test
Jalankan uji dengan perintah `pytest` pada direktori root projek

## Code Coverage
Saat mengembangkan tes, ada baiknya untuk memahami seberapa banyak kode sumber yang benar-benar diuji. Konsep ini dikenal sebagai _code coverage_.

## Kesimpulan