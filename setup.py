"""Installation script for flask-admin application."""
from pathlib import Path
from setuptools import setup, find_packages

DESCRIPTION = ""
APP_ROOT = Path(__file__).parent
README = (APP_ROOT / "README.md").read_text()
AUTHOR = "David Rigan"
AUTHOR_EMAIL = "15957000+dvrg@users.noreply.github.com"
PROJECT_URLS = {
    "Documentation": "https://gitlab.com/dvrg/flask-amikos/-/wikis",
    "Bug Tracker": "https://gitlab.com/dvrg/flask-amikos/-/issues",
    "Source Code": "https://gitlab.com/dvrg/flask-amikos",
}
INSTALL_REQUIRES = [
    "MarkupSafe==2.0.1",
    "Flask==1.1.4",
    "Flask-Security-Too",
    "Flask-Mail",
    "Flask-SQLAlchemy",
    "sqlalchemy==1.3.23",
    "Flask-Migrate==2.7.0",
    "Flask-Babel",
    "Flask-Moment",
    "Flask-Menu",
    "Flask-Breadcrumbs",
    "Flask-DebugToolbar",
    "Flask-WTF",
    "WTForms-SQLAlchemy",
    "python-dotenv",
    "bcrypt",
    "zxcvbn",
    "pyqrcode",
    "phonenumbers",
    "sms",
    "twilio",
    "cryptography",
    "celery[redis]",
    "elasticsearch>=7.0.0,<8.0.0",
    "midtransclient",
    "sentry-sdk[flask]",
    "flask_profiler",
    "flask-talisman",
    "Flask-APScheduler",
    "psycopg2",
]
EXTRAS_REQUIRE = {
    "dev": [
        "black",
        "flake8",
        "pre-commit",
        "pydocstyle",
        "pytest",
        "pytest-black",
        "pytest-clarity",
        "pytest-dotenv",
        "pytest-flake8",
        "pytest-flask",
        "pytest-xdist[psutil]",
        "pytest-cov",
        "tox",
        "faker",
    ]
}
setup(
    name="flask-amikos",
    description=DESCRIPTION,
    long_description=README,
    long_description_content_type="text/markdown",
    version="0.1",
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    maintainer=AUTHOR,
    maintainer_email=AUTHOR_EMAIL,
    license="MIT",
    url="https://gitlab.com/dvrg/flask-amikos",
    project_urls=PROJECT_URLS,
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    python_requires=">=3.7",
    install_requires=INSTALL_REQUIRES,
    extras_require=EXTRAS_REQUIRE,
)
