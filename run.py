"""Flask CLI/Application  entry point."""
import os
import click
from datetime import datetime, timedelta
from flask import g
from flask_babel import get_locale
from flask_migrate import upgrade
from src.flask_amikos import create_app, db, user_datastore
from src.flask_amikos.models.user import User
from src.flask_amikos.models.role import Role
from src.flask_amikos.models.ads import Ads
from src.flask_amikos.models.ads_package import AdsPackages
from src.flask_amikos.models.amenities import Amenities
from src.flask_amikos.models.location import Location
from src.flask_amikos.models.transaction import Transaction
from src.flask_amikos.util.util import day_to_second, remove_unused_char

app = create_app(os.getenv("FLASK_ENV"))


@app.shell_context_processor
def shell():
    """register models so we can call from shell"""

    return {
        "db": db,
        "ud": user_datastore,
        "user": User,
        "role": Role,
        "ads": Ads,
        "ads_packages": AdsPackages,
        "amenities": Amenities,
        "location": Location,
        "transaction": Transaction,
    }


@app.before_request
def before_request():
    """store local information to g object, and call in moment.js"""
    g.locale = str(get_locale())


@app.before_first_request
def role():
    """Create default role"""

    db.create_all()
    user_datastore.find_or_create_role(
        name="Admin",
        description="Admin role for application",
        permissions={"role-read", "role-write", "user-read", "user-write"},
    )
    user_datastore.find_or_create_role(
        name="Editor",
        description="Editor role for application",
        permissions={"role-read", "user-read"},
    )
    user_datastore.find_or_create_role(
        name="SuperUser", description="SuperUser role for application"
    )
    db.session.commit()
    ads_packages = AdsPackages(
        package_name="Basic",
        price=10000,
        discount_price=5000,
        duration=day_to_second(30),
        description="Tampilkan iklan saya di halaman beranda &amp; social media <img src='https://github.githubassets.com/images/icons/emoji/unicode/1f388.png?v8'>",
        created_at=datetime.now(),
    )
    ads_packages.create_ads_packages()

    amenities = Amenities(
        amenities_name="Kamar Mandi Dalam <img src='https://github.githubassets.com/images/icons/emoji/unicode/1f388.png?v8'>",
        description="Fasilitas kamar mandi dalam",
        status=False,
        created_at=datetime.now(),
        updated_at=None,
    )
    amenities.create_amenities()

    location = Location(
        location_name="Universitas Amikom Yogyakarta",
        status=False,
        created_at=datetime.now(),
        updated_at=None,
    )
    location.create_location()

    ads = Ads(
        property_type="indekos",
        brand_name="hanjaya putra",
        gender_of_occupant="he",
        number_bedroom_available=3,
        type_of_bathroom="kmd",
        number_of_bathroom=3,
        address="jl. selorejo no.3, condong catur, sleman",
        rental_price="550000",
        duration_of_ease="d1",
        mobile_number=remove_unused_char("+628-9896-6494-4"),
        whatsapp_number=remove_unused_char("+628-9896-6494-4"),
        additional_info="dilarang membawa hewan peliharaan, jam malam pukul 23:00 wib",
        email="fsdmncgiibeuyr@logicstreak.com",
        feedback="semoga lebih baik kedepannya",
        accept_policy=True,
        status=False,
        created_at=datetime.now(),
        updated_at=None,
        user_id=1,
    )
    ads.remaining_time = ads_packages.duration
    ads.start_date = ads.created_at
    ads.end_date = ads.start_date + timedelta(seconds=ads.remaining_time)
    ads.ads_cost = ads_packages.discount_price
    ads.location_id = location.id
    ads.create_ads()
    ads.amenities.append(amenities)
    ads.ads_packages.append(ads_packages)
    db.session.commit()

    transaction = Transaction(
        ads_id=ads.id,
        payment_method="credit_card",
        transaction_status="created",
        created_at=datetime.now(),
        updated_at=None,
    )
    transaction.create_transaction()


@app.cli.command()
def deploy():
    """Run deployment tasks."""
    upgrade()


@app.cli.group()
def translate():
    """Translation and localization command"""
    pass


@translate.command()
def update():
    """Update all languages."""
    if os.system("pybabel extract -F babel.cfg -k _l -o messages.pot ."):
        raise RuntimeError("extract command failed")
    if os.system("pybabel update -i messages.pot -d src/flask_amikos/translations"):
        raise RuntimeError("update command failed")
    os.remove("messages.pot")


@translate.command()
def compile():
    """Compile all languages."""
    if os.system("pybabel compile -d src/flask_amikos/translations"):
        raise RuntimeError("compile command failed")


@translate.command()
@click.argument("lang")
def init(lang):
    """Initialize a new language."""
    if os.system("pybabel extract -F babel.cfg -k _l -o messages.pot ."):
        raise RuntimeError("extract command failed")
    if os.system(
        "pybabel init -i messages.pot -d src/flask_amikos/translations -l " + lang
    ):
        raise RuntimeError("init command failed")
    os.remove("messages.pot")


@app.cli.command()
@click.option(
    "--length", default=25, help="Number of function to include in the profiler report."
)
@click.option(
    "--profile-dir", default=None, help="Directory where profiler data file are saved."
)
def profiler(length, profile_dir):
    """Start the application under the code profiler."""
    from werkzeug.middleware.profiler import ProfilerMiddleware

    ProfilerMiddleware(
        app.wsgi_app,
        restrictions=[length],
        profile_dir=profile_dir,
    )


if __name__ == "__main__":
    app.run(debug=os.getenv("FLASK_DEBUG"))
