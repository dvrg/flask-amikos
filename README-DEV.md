### Step-by-step documentation when build Flask-Admin

#### Setup workspace
1. Create virtualenv using pyenv
```bash
$ pyenv virtualenv 3.8.0 flask-admin-3.8.0
```
2. Activate virtualenv
```bash
$ pyenv shell flask-admin-3.8.0
```
3. Upgrade outdate default pip packages including pip itself, setuptools and wheel
```bash
$ pip install --upgrade pip setuptools wheel
```
4. Add `.gitignore` for Python from [here](https://raw.githubusercontent.com/github/gitignore/master/Python.gitignore)
5. Create `.env` file with default value is :
```bash
FLASK_APP=run.py
FLASK_ENV=development
SECRET_KEY="this is not secret"
```
note: for generate SECRET_KEY consider using `os.urandom(24)` python function

6. Create Black rules configuration with file name `pyproject.toml`
7. Create Black pre-commit git-hooks, so all file will be autoformat using Black before commit to git. File `pre-commit-config-yaml`
8. Create pytest configuration with `pytest.ini` file
9. Create tox configuration with `tox.ini`
10. Create instalation script with `setup.py`
11. Install application depedencies
```bash
$ pip install -e .[dev]  
```
12. After install all application dependencies, install pre-commit
```bash
$ pre-commit install
```

#### Running Localhost mail
```bash
$ python -m smtpd -n -c DebuggingServer localhost:8025
```

#### View celery worker
```bash
$ celery --app flask_amikos.util.tasks.celery worker
```