from http import HTTPStatus
from datetime import datetime
from flask import url_for
from src.flask_amikos import csrf
from tests.utils import (
    ADS_PROPERTY_TYPE,
    ADS_BRAND_NAME,
    ADS_GENDER_OF_OCCUPANT,
    ADS_NUMBER_BEDROOM_AVAILABLE,
    ADS_TYPE_OF_BATHROOM,
    ADS_NUMBER_OF_BATHROOM,
    ADS_ADDRESS,
    ADS_RENTAL_PRICE,
    ADS_DURATION_OF_EASE,
    ADS_MOBILE_NUMBER,
    ADS_WHATSAPP_NUMBER,
    ADS_ADDITIONAL_INFO,
    ADS_EMAIL,
    ADS_FEEDBACK,
    ADS_ACCEPT_POLICY,
    ADS_STATUS,
    EMAIL,
    PASSWORD,
    create_ads,
    create_fake_user,
    set_current_user,
)


def test_home_page(client, db, ads):
    rv = client.get(url_for("front.home_page"))
    assert rv.status_code == HTTPStatus.OK
    assert (
        b"Daftarkan rumah kos ataupun kontrakan secara GRATIS selama masa pandemi"
        in rv.data
    )
    assert b"Postingan Terbaru" in rv.data
    assert not f"{ADS_BRAND_NAME}".encode() in rv.data

    # set ads to True, so the ads will show in home page
    ads.status = True
    db.session.commit()

    rv = client.get(url_for("front.home_page"))
    assert rv.status_code == HTTPStatus.OK
    assert f"{ADS_BRAND_NAME}".encode() in rv.data

    rv = client.post(url_for("front.home_page"))
    assert rv.status_code == HTTPStatus.METHOD_NOT_ALLOWED
    assert (
        b"Daftarkan rumah kos ataupun kontrakan secara GRATIS selama masa pandemi"
        not in rv.data
    )
    assert b"Postingan Terbaru" not in rv.data


def test_search_page(client, db, ads):
    search_query = ADS_BRAND_NAME.replace(" ", "+")

    rv = client.get(url_for("front.home_ads_search", query=search_query))
    assert rv.status_code == HTTPStatus.OK
    assert b"Postingan Terbaru" in rv.data

    # because ads status is False, so we cannot find data
    assert not f"{ADS_BRAND_NAME}".encode() in rv.data

    # set ads to True, so the ads will show in home page
    ads.status = True
    db.session.commit()

    rv = client.get(url_for("front.home_ads_search", query=search_query))
    assert rv.status_code == HTTPStatus.OK

    assert f"{ADS_BRAND_NAME}".encode() in rv.data


def test_ads_page(app, client, user):
    set_current_user(app, user)

    rv = client.get(url_for("front.home_ads"), follow_redirects=True)
    assert rv.status_code == HTTPStatus.OK
    assert rv.headers.get("Cookie") == "session"
    """
    assert b"Login Pengguna" in rv.data
    assert b"pasang iklan" in rv.data
    assert b"Basic Information" in rv.data
    assert b"Amenities Information" in rv.data
    
        rv = create_ads(
        client,
        ADS_PROPERTY_TYPE,
        ADS_BRAND_NAME,
        ADS_GENDER_OF_OCCUPANT,
        ADS_NUMBER_BEDROOM_AVAILABLE,
        ADS_TYPE_OF_BATHROOM,
        ADS_NUMBER_OF_BATHROOM,
        location.id,
        ADS_ADDRESS,
        ADS_RENTAL_PRICE,
        ADS_DURATION_OF_EASE,
        ADS_MOBILE_NUMBER,
        ADS_WHATSAPP_NUMBER,
        ADS_ADDITIONAL_INFO,
        ADS_EMAIL,
        ADS_FEEDBACK,
        ADS_ACCEPT_POLICY,
        ADS_STATUS,
        datetime.now(),
        None,
        ads_packages,
        amenities,
    )
    assert b"Operation has been success" in rv.data.data """


def test_ads_detail_page(client, ads):
    ads_id = ads.public_id
    rv = client.get(url_for("front.home_ads_detail", public_id=ads_id))
    assert rv.status_code == HTTPStatus.OK
    assert f"{ADS_BRAND_NAME}".encode() in rv.data

    # check similiar around content, because data is only 1 so no similiar around
    # data with same :location_id
    assert b"no data available." in rv.data


def test_transaction_page(client, transaction):
    transaction_id = transaction.public_id
    rv = client.get(url_for("front.home_transaction_detail", public_id=transaction_id))
    assert rv.status_code == HTTPStatus.OK
    # test here


@csrf.exempt
def test_payment_notification_page(client):
    pass
    # test here


@csrf.exempt
def test_payment_status(client):
    pass


def test_change_log_page(client):
    rv = client.get(url_for("front.home_change_log"))
    assert rv.status_code == HTTPStatus.OK
    assert b"Riwayat Pembaharuan" in rv.data
    rv = client.post(url_for("front.home_change_log"))
    assert rv.status_code == HTTPStatus.METHOD_NOT_ALLOWED
    assert b"Riwayat Pembaharuan" not in rv.data


def test_faq_page(client):
    rv = client.get(url_for("front.home_faq"))
    assert rv.status_code == HTTPStatus.OK
    assert b"Hal Yang Mungkin Akan Ditanyakan" in rv.data
    assert b"Berkaitan Dengan Pemilik Properti Indekos/Kontrakan" in rv.data
    rv = client.post(url_for("front.home_faq"))
    assert rv.status_code == HTTPStatus.METHOD_NOT_ALLOWED
    assert b"Hal Yang Mungkin Akan Ditanyakan" not in rv.data
    assert b"Berkaitan Dengan Pemilik Properti Indekos/Kontrakan" not in rv.data


def test_about_page(client):
    rv = client.get(url_for("front.home_about"))
    assert rv.status_code == HTTPStatus.OK
    assert b"Tentang Amikos" in rv.data
    rv = client.post(url_for("front.home_about"))
    assert rv.status_code == HTTPStatus.METHOD_NOT_ALLOWED
    assert b"Tentang Amikos" not in rv.data


def test_ads_policy_page(client):
    rv = client.get(url_for("front.home_ads_policy"))
    assert rv.status_code == HTTPStatus.OK
    assert b"Harga Iklan" in rv.data
    rv = client.post(url_for("front.home_ads_policy"))
    assert rv.status_code == HTTPStatus.METHOD_NOT_ALLOWED
    assert b"Kebijakan Privasi" not in rv.data


def test_tos_page(client):
    rv = client.get(url_for("front.home_tos"))
    assert rv.status_code == HTTPStatus.OK
    assert b"Ketentuan Penggunaan" in rv.data
    assert b"Diperbaharui pada <strong>01 Mei 2021</strong>" in rv.data
    rv = client.post(url_for("front.home_tos"))
    assert rv.status_code == HTTPStatus.METHOD_NOT_ALLOWED
    assert b"Ketentuan Penggunaan" not in rv.data
    assert b"Diperbaharui pada <strong>01 Mei 2021</strong>" not in rv.data


def test_privacy_policy_page(client):
    rv = client.get(url_for("front.home_privacy_policy"))
    assert rv.status_code == HTTPStatus.OK
    assert b"Kebijakan Privasi" in rv.data
    assert b"Diperbaharui pada <strong>01 Mei 2021</strong>" in rv.data
    rv = client.post(url_for("front.home_privacy_policy"))
    assert rv.status_code == HTTPStatus.METHOD_NOT_ALLOWED
    assert b"Kebijakan Privasi" not in rv.data
    assert b"Diperbaharui pada <strong>01 Mei 2021</strong>" not in rv.data
