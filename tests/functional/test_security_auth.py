from http import HTTPStatus
from flask.helpers import url_for

from flask_security.utils import url_for_security
from tests.utils import (
    security_register,
    security_login,
    security_logout,
    create_fake_user,
    set_current_user,
    EMAIL,
)

SUCCESS = "successfully registered"
EMAIL_ALREADY_EXISTS = f"{EMAIL} is already registered"


def test_security_login_page(client):
    """
    F-1.3 - Login User
    """
    # Test get page login first
    rv = client.get(url_for_security("login"), follow_redirects=True)
    assert rv.status_code == HTTPStatus.OK
    assert b"Login Pengguna" in rv.data

    # Test login process with username and password
    rv = security_login(client)
    assert rv.status_code == HTTPStatus.OK
    print(rv)
    assert rv.headers.get("Cookie") == "session"
    assert b"Login Pengguna" in rv.data

    # test here
    """ assert b"Iklankan Indekos & Kontrakan Secara Gratis! - AMIKOS" in rv.data
    assert b"Postingan Terbaru" in rv.data """


def test_security_logout_page(app, client, user):
    """
    F-1.4 - Logout User
    """
    # create fake login for current user
    set_current_user(app, user)
    # access home page
    rv = client.get(url_for("front.home_page"), follow_redirects=True)
    # makesure http status response is 200
    assert rv.status_code == HTTPStatus.OK
    # makesure http header for cokie is have value session
    assert rv.headers.get("Cookie") == "session"
    # access logout page
    rv = client.get(url_for_security("logout"), follow_redirects=True)
    # makesure http status response is 200
    assert rv.status_code == HTTPStatus.OK
    # makesure http cookie header is False
    assert not rv.headers.get("Cookie")
    # makesure response value data have spesified bytes string
    assert b"Login Pengguna" in rv.data


def test_security_register_page(client):
    # Test get page register first
    rv = client.get(url_for_security("register"), follow_redirects=True)
    assert rv.status_code == HTTPStatus.OK
    assert b"Daftar Pengguna" in rv.data

    # Test register process
    rv = security_register(client)
    assert rv.status_code == HTTPStatus.OK
    assert b"Daftar Pengguna" in rv.data
    # test here
    """ assert (
        b"Thank you. Confirmation instructions have been sent to user@mail.com."
        in rv.data
    ) """


""" 
def test_security_register_user_verify_token(client, user):
    response = security_register_user_verify_token(client, user)
    assert response.status_code == HTTPStatus.OK
    assert b"Thank you. Your email has been confirmed." in response.data
    assert b"Your email has already been confirmed." in response.data
 """
