import pytest
from datetime import datetime, time, timedelta
from http import HTTPStatus
from tests.utils import (
    ADS_PACKAGE_DISCOUNT_PRICE,
    ADS_PROPERTY_TYPE,
    ADS_BRAND_NAME,
    ADS_GENDER_OF_OCCUPANT,
    ADS_NUMBER_BEDROOM_AVAILABLE,
    ADS_TYPE_OF_BATHROOM,
    ADS_NUMBER_OF_BATHROOM,
    ADS_ADDRESS,
    ADS_RENTAL_PRICE,
    ADS_DURATION_OF_EASE,
    ADS_MOBILE_NUMBER,
    ADS_WHATSAPP_NUMBER,
    ADS_ADDITIONAL_INFO,
    ADS_EMAIL,
    ADS_FEEDBACK,
    ADS_ACCEPT_POLICY,
    ADS_STATUS,
    ADS_PACKAGE_DURATION,
)
from src.flask_amikos.models.ads import Ads
from src.flask_amikos.util.util import remove_unused_char, day_to_second

"""
View Ads
Create Ads
Update Ads
Find Ads
Detail Ads
"""


def test_model_ads_get_all(ads, user):
    """
    F-2.1 - View Ads
    GIVEN ads and user model
    WHEN ads and user created
    THEN view ads from spesified owner from user
    """
    # check ads status and makesure status is False
    assert not ads.status
    # get ads data when status is False
    ads_false = Ads.get_all(status=False, page=1)
    assert ads_false.items
    # change status ads
    assert Ads.activate_ads(ads)
    # makesure ads status is True
    assert ads.status
    # get ads data when status is True
    ads_true = Ads.get_all(status=True, page=1)
    assert ads_true.items
    # get ads data from spesific user when status is True
    ads_with_status_and_user = Ads.get_all(status=True, user=user, page=1)
    assert ads_with_status_and_user.items


def test_model_ads(ads, location, ads_packages, amenities):
    """
    F-2.2 - Create Ads
    GIVEN ads model
    WHEN ads created
    THEN makesure ads data is True
    """
    # makesure all data same with define parameter
    assert ads.property_type.name == ADS_PROPERTY_TYPE
    assert ads.brand_name == ADS_BRAND_NAME
    assert ads.gender_of_occupant.name == ADS_GENDER_OF_OCCUPANT
    assert ads.number_bedroom_available == ADS_NUMBER_BEDROOM_AVAILABLE
    assert ads.type_of_bathroom.name == ADS_TYPE_OF_BATHROOM
    assert ads.number_of_bathroom == ADS_NUMBER_OF_BATHROOM
    assert ads.location_id == location.id
    assert ads.address == ADS_ADDRESS
    assert ads.rental_price == ADS_RENTAL_PRICE
    assert ads.duration_of_ease.name == ADS_DURATION_OF_EASE
    assert ads.mobile_number == remove_unused_char(ADS_MOBILE_NUMBER)
    assert ads.whatsapp_number == remove_unused_char(ADS_WHATSAPP_NUMBER)
    assert ads.additional_info == ADS_ADDITIONAL_INFO
    assert ads.email == ADS_EMAIL
    assert ads.feedback == ADS_FEEDBACK
    assert ads.accept_policy == ADS_ACCEPT_POLICY
    assert ads.status == ADS_STATUS
    assert ads.created_at
    assert ads.updated_at is None
    assert ads.amenities == [amenities]
    assert ads.ads_packages == [ads_packages]
    assert ads.remaining_time == day_to_second(ADS_PACKAGE_DURATION)
    assert ads.ads_cost == ADS_PACKAGE_DISCOUNT_PRICE


def test_model_ads_update(ads):
    """
    F-2.3 - Update Ads
    GIVEN ads model
    WHEN ads created
    THEN update ads data
    """
    # makesure ads is False dan ads update_at is False
    assert not ads.status
    assert not ads.updated_at
    # change ads status to True
    assert Ads.activate_ads(ads)
    # makesure ads status is True
    assert ads.status
    # change ads status to False
    assert Ads.deactivate_ads(ads)
    # makesure ads staus is False and ads update_at is True
    assert not ads.status
    assert ads.updated_at


def test_model_ads_find(ads):
    """
    F-2.4 - Find Ads
    GIVEN ads model
    WHEN ads created
    THEN find ads data
    """
    # find data by public_id
    assert Ads.find_by_public_id(public_id=ads.public_id)
    # find data by brand name
    assert Ads.find_by_brand_name(brand_name=ADS_BRAND_NAME, status=ADS_STATUS)
    # find data by email
    assert Ads.find_by_email(email=ADS_EMAIL, status=ADS_STATUS)
    # fibd data by mobile number
    assert Ads.find_by_mobile_number(
        mobile_number=remove_unused_char(ADS_MOBILE_NUMBER), status=ADS_STATUS
    )
    # find data by whatsapp number
    assert Ads.find_by_whatsapp_number(
        whatsapp_number=remove_unused_char(ADS_WHATSAPP_NUMBER), status=ADS_STATUS
    )
    # find data property type
    assert Ads.find_by_property_type(property_type=ads.property_type, status=ADS_STATUS)
    # find data by gender type
    assert Ads.find_by_gender_of_occupant(
        gender_of_occupant=ads.gender_of_occupant, status=ADS_STATUS
    )
    # find data by type of bathroom
    assert Ads.find_by_type_of_bathroom(
        type_of_bathroom=ads.type_of_bathroom, status=ADS_STATUS
    )
    # find by location
    assert Ads.find_by_location_close_to(
        location_id=ads.location_id, ignore_id=ads.public_id, status=ADS_STATUS
    )
    # find by duration of ease
    assert Ads.find_by_duration_of_ease(
        duration_of_ease=ads.duration_of_ease, status=ADS_STATUS
    )


def test_model_ads_delete_ads(ads):
    """
    GIVEN ads model
    WHEN ads created
    THEN delete ads data
    """
    data = Ads.find_by_public_id(public_id=ads.public_id)
    assert data
    data.delete_ads()
    assert Ads.find_by_public_id(public_id=data.public_id) == HTTPStatus.NOT_FOUND


def test_model_ads_count_ads(ads):
    """
    GIVEN ads model
    WHEN ads created
    THEN count total of ads
    """
    assert Ads.find_by_brand_name(brand_name=ADS_BRAND_NAME, status=ADS_STATUS)
    assert not ads.status
    assert Ads.count() == 1
    assert Ads.count(status=False) == 1
    assert Ads.count(status=True) == 0


def test_model_ads_subtraction_ads_remaining_time(db, ads):
    """
    BERIKAN data :ads dengan kriteria :remaining_time != 0
    KETIKA fungsi :job_subtraction_ads_remaining_time() dijalankan]
    MAKA :remaining_time dan :updated_at berubah
    """
    assert ads.updated_at is None
    remaining_time = ads.remaining_time
    ads.status = True
    db.session.commit()
    Ads.job_subtraction_ads_remaining_time()
    assert ads.updated_at is not None
    assert ads.remaining_time != remaining_time
    assert ads.status
    # set :end_date == datetime.now() sehingga remaining_time.days <= 0
    ads.end_date = datetime.now()
    db.session.commit()
    Ads.job_subtraction_ads_remaining_time()
    assert not ads.status


def test_model_ads_job_change_status(db, ads):
    """
    BERIKAN data :ads dengan kriteria :status True & :end_date < hari ini
    KETIKA fungsi :job_change_status() dijalankan
    MAKA status :ads menjadi False
    """
    assert ads.updated_at is None
    ads.status = True
    ads.end_date = datetime.now() - timedelta(days=1)
    db.session.commit()
    Ads.job_change_status()
    assert not ads.status
    assert ads.updated_at is not None
