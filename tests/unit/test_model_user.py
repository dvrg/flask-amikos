from flask_security import verify_password
from src.flask_amikos import user_datastore
from tests.utils import EMAIL, ANOTHER_EMAIL, PASSWORD, FS_UNIQUIFIER, ROLE_ADMIN
from src.flask_amikos.models.user import User

"""
Create User
Find User
Login User
Logout User
View User
Update User
"""


def test_model_user(user):
    """
    F-1.1 - Create User
    GIVEN a User model
    WHEN a new User is created
    THEN check email, password, fs_uniquifier, active, and role are defined correctly.
    """
    # check user exist after created
    assert user.email == EMAIL
    assert user.fs_uniquifier == FS_UNIQUIFIER
    assert user.roles == []
    assert user.is_active
    assert user.is_authenticated
    assert user.get_id() == FS_UNIQUIFIER
    assert not user.is_anonymous
    assert not user.has_role(ROLE_ADMIN)
    assert verify_password(PASSWORD, user.password)
    assert user_datastore.deactivate_user(user)
    assert user_datastore.find_user(email=EMAIL)


def test_model_user_find(user):
    """
    F-.1.2 - Find User
    GIVEN a User model
    WHEN a new User is created
    THEN find user by email is must Found
    """
    # check with user exist
    assert user_datastore.find_user(email=EMAIL)
    # check with user not exist
    assert not user_datastore.find_user(email=ANOTHER_EMAIL)


def test_model_user_get_all(user):
    """
    F-1.5 - View User
    GIVEN a User model
    WHEN a new User is created
    THEN get all user data filter by status user is active
    """
    # validate user status is true and get data
    assert user.is_active
    user_active = User.get_all(active=True, page=1)
    assert user_active.items
    # using user status is false and data is empty
    user_not_active = User.get_all(active=False, page=1)
    assert not user_not_active.items


def test_model_user_update(user, db):
    """
    F-1.6 - Update User
    GIVEN a User model
    WHEN a new User is created
    THEN change information about user and validate the changes.
    """
    # validate user information before change
    assert user.email == EMAIL
    assert user.is_active
    # change user information
    user.email = ANOTHER_EMAIL
    db.session.commit()
    # validate the changes
    assert user.email == ANOTHER_EMAIL


def test_model_user_delete(user):
    """
    GIVEN a User model
    WHEN a new User is created
    THEN delete User and find user must be False
    """
    assert user_datastore.find_user(email=EMAIL)
    user_datastore.delete_user(user)
    assert not user_datastore.find_user(email=EMAIL)


def test_model_user_add_role(user, role):
    """
    GIVEN a User and Role model
    WHEN a new User is created
    THEN add role to user and check role user must be same with ROLE_ADMIN
    """
    assert user_datastore.find_user(email=EMAIL)
    assert user_datastore.add_role_to_user(user, role)
    assert user.has_role(ROLE_ADMIN)


def test_model_user_count(user):
    """
    GIVEN a User model
    WHEN a new User is created
    THEN find user by email, makesure user status is active and count user with status active
    """
    assert user_datastore.find_user(email=EMAIL)
    assert user.is_active
    assert User.count() == 1
    assert User.count(active=True) == 1
