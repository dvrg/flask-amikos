from flask import current_app
from tests.utils import EMAIL, MAIL_SUBJECT, MAIL_BODY


def test_email(email):
    assert len(email) == 1
    assert email[0].subject == MAIL_SUBJECT
    assert email[0].body == MAIL_BODY
    assert email[0].recipients == [EMAIL]
    assert email[0].sender == current_app.config["MAIL_DEFAULT_SENDER"]
