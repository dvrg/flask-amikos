from src.flask_amikos.models.amenities import Amenities
from tests.utils import AMENITIES_NAME, AMENITIES_DESCRIPTION


def test_model_amenities(amenities):
    assert amenities.amenities_name == AMENITIES_NAME
    assert amenities.description == AMENITIES_DESCRIPTION
    assert amenities.created_at
    assert amenities.updated_at is None
    assert not amenities.status


def test_model_amenities_find_data(amenities):
    assert Amenities.find_by_amenities_name(amenities_name=AMENITIES_NAME)
    assert Amenities.find_by_public_id(public_id=amenities.public_id)


def test_model_amenities_get_all_amenities(amenities):
    assert not amenities.status
    amenities_false = Amenities.get_all(status=False, page=1)
    assert amenities_false.items
    assert Amenities.activate_amenities(amenities)
    assert amenities.status
    amenities_true = Amenities.get_all(status=True, page=1)
    assert amenities_true.items


def test_model_amenities_change_status_amenities(amenities):
    assert not amenities.status
    assert not amenities.updated_at
    assert Amenities.activate_amenities(amenities)
    assert amenities.status
    assert Amenities.deactivate_amenities(amenities)
    assert not amenities.status
    assert amenities.updated_at


def test_model_amenities_delete_amenities(amenities):
    data = Amenities.find_by_public_id(public_id=amenities.public_id)
    assert data
    data.delete_amenities()
    assert not Amenities.find_by_public_id(public_id=data.public_id)


def test_model_amenities_count_amenities(amenities):
    assert Amenities.find_by_amenities_name(amenities_name=AMENITIES_NAME)
    assert not amenities.status
    assert Amenities.count() == 1
    assert Amenities.count(status=False) == 1
    assert Amenities.count(status=True) == 0
