from src.flask_amikos.models.ads_package import AdsPackages
from src.flask_amikos.util.util import day_to_second
from tests.utils import (
    ADS_PACKAGE_DESCRIPTION,
    ADS_PACKAGE_DISCOUNT_PRICE,
    ADS_PACKAGE_NAME,
    ADS_PACKAGE_PRICE,
    ADS_PACKAGE_DURATION,
)


def test_model_ads_packages(ads_packages):
    assert ads_packages.package_name == ADS_PACKAGE_NAME
    assert ads_packages.price == ADS_PACKAGE_PRICE
    assert ads_packages.discount_price == ADS_PACKAGE_DISCOUNT_PRICE
    assert ads_packages.description == ADS_PACKAGE_DESCRIPTION
    assert ads_packages.duration == day_to_second(ADS_PACKAGE_DURATION)
    assert ads_packages.created_at
    assert ads_packages.updated_at is None
    assert not ads_packages.status


def test_model_ads_packages_find_data(ads_packages):
    assert AdsPackages.find_by_package_name(package_name=ADS_PACKAGE_NAME)
    assert ads_packages.find_or_create()
    assert AdsPackages.find_by_public_id(public_id=ads_packages.public_id)
    # assert not ads_packages.find_or_create(package_name="Standart")


def test_model_ads_pacakges_get_all_packages(ads_packages):
    assert not ads_packages.status
    ads_pacakges_false = AdsPackages.get_all(status=False, page=1)
    assert ads_pacakges_false.items
    assert AdsPackages.activate_ads_package(ads_packages)
    assert ads_packages.status
    ads_pacakges_true = AdsPackages.get_all(status=True, page=1)
    assert ads_pacakges_true.items


def test_model_ads_pacakges_change_status_packages(ads_packages):
    assert not ads_packages.status
    assert not ads_packages.updated_at
    assert AdsPackages.activate_ads_package(ads_packages)
    assert ads_packages.status
    assert AdsPackages.deactivate_ads_package(ads_packages)
    assert not ads_packages.status
    assert ads_packages.updated_at


def test_model_ads_pacakges_delete_ads_pacakge(ads_packages):
    data = AdsPackages.find_by_public_id(public_id=ads_packages.public_id)
    assert data
    data.delete_ads_packages()
    assert not AdsPackages.find_by_public_id(public_id=data.public_id)


def test_model_ads_packages_count_ads_package(ads_packages):
    assert AdsPackages.find_by_package_name(package_name=ADS_PACKAGE_NAME)
    assert not ads_packages.status
    assert AdsPackages.count() == 1
    assert AdsPackages.count(status=False) == 1
    assert AdsPackages.count(status=True) == 0
