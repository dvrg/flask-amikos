from src.flask_amikos.models.transaction import (
    Transaction,
    StatusTransaction,
    PaymentMethod,
)
from tests.utils import TRANSACTION_PAYMENT_METHOD, TRANSACTION_STATUS

"""
View Transaction
Create Transaction
Update Transaction
Find Transaction
Detail Transaction
"""


def test_model_transaction_get_all(transaction, user):
    """
    F-3.1 - View Transaction
    GIVEN transaction and user model
    WHEN transaction with spesified user created
    THEN get all data and filter transaction data by spesified user
    """
    # makesure transaction status is same as define before
    assert transaction.transaction_status.name == TRANSACTION_STATUS
    # get data transaction with spesific transaction status
    transaction_created = Transaction.get_all(
        transaction_status=[TRANSACTION_STATUS], page=1
    )
    assert transaction_created.items
    # get data transaction without status
    transaction_all = Transaction.get_all(page=1)
    assert transaction_all.items
    # get data transaction with spesific user owner
    transaction_with_related_user_ads = Transaction.get_all(
        transaction_status=[TRANSACTION_STATUS], user=user, page=1
    )
    assert transaction_with_related_user_ads.items


def test_model_transaction(transaction, ads):
    """
    F-3.2 - Create Transaction
    GIVEN transaction and ads model
    WHEN transaction created
    THEN makesure data is correct
    """
    # makesure all data same as define before
    assert transaction.ads_id == ads.id
    assert transaction.payment_method.name == TRANSACTION_PAYMENT_METHOD
    assert transaction.transaction_status.name == TRANSACTION_STATUS
    assert transaction.created_at
    assert transaction.updated_at is None


def test_model_transaction_update(transaction):
    """
    F-3.3 - Update Transaction
    GIVEn transaction model
    WHEN transaction created
    THEN update transaction status and payment type
    """
    # update transaction_status
    assert transaction.transaction_status.name == TRANSACTION_STATUS
    for status in StatusTransaction.get():
        if TRANSACTION_STATUS in status.name:
            assert transaction.update_transaction_status(
                transaction_status=TRANSACTION_STATUS
            )

    # update payment_type
    assert transaction.payment_method.name == TRANSACTION_PAYMENT_METHOD
    for payment in PaymentMethod.get():
        if TRANSACTION_PAYMENT_METHOD in payment.name:
            assert transaction.update_payment_method(
                payment_type=TRANSACTION_PAYMENT_METHOD
            )


def test_model_transaction_find(transaction, ads):
    """
    F-3.4 - Find Transaction
    GIVEN transaction and ads
    WHEN transaction is created
    THEN find data
    """
    # find by public id
    assert Transaction.find_by_public_id(public_id=transaction.public_id)
    # find by ads id
    assert Transaction.find_by_ads_id(ads_id=ads.id)
    # fin dby transaction status
    transaction_status = Transaction.find_by_transaction_status(
        transaction_status=TRANSACTION_STATUS, page=1
    )
    assert transaction_status.items


def test_model_transaction_get_all_transaction_created(transaction):
    assert transaction.transaction_status
    transaction_created = Transaction.get_all(
        transaction_status=TRANSACTION_STATUS, page=1
    )
    assert transaction_created.items


def test_model_transaction_delete_transaction(transaction):
    data = Transaction.find_by_public_id(public_id=transaction.public_id)
    assert data
    data.delete_transaction()
    assert not Transaction.find_by_public_id(public_id=transaction.public_id)


def test_model_transaction_count_transaction(transaction):
    data = Transaction.find_by_public_id(public_id=transaction.public_id)
    assert data
    assert data.transaction_status.name == TRANSACTION_STATUS
    assert Transaction.count() == 1
    assert Transaction.count(transaction_status=TRANSACTION_STATUS) == 1
    assert Transaction.count(transaction_status="settlement") == 0
    assert Transaction.count(transaction_status="pending") == 0
    assert Transaction.count(transaction_status="expire") == 0
    assert Transaction.count(transaction_status="cancel") == 0
    assert Transaction.count(transaction_status="deny") == 0
