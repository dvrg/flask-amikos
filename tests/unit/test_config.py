"""Unit tests for environtment config settings."""
import os
import pkg_resources
import pytest
from datetime import datetime
from flask_security import uia_phone_mapper, uia_email_mapper
from src.flask_amikos import create_app
from src.flask_amikos.config import SQLITE_DEV, SQLITE_TEST, get_config


def test_config(config):
    assert config["SECURITY_BLUEPRINT_NAME"] == "security"
    assert config["SECURITY_PASSWORD_COMPLEXITY_CHECKER"] == "zxcvbn"
    assert config["SECURITY_USER_IDENTITY_ATTRIBUTES"] == [
        {"email": {"mapper": uia_email_mapper, "case_insensitive": True}},
        {"us_phone_number": {"mapper": uia_phone_mapper}},
    ]
    assert config["SECURITY_DATETIME_FACTORY"] == datetime.now
    assert config["SECURITY_CONFIRM_SALT"] == os.getenv("SECURITY_CONFIRM_SALT")
    assert config["SECURITY_RESET_SALT"] == os.getenv("SECURITY_RESET_SALT")
    assert config["SECURITY_LOGIN_SALT"] == os.getenv("SECURITY_LOGIN_SALT")
    assert config["SECURITY_TWO_FACTOR_VALIDITY_SALT"] == os.getenv(
        "SECURITY_TWO_FACTOR_VALIDITY_SALT"
    )
    assert config["SECURITY_US_SETUP_SALT"] == os.getenv("SECURITY_US_SETUP_SALT")
    assert config["SECURITY_REGISTERABLE"]
    assert config["SECURITY_POST_REGISTER_VIEW"] == "/login"
    assert config["SECURITY_CONFIRMABLE"]
    assert not config["SECURITY_AUTO_LOGIN_AFTER_CONFIRM"]
    assert config["SECURITY_CHANGEABLE"]
    assert config["SECURITY_POST_CHANGE_VIEW"] == "/logout"
    assert config["SECURITY_RECOVERABLE"]
    assert config["SECURITY_POST_RESET_VIEW"] == "/"
    assert config["SECURITY_TWO_FACTOR"]
    assert config["SECURITY_TWO_FACTOR_ENABLED_METHODS"] == [
        "email",
        "authenticator",
        "sms",
    ]
    assert config["SECURITY_TOTP_ISSUER"] == config["APP_NAME"]
    assert config["SECURITY_PHONE_REGION_DEFAULT"] == "ID"
    assert config["SECURITY_UNIFIED_SIGNIN"]
    assert config["SECURITY_US_ENABLED_METHODS"] == [
        "password",
        "email",
        "authenticator",
        "sms",
    ]
    assert config["SECURITY_PASSWORDLESS"]
    assert config["SECURITY_TRACKABLE"]
    assert config["SECURITY_POST_LOGOUT_VIEW"] == "/login"
    assert config["SECURITY_I18N_DOMAIN"] == "flask_security"
    assert config["SECURITY_I18N_DIRNAME"] == [
        pkg_resources.resource_filename("flask_security", "translations"),
        "/src/flask_amikos/translations",
    ]
    assert config["POST_PER_PAGE"] == 10
    assert config["MAX_PER_PAGE"] == 10
    assert config["SQLALCHEMY_RECORD_QUERIES"]
    assert config["SLOW_DB_QUERY_TIME"] == 0.5
    assert config["BABEL_DEFAULT_LOCALE"] == "id"
    assert config["BABEL_DEFAULT_TIMEZONE"] == "UTC"
    assert config["LANGUAGES"] == ["id", "en"]
    assert config["RECAPTCHA_PARAMETERS"] == {"hl": "id"}
    assert config["FLASK_PROFILER_USERNAME"] == os.getenv("FLASK_PROFILER_USERNAME")
    assert config["FLASK_PROFILER_PASSWORD"] == os.getenv("FLASK_PROFILER_PASSWORD")
    assert config["FLASK_PROFILER_ENDPOINT"] == os.getenv("FLASK_PROFILER_ENDPOINT")


def test_config_testing(config):
    config.from_object(get_config("testing"))
    assert config["PROFILE"]
    assert config["TESTING"]
    assert not config["WTF_CSRF_ENABLED"]
    assert not config["WTF_CSRF_CHECK_DEFAULT"]
    assert config["SECRET_KEY"] == os.getenv("SECRET_KEY")
    assert config["SECURITY_PASSWORD_SALT"] == os.getenv("SECURITY_PASSWORD_SALT")
    assert config["RECAPTCHA_PUBLIC_KEY"] == os.getenv("RECAPTCHA_PUBLIC_KEY")
    assert config["RECAPTCHA_PRIVATE_KEY"] == os.getenv("RECAPTCHA_PRIVATE_KEY")
    assert config["SECURITY_TOKEN_MAX_AGE"] == 5
    assert config["SECURITY_EMAIL_VALIDATOR_ARGS"] == {"check_deliverability": False}
    assert config["SECURITY_PASSWORD_HASH"] == "plaintext"
    assert config["SQLALCHEMY_DATABASE_URI"] == SQLITE_TEST
    assert config["LOGIN_DISABLED"]
    assert config["CELERY_RESULT_BACKEND"] == os.getenv("CELERY_RESULT_BACKEND")
    assert config["CELERY_BROKER_URL"] == os.getenv("CELERY_BROKER_URL")
    assert not config["DEBUG_TB_ENABLED"]
    assert config["MAIL_SERVER"] == "localhost"
    assert config["MAIL_PORT"] == 8025
    assert not config["MAIL_USE_TLS"]
    assert not config["MAIL_USE_SSL"]
    assert config["MAIL_USERNAME"] == os.getenv("MAIL_USERNAME")
    assert config["MAIL_PASSWORD"] == os.getenv("MAIL_PASSWORD")
    assert config["MAIL_DEFAULT_SENDER"] == "no-reply@localhost.com"
    assert config["SECURITY_SMS_SERVICE"] == "Twilio"
    assert config["SECURITY_SMS_SERVICE_CONFIG"] == {
        "ACCOUNT_SID": os.getenv("ACCOUNT_SID"),
        "AUTH_TOKEN": os.getenv("AUTH_TOKEN"),
        "PHONE_NUMBER": os.getenv("PHONE_NUMBER"),
    }
    assert not config["MIDTRANS_PRODUCTION"]
    assert config["MIDTRANS_SERVER_KEY"] == os.getenv("MIDTRANS_SERVER_KEY")
    assert config["MIDTRANS_CLIENT_KEY"] == os.getenv("MIDTRANS_CLIENT_KEY")
    assert config["ELASTICSEARCH_URL"] == os.getenv("ELASTICSEARCH_URL")
    assert config["SENTRY_DSN"] == os.getenv("SENTRY_DSN")
    assert config["TRACE_SAMPLE_RATE"] == 1


def test_config_development(config):
    config.from_object(get_config("development"))
    assert config["FLASK_ENV"] == "development"
    assert config["SECRET_KEY"] == os.getenv("SECRET_KEY")
    assert config["SECURITY_PASSWORD_SALT"] == os.getenv("SECURITY_PASSWORD_SALT")
    assert config["DEBUG_TB_PROFILER_ENABLED"]
    assert config["DEBUG_TB_TEMPLATE_EDITOR_ENABLED"]
    assert config["DEBUG_TB_INTERCEPT_REDIRECTS"]
    assert config["RECAPTCHA_PUBLIC_KEY"] == os.getenv("RECAPTCHA_PUBLIC_KEY")
    assert config["RECAPTCHA_PRIVATE_KEY"] == os.getenv("RECAPTCHA_PRIVATE_KEY")
    assert config["SECURITY_TOKEN_MAX_AGE"] == 900
    assert config["SQLALCHEMY_DATABASE_URI"] == os.getenv("DATABASE_URL", SQLITE_DEV)
    assert config["SQLALCHEMY_TRACK_MODIFICATIONS"]
    assert config["CELERY_RESULT_BACKEND"] == os.getenv("CELERY_RESULT_BACKEND")
    assert config["CELERY_BROKER_URL"] == os.getenv("CELERY_BROKER_URL")
    assert config["MAIL_SERVER"] == "localhost"
    assert config["MAIL_PORT"] == 8025
    assert config["MAIL_USERNAME"] == os.getenv("MAIL_USERNAME")
    assert config["MAIL_PASSWORD"] == os.getenv("MAIL_PASSWORD")
    assert config["MAIL_DEFAULT_SENDER"] == "no-reply@localhost.com"
    assert config["SECURITY_SMS_SERVICE"] == "Twilio"
    assert config["SECURITY_SMS_SERVICE_CONFIG"] == {
        "ACCOUNT_SID": os.getenv("ACCOUNT_SID"),
        "AUTH_TOKEN": os.getenv("AUTH_TOKEN"),
        "PHONE_NUMBER": os.getenv("PHONE_NUMBER"),
    }
    assert not config["MIDTRANS_PRODUCTION"]
    assert config["MIDTRANS_SERVER_KEY"] == os.getenv("MIDTRANS_SERVER_KEY")
    assert config["MIDTRANS_CLIENT_KEY"] == os.getenv("MIDTRANS_CLIENT_KEY")
    assert config["ELASTICSEARCH_URL"] == os.getenv("ELASTICSEARCH_URL")
    assert config["SENTRY_DSN"] == os.getenv("SENTRY_DSN")
    assert config["TRACE_SAMPLE_RATE"] == 1
    assert config["WTF_CSRF_SECRET_KEY"] == os.getenv("WTF_CSRF_SECRET_KEY")
    assert config["SECURITY_PASSWORD_HASH"] == "bcrypt"


def test_config_production(config):
    config.from_object(get_config("production"))
    assert config["FLASK_ENV"] == "production"
    assert config["SECRET_KEY"] == os.getenv("SECRET_KEY")
    assert config["SECURITY_PASSWORD_SALT"] == os.getenv("SECURITY_PASSWORD_SALT")
    assert config["RECAPTCHA_PUBLIC_KEY"] == os.getenv("RECAPTCHA_PUBLIC_KEY")
    assert config["RECAPTCHA_PRIVATE_KEY"] == os.getenv("RECAPTCHA_PRIVATE_KEY")
    assert config["SECURITY_TOKEN_MAX_AGE"] == 3600
    assert config["SQLALCHEMY_DATABASE_URI"] == os.getenv("DATABASE_URL")
    assert not config["PRESERVE_CONTEXT_ON_EXCEPTION"]
    assert not config["SQLALCHEMY_TRACK_MODIFICATIONS"]
    assert config["CELERY_RESULT_BACKEND"] == os.getenv("CELERY_RESULT_BACKEND")
    assert config["CELERY_BROKER_URL"] == os.getenv("CELERY_BROKER_URL")
    assert config["MAIL_SERVER"] == "smtp.sendgrid.net"
    assert config["MAIL_PORT"] == 587
    assert config["MAIL_USE_TLS"]
    assert not config["MAIL_USE_SSL"]
    assert config["MAIL_USERNAME"] == os.getenv("MAIL_USERNAME")
    assert config["MAIL_PASSWORD"] == os.getenv("MAIL_PASSWORD")
    assert config["MAIL_DEFAULT_SENDER"] == "no-reply@amikos.xyz"
    assert config["MAIL_ADMINISTRATOR"] == os.getenv("MAIL_ADMINISTRATOR")
    assert config["SECURITY_SMS_SERVICE"] == "Twilio"
    assert config["SECURITY_SMS_SERVICE_CONFIG"] == {
        "ACCOUNT_SID": os.getenv("ACCOUNT_SID"),
        "AUTH_TOKEN": os.getenv("AUTH_TOKEN"),
        "PHONE_NUMBER": os.getenv("PHONE_NUMBER"),
    }
    assert config["MIDTRANS_PRODUCTION"]
    assert config["MIDTRANS_SERVER_KEY"] == os.getenv("MIDTRANS_SERVER_KEY")
    assert config["MIDTRANS_CLIENT_KEY"] == os.getenv("MIDTRANS_CLIENT_KEY")
    assert config["ELASTICSEARCH_URL"] == os.getenv("ELASTICSEARCH_URL")
    assert config["SENTRY_DSN"] == os.getenv("SENTRY_DSN")
    assert config["TRACE_SAMPLE_RATE"] == 0.25
    assert config["WTF_CSRF_SECRET_KEY"] == os.getenv("WTF_CSRF_SECRET_KEY")
    assert config["SECURITY_PASSWORD_HASH"] == "bcrypt"
