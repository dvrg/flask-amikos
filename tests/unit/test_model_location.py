from src.flask_amikos.models.location import Location
from tests.utils import LOCATION_NAME


def test_model_location(location):
    assert location.location_name == LOCATION_NAME
    assert not location.status
    assert not location.updated_at


def test_model_location_find_data(location):
    assert Location.find_by_public_id(public_id=location.public_id)
    assert Location.find_by_location_name(location_name=LOCATION_NAME)


def test_model_location_get_all_location(location):
    assert not location.status
    location_false = Location.get_all(status=False, page=1)
    assert location_false.items
    assert Location.activate_location(location)
    assert location.status
    location_true = Location.get_all(status=True, page=1)
    assert location_true.items


def test_model_location_change_status_location(location):
    assert not location.status
    assert not location.updated_at
    assert Location.activate_location(location)
    assert location.status
    assert Location.deactivate_location(location)
    assert not location.status
    assert location.updated_at


def test_model_location_delete_location(location):
    data = Location.find_by_public_id(public_id=location.public_id)
    assert data
    data.delete_location()
    assert not Location.find_by_public_id(public_id=data.public_id)


def test_model_location_count_location(location):
    assert Location.find_by_location_name(location_name=LOCATION_NAME)
    assert not location.status
    assert Location.count() == 1
    assert Location.count(status=False) == 1
    assert Location.count(status=True) == 0
