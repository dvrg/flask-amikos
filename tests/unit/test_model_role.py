import pytest
from src.flask_amikos import user_datastore
from src.flask_amikos.models.role import Role
from tests.utils import ROLE_ADMIN, ROLE_DESC


def test_model_role(role):
    """
    GIVEN a Role model
    WHEN a new Role is created
    THEN check name, description are defined correctly
    """
    assert role.name == ROLE_ADMIN
    assert role.description == ROLE_DESC


def test_model_role_get_all_role(role):
    """
    Given a Role Model
    When a new role is created
    THEN get all data role
    """
    role_data = Role.get_all(page=1)
    assert role_data.items


def test_model_role_find_data(role):
    """
    GIVEN a Role model
    WHEN a new Role is created
    THEN find role data by name of role must be Found
    """
    assert user_datastore.find_role(ROLE_ADMIN)


@pytest.mark.parametrize(
    "name, desc",
    [
        ("Admin", "Admin role for app"),
        ("Editor", "Editor role for app"),
        ("SuperUser", "SuperUser role for app"),
    ],
)
def test_model_role_insert_multiple_role(name, desc, role):
    """
    GIVEN set multiple name and desc of role
    WHEN a new set Role is creted
    THEN check set name, description are defined correctly
    """
    assert user_datastore.find_or_create_role(name=name, description=desc)
    assert user_datastore.find_role(name)
