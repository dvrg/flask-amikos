from flask.helpers import url_for
from flask_security.utils import url_for_security
import pytest
from datetime import datetime, timedelta
from flask_security import hash_password
from src.flask_amikos import mail, create_app, user_datastore
from src.flask_amikos.util.util import remove_unused_char, day_to_second
from src.flask_amikos.models.user import User
from src.flask_amikos.models.role import Role
from src.flask_amikos.models.ads_package import AdsPackages
from src.flask_amikos.models.ads import Ads
from src.flask_amikos.models.amenities import Amenities
from src.flask_amikos.models.location import Location
from src.flask_amikos.models.transaction import Transaction
from src.flask_amikos.extensions import db as database
from tests.utils import (
    AMENITIES_DESCRIPTION,
    AMENITIES_NAME,
    EMAIL,
    PASSWORD,
    FS_UNIQUIFIER,
    ROLE_ADMIN,
    ROLE_DESC,
    ADS_PACKAGE_NAME,
    ADS_PACKAGE_PRICE,
    ADS_PACKAGE_DISCOUNT_PRICE,
    ADS_PACKAGE_DURATION,
    ADS_PACKAGE_DESCRIPTION,
    ADS_PROPERTY_TYPE,
    ADS_BRAND_NAME,
    ADS_GENDER_OF_OCCUPANT,
    ADS_NUMBER_BEDROOM_AVAILABLE,
    ADS_TYPE_OF_BATHROOM,
    ADS_NUMBER_OF_BATHROOM,
    ADS_ADDRESS,
    ADS_RENTAL_PRICE,
    ADS_DURATION_OF_EASE,
    ADS_MOBILE_NUMBER,
    ADS_WHATSAPP_NUMBER,
    ADS_ADDITIONAL_INFO,
    ADS_EMAIL,
    ADS_FEEDBACK,
    ADS_ACCEPT_POLICY,
    ADS_STATUS,
    LOCATION_NAME,
    MAIL_SUBJECT,
    MAIL_BODY,
    TRANSACTION_PAYMENT_METHOD,
    TRANSACTION_STATUS,
    WrapApp,
)


@pytest.fixture
def app():
    """
    More details :
    https://flask-security-too.readthedocs.io/en/latest/quickstart.html#unit-testing-your-application
    https://pytest-flask.readthedocs.io/en/latest/features.html
    """
    app = create_app("testing")
    return app


@pytest.fixture(scope="session", autouse=True)
def faker_session_locale():
    return ["id_ID"]


@pytest.fixture
def db(app, client, request):
    database.drop_all()
    database.create_all()
    database.session.commit()

    def fin():
        database.session.remove()

    request.addfinalizer(fin)
    return database


@pytest.fixture
def email(app):
    with mail.record_messages() as outbox:
        mail.send_message(subject=MAIL_SUBJECT, body=MAIL_BODY, recipients=[EMAIL])
    return outbox


@pytest.fixture
def role(db):
    role = user_datastore.create_role(name=ROLE_ADMIN, description=ROLE_DESC)
    return role


@pytest.fixture
def user(db):
    user = user_datastore.create_user(
        email=EMAIL,
        password=hash_password(PASSWORD),
        active=True,
        fs_uniquifier=FS_UNIQUIFIER,
    )
    return user


@pytest.fixture
def user_with_role(db, user, role):
    user_datastore.add_role_to_user(user, role)
    return user


@pytest.fixture
def ads_packages(db):
    ads_packages = AdsPackages(
        package_name=ADS_PACKAGE_NAME,
        price=ADS_PACKAGE_PRICE,
        discount_price=ADS_PACKAGE_DISCOUNT_PRICE,
        duration=day_to_second(ADS_PACKAGE_DURATION),
        description=ADS_PACKAGE_DESCRIPTION,
        created_at=datetime.now(),
        updated_at=None,
    )
    ads_packages.create_ads_packages()
    return ads_packages


@pytest.fixture
def amenities(db):
    amenities = Amenities(
        amenities_name=AMENITIES_NAME,
        description=AMENITIES_DESCRIPTION,
        status=False,
        created_at=datetime.now(),
        updated_at=None,
    )
    amenities.create_amenities()
    return amenities


@pytest.fixture
def location(db):
    location = Location(
        location_name=LOCATION_NAME,
        status=False,
        created_at=datetime.now(),
        updated_at=None,
    )
    location.create_location()
    return location


@pytest.fixture
def ads(db, location, amenities, ads_packages, user):
    current_user = user_datastore.find_user(email=user.email)
    ads = Ads(
        property_type=ADS_PROPERTY_TYPE,
        brand_name=ADS_BRAND_NAME,
        gender_of_occupant=ADS_GENDER_OF_OCCUPANT,
        number_bedroom_available=ADS_NUMBER_BEDROOM_AVAILABLE,
        type_of_bathroom=ADS_TYPE_OF_BATHROOM,
        number_of_bathroom=ADS_NUMBER_OF_BATHROOM,
        address=ADS_ADDRESS,
        rental_price=ADS_RENTAL_PRICE,
        duration_of_ease=ADS_DURATION_OF_EASE,
        mobile_number=remove_unused_char(ADS_MOBILE_NUMBER),
        whatsapp_number=remove_unused_char(ADS_WHATSAPP_NUMBER),
        additional_info=ADS_ADDITIONAL_INFO,
        email=ADS_EMAIL,
        feedback=ADS_FEEDBACK,
        accept_policy=ADS_ACCEPT_POLICY,
        status=ADS_STATUS,
        created_at=datetime.now(),
        updated_at=None,
    )
    ads.user_id = current_user.id
    ads.location_id = location.id
    ads.remaining_time = ads_packages.duration
    ads.start_date = ads.created_at
    ads.end_date = ads.start_date + timedelta(seconds=ads.remaining_time)
    ads.ads_cost = ads_packages.discount_price
    ads.create_ads()
    ads.amenities.append(amenities)
    ads.ads_packages.append(ads_packages)
    db.session.commit()
    return ads


@pytest.fixture
def transaction(db, ads):
    transaction = Transaction(
        ads_id=ads.id,
        payment_method=TRANSACTION_PAYMENT_METHOD,
        transaction_status=TRANSACTION_STATUS,
        created_at=datetime.now(),
        updated_at=None,
    )
    transaction.create_transaction()
    return transaction


@pytest.fixture
def login_default_user(client):
    client.post(
        url_for_security("login"),
        data=dict(email=EMAIL, password=PASSWORD, follow_redirects=True),
    )

    client.get(url_for_security("logout"), follow_redirects=True)
