import pytest
from flask_security import url_for_security, hash_password
from src.flask_amikos.models.user import User
from src.flask_amikos.models.role import Role

ROLE_ADMIN = "Admin"
ROLE_DESC = "Admin role for app"
EMAIL = "user@mail.com"
ANOTHER_EMAIL = "change@localhost"
PASSWORD = "Test1234"
FS_UNIQUIFIER = "01066b80c0694f6da6ea5fe9ac57f268"

ADS_PACKAGE_NAME = "Basic"
ADS_PACKAGE_PRICE = 10000
ADS_PACKAGE_DISCOUNT_PRICE = 5000
ADS_PACKAGE_DURATION = 30
ADS_PACKAGE_DESCRIPTION = "Tampilkan iklan saya di halaman beranda &amp; social media <s>(+ Rp. 10.000)</s><strong>(Rp. 0) :snake </strong>"

AMENITIES_NAME = "Kamar Mandi Dalam <img src='https://github.githubassets.com/images/icons/emoji/unicode/1f388.png?v8'>"
AMENITIES_DESCRIPTION = "Fasilitas kamar mandi dalam"

ADS_PROPERTY_TYPE = "indekos"
ADS_BRAND_NAME = "The Paragon"
ADS_GENDER_OF_OCCUPANT = "he"
ADS_NUMBER_BEDROOM_AVAILABLE = 2
ADS_TYPE_OF_BATHROOM = "kmd"
ADS_NUMBER_OF_BATHROOM = 2
ADS_LOCATION_CLOSE_TO = "uay"
ADS_ADDRESS = "Jl. Ring Road Utara No. 4 Condong Catur Seleman, D.I Yogyakarta"
ADS_AMENITIES = 1
ADS_RENTAL_PRICE = 450000
ADS_DURATION_OF_EASE = "d1"
ADS_MOBILE_NUMBER = "+628-1290-1209"
ADS_WHATSAPP_NUMBER = "+628-1290-1209"
ADS_ADDITIONAL_INFO = "dilarang membawa hewan peliharaan, jam malam pukul 23:00 wib"
ADS_PACKAGES = 1
ADS_EMAIL = "cp@theparagon.com"
ADS_FEEDBACK = "semoga amikos bisa lebih baik lagi kedepannya"
ADS_ACCEPT_POLICY = True
ADS_STATUS = False

LOCATION_NAME = "Universitas Amikom Yogyakarta"

MAIL_SUBJECT = "Pembelian Paket Iklan"
MAIL_BODY = "<p>Selamat anda berhasil membeli paket iklan</p>"

TRANSACTION_PAYMENT_METHOD = "credit_card"
TRANSACTION_STATUS = "created"

RESPONSE_PAYMENT_CARD = {
    "transaction_time": "2020-01-09 18:27:19",
    "transaction_status": "capture",
    "transaction_id": "57d5293c-e65f-4a29-95e4-5959c3fa335b",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "16d6f84b2fb0468e2a9cf99a8ac4e5d803d42180347aaa70cb2a7abb13b5c6130458ca9c71956a962c0827637cd3bc7d40b21a8ae9fab12c7c3efe351b18d00a",
    "payment_type": "credit_card",
    "order_id": "Postman-1578568851",
    "merchant_id": "G141532850",
    "masked_card": "481111-1114",
    "gross_amount": "10000.00",
    "fraud_status": "accept",
    "eci": "05",
    "currency": "IDR",
    "channel_response_message": "Approved",
    "channel_response_code": "00",
    "card_type": "credit",
    "bank": "bni",
    "approval_code": "1578569243927",
}

RESPONSE_PAYMENT_GOPAY = {
    "transaction_time": "2021-06-15 18:45:13",
    "transaction_status": "settlement",
    "transaction_id": "513f1f01-c9da-474c-9fc9-d5c64364b709",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "2496c78cac93a70ca08014bdaaff08eb7119ef79ef69c4833d4399cada077147febc1a231992eb8665a7e26d89b1dc323c13f721d21c7485f70bff06cca6eed3",
    "settlement_time": "2021-06-15 18:45:28",
    "payment_type": "gopay",
    "order_id": "Order-5100",
    "merchant_id": "G141532850",
    "gross_amount": "154600.00",
    "fraud_status": "accept",
    "currency": "IDR",
}
RESPONSE_PAYMENT_QRIS = {
    "transaction_type": "on-us",
    "transaction_time": "2021-06-23 14:02:42",
    "transaction_status": "settlement",
    "transaction_id": "513f1f01-c9da-474c-9fc9-d5c64364b709",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "2496c78cac93a70ca08014bdaaff08eb7119ef79ef69c4833d4399cada077147febc1a231992eb8665a7e26d89b1dc323c13f721d21c7485f70bff06cca6eed3",
    "settlement_time": "2021-06-23 14:06:48",
    "payment_type": "qris",
    "order_id": "qris-01",
    "merchant_id": "G141532850",
    "issuer": "gopay",
    "gross_amount": "5539.00",
    "fraud_status": "accept",
    "currency": "IDR",
    "acquirer": "gopay",
}
RESPONSE_PAYMENT_SHOPEEPAY = {
    "transaction_time": "2021-06-23 13:28:05",
    "transaction_status": "settlement",
    "transaction_id": "513f1f01-c9da-474c-9fc9-d5c64364b709",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "2496c78cac93a70ca08014bdaaff08eb7119ef79ef69c4833d4399cada077147febc1a231992eb8665a7e26d89b1dc323c13f721d21c7485f70bff06cca6eed3",
    "settlement_time": "2021-06-23 13:28:21",
    "payment_type": "shopeepay",
    "order_id": "shopeepay-01",
    "merchant_id": "G141532850",
    "gross_amount": "16700.00",
    "fraud_status": "accept",
    "currency": "IDR",
}

RESPONSE_PEYMENT_BANK_TRANSFER = {
    "va_numbers": [{"va_number": "123456789123456789", "bank": "bri"}],
    "transaction_time": "2021-06-23 11:53:34",
    "transaction_status": "settlement",
    "transaction_id": "9aed5972-5b6a-401e-894b-a32c91ed1a3a",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "fe5f725ea770c451017e9d6300af72b830a668d2f7d5da9b778ec2c4f9177efe5127d492d9ddfbcf6806ea5cd7dc1a7337c674d6139026b28f49ad0ea1ce5107",
    "settlement_time": "2021-06-23 11:53:34",
    "payment_type": "bank_transfer",
    "payment_amounts": [],
    "order_id": "bri-va-01",
    "merchant_id": "G141532850",
    "gross_amount": "300000.00",
    "fraud_status": "accept",
    "currency": "IDR",
}

RESPONSE_PAYMENT_ECHANNEL = {
    "transaction_time": "2021-06-23 11:57:49",
    "transaction_status": "settlement",
    "transaction_id": "883af6a4-c1b4-4d39-9bd8-b148fcebe853",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "bbceb3724b0b2446c59435795039fed2d249d3438f06bf90c999cc9d383b95170b7b58f9412fba25ce7756da8075ab1d78a48800156380a62dc84eb22b3f7de9",
    "settlement_time": "2021-06-23 11:58:44",
    "payment_type": "echannel",
    "order_id": "mandiri-bill-01",
    "merchant_id": "G141532850",
    "gross_amount": "30000.00",
    "fraud_status": "accept",
    "currency": "IDR",
    "biller_code": "70012",
    "bill_key": "990000000260",
}

RESPONSE_PAYMENT_KLIKBCA = {
    "transaction_time": "2021-06-23 11:50:38",
    "transaction_status": "settlement",
    "transaction_id": "c0ba3583-5111-45a5-9f1c-84c9de7cb2f6",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "ef0f472fa8a5165dc9f2ff6300832eb28657e88b9f3335ae5ebb27c8ef258d203c6da18ac6cd5738d2e38c54dfec860d8e067bdbc759a1268ab04218ccab93cc",
    "settlement_time": "2021-06-23 12:17:29",
    "payment_type": "bca_klikbca",
    "order_id": "klikbca-01",
    "merchant_id": "G141532850",
    "gross_amount": "1713600.00",
    "currency": "IDR",
    "approval_code": "ABC0101BCA02",
}
RESPONSE_PAYMENT_BCA_KLIKPAY = {
    "transaction_time": "2021-06-23 09:26:08",
    "transaction_status": "settlement",
    "transaction_id": "ada84cd9-2233-4c67-877a-01884eece45e",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "35c4111539e184b268b7c1cd62a9c254e5d27c992c8fd55084f930b69b09eaafcfe14b0d512c697648295fdb45de777e1316b401f4729846a91b3de88cde3f05",
    "settlement_time": "2021-06-23 09:26:51",
    "payment_type": "bca_klikpay",
    "order_id": "bca-klikpay-01",
    "merchant_id": "G141532850",
    "gross_amount": "912844.00",
    "fraud_status": "accept",
    "currency": "IDR",
    "approval_code": "",
}
RESPONSE_PAYMENT_CIMB_CLICKS = {
    "transaction_time": "2021-06-23 11:39:21",
    "transaction_status": "settlement",
    "transaction_id": "226f042f-020e-4829-8bd7-2de64b8673ce",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "3bcdf0700d3c8a288f279e4fe27a4012e916cb44120d541f6e4c48c83a107b605fdb063ae7c8744d15891047aeb1fc8d2e95741c0abc5f67e10e0b60244bc441",
    "settlement_time": "2021-06-23 11:40:24",
    "payment_type": "cimb_clicks",
    "order_id": "cimb-clicks-01",
    "merchant_id": "G141532850",
    "gross_amount": "2444700.00",
    "currency": "IDR",
    "approval_code": "ABC0101BCA02",
}
RESPONSE_PAYMENT_DANAMON_ONLINE = {
    "transaction_time": "2021-06-18 14:55:15",
    "transaction_status": "settlement",
    "transaction_id": "226f042f-020e-4829-8bd7-2de64b8673ce",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "3bcdf0700d3c8a288f279e4fe27a4012e916cb44120d541f6e4c48c83a107b605fdb063ae7c8744d15891047aeb1fc8d2e95741c0abc5f67e10e0b60244bc441",
    "settlement_time": "2021-06-18 14:58:57",
    "payment_type": "danamon_online",
    "order_id": "danamon-online-01",
    "merchant_id": "G141532850",
    "gross_amount": "30000.00",
    "fraud_status": "accept",
    "currency": "IDR",
    "approval_code": "ABC0101BCA02",
}
RESPONSE_PAYMENT_INDOMARET = {
    "transaction_time": "2021-06-23 13:13:21",
    "transaction_status": "settlement",
    "transaction_id": "991af93c-1049-4973-b38f-d6052c72e367",
    "store": "indomaret",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "a198f93ac43cf98171dcb4bd0323c7e3afbee77a162a09e2381f0a218c761a4ef0254d7650602971735c486fea2e8e9c6d41ee65d6a53d65a12fb1c824e86f9f",
    "settlement_time": "2021-06-23 13:13:56",
    "payment_type": "cstore",
    "payment_code": "300063000630006300",
    "order_id": "indomaret-01",
    "merchant_id": "G141532850",
    "gross_amount": "336000.00",
    "currency": "IDR",
    "approval_code": "ABC0101BCA02",
}
RESPONSE_PAYMENT_ALFAMART = {
    "transaction_time": "2021-06-23 13:17:01",
    "transaction_status": "settlement",
    "transaction_id": "991af93c-1049-4973-b38f-d6052c72e367",
    "store": "alfamart",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "a198f93ac43cf98171dcb4bd0323c7e3afbee77a162a09e2381f0a218c761a4ef0254d7650602971735c486fea2e8e9c6d41ee65d6a53d65a12fb1c824e86f9f",
    "settlement_time": "2021-06-23 13:18:04",
    "payment_type": "cstore",
    "payment_code": "300063000630006300",
    "order_id": "alfamart-01",
    "merchant_id": "G141532850",
    "gross_amount": "662000.00",
    "fraud_status": "accept",
    "currency": "IDR",
}
RESPONSE_PAYMENT_AKULAKU = {
    "transaction_time": "2021-06-23 10:55:24",
    "transaction_status": "settlement",
    "transaction_id": "b3a40398-d95d-4bb9-afe8-9a57bc0786ea",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "35c4111539e184b268b7c1cd62a9c254e5d27c992c8fd55084f930b69b09eaafcfe14b0d512c697648295fdb45de777e1316b401f4729846a91b3de88cde3f05",
    "settlement_time": "2021-06-23 10:56:55",
    "payment_type": "akulaku",
    "order_id": "akulaku-01",
    "merchant_id": "G141532850",
    "gross_amount": "130000.00",
    "fraud_status": "accept",
    "currency": "IDR",
}
RESPONSE_PAYMENT_BRI_EPAY = {
    "transaction_time": "2021-06-21 23:31:51",
    "transaction_status": "settlement",
    "transaction_id": "f8635cd7-615d-4a6d-a806-c9ca4a56257e",
    "status_message": "midtrans payment notification",
    "status_code": "200",
    "signature_key": "13b6b8a2da46428812e7685463770e3704ece7fc3242a5f016f068b7b135e12a71afd02259fe4dbd8c97d747ae9cf8e13412842325ea8da4cf6d7177e32b7e31",
    "settlement_time": "2021-06-21 23:32:40",
    "payment_type": "bri_epay",
    "order_id": "bri-epay-01",
    "merchant_id": "G141532850",
    "gross_amount": "5622200.00",
    "fraud_status": "accept",
    "currency": "IDR",
    "approval_code": "ABC0101BCA02",
}


class WrapApp:
    def __init__(self, app, user_cls=None, role_cls=None):
        """Used to help create a app test fixture - with optionally passing in mocks"""
        self.app = app
        self.user_cls = user_cls
        self.role_cls = role_cls
        self.test_client = app.test_client()


def create_fake_user(email=EMAIL, password=PASSWORD, userid=1, roles=None):
    """
    Function for create fake user optionally with roles.
    use with `editor_user = create_fake_user(role=['Editor])`
    and pass `editor_user` to `set_current_user` function
    """

    user = User()
    user.email = email
    user.id = userid
    user.password = hash_password(password)
    user.active = True
    if roles:
        user.roles = []
        if isinstance(roles, str):
            roles = [roles]
        for role_name in roles:
            role = Role()
            role.name = role_name
            user.roles.append(role)
    return user


def create_fake_role(role_cls, name, permissions=None):
    if permissions:
        permissions = ",".join(p.strip() for p in permissions.split(","))
    return role_cls(name=name, permissions=permissions)


def set_current_user(app, user_with_role):
    """
    Set up so that when request is received,
    the token will cause 'user' to be made the current_user
    We can pass `user` fixtures or object from `create_fake_user`
    """

    def token_cb(request):
        if request.headers.get("Cookie") == "session":
            return user_with_role
        return app.login_manager.anonymous_user()

    app.login_manager.request_loader(token_cb)


def security_forgot_password(test_client):
    return test_client.post(
        url_for_security("forgot_password"), data=dict(email=EMAIL), follow_redirects=True
    )


def security_login(test_client):
    return test_client.post(
        url_for_security("login"),
        data=dict(email=EMAIL, password=PASSWORD, remember="y"),
        follow_redirects=True,
    )


def security_logout(test_client):
    return test_client.post(url_for_security("logout"), follow_redirect=True)


def security_reset_password(test_client):
    return test_client.post(url_for_security("reset_password"), data=dict())


def security_register(test_client):
    return test_client.post(
        url_for_security("register"),
        data=dict(
            email=EMAIL,
            password=PASSWORD,
        ),
        follow_redirects=True,
    )


def security_send_confirmation(test_client, user):
    return test_client.post(
        url_for_security("send_confirmation"),
        data=dict(email=EMAIL),
        follow_redirects=True,
    )


def create_ads(
    client,
    property_type,
    brand_name,
    gender_of_occupant,
    number_bedroom_available,
    type_of_bathroom,
    number_of_bathroom,
    location_id,
    address,
    rental_price,
    duration_of_ease,
    mobile_number,
    whatsapp_number,
    additional_info,
    email,
    feedback,
    accept_policy,
    status,
    created_at,
    updated_at,
    ads_packages,
    amenities,
):
    return client.post(
        "/ads",
        data=dict(
            property_type=property_type,
            brand_name=brand_name,
            gender_of_occupant=gender_of_occupant,
            number_bedroom_available=number_bedroom_available,
            type_of_bathroom=type_of_bathroom,
            number_of_bathroom=number_of_bathroom,
            location_id=location_id,
            address=address,
            rental_price=rental_price,
            duration_of_ease=duration_of_ease,
            mobile_number=mobile_number,
            whatsapp_number=whatsapp_number,
            additional_info=additional_info,
            email=email,
            feedback=feedback,
            accept_policy=accept_policy,
            status=status,
            created_at=created_at,
            updated_at=updated_at,
            ads_packages=ads_packages,
            amenities=amenities,
        ),
        follow_redirects=True,
    )
