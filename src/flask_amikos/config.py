"""Config settings for for development, testing and production environments."""
import os
import secrets
import pkg_resources
from passlib import totp
from pathlib import Path
from datetime import datetime
from flask_security import uia_phone_mapper, uia_email_mapper
from flask_apscheduler.auth import HTTPBasicAuth


HERE = Path(__file__).parent
SQLITE_DEV = "sqlite:///" + str(HERE / "flask_amikos_dev.db")
SQLITE_TEST = "sqlite:///" + str(HERE / "flask_amikos_test.db")

CELERY_DB_DEV = "sqla+" + SQLITE_DEV
CELERY_DB_TEST = "sqla+" + SQLITE_TEST

REDIS_RESULT_BACKEND = "redis://localhost:6379/0"
REDIS_BROKER_URL = "redis://localhost:6379/0"


class Config:
    """Base configuration.
    This is parent configuration for all environtment. Details documentation every config :
    Flask: https://flask.palletsprojects.com/en/1.1.x/config/#builtin-configuration-values
    Flask-Security-Too: https://flask-security-too.readthedocs.io/en/stable/configuration.html
    Flask-Mail: https://pythonhosted.org/Flask-Mail/#configuring-flask-mail
    Flask-SQLAlchemy : https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/
    Flask-Bootstrap : https://bootstrap-flask.readthedocs.io/en/stable/basic.html#configurations
    Flask-Babel : https://flask-babel.tkte.ch/#configuration
    Flask-DebugToolbar: https://flask-debugtoolbar.readthedocs.io/en/latest/#configuration
    """

    APP_NAME = "Amikos"
    APP_VERSION = "1.0.0"
    APP_SOURCE_CODE = "https://gitlab.com/dvrg/flask-amikos"
    APP_AUTHOR_PROFILE = "https://twitter.com/hgenpati"
    APP_WHATSAPP = "https://wa.me/+628981055595"
    APP_TWITTER = "https://twitter.com/amikos.id"
    APP_FACEBOOK = "https://facebook.com/amikos.id"
    APP_INSTAGRAM = "https://instagram.com/amikos.id"

    # Flask
    PRESERVE_CONTEXT_ON_EXCEPTION = True

    # Flask-Security-Too : Core
    SECURITY_BLUEPRINT_NAME = "security"
    SECURITY_PASSWORD_HASH = "bcrypt"
    SECURITY_PASSWORD_COMPLEXITY_CHECKER = "zxcvbn"
    SECURITY_USER_IDENTITY_ATTRIBUTES = [
        {"email": {"mapper": uia_email_mapper, "case_insensitive": True}},
        {"us_phone_number": {"mapper": uia_phone_mapper}},
    ]
    # Save datetime aware time zone information https://realpython.com/python-datetime/
    SECURITY_DATETIME_FACTORY = datetime.now
    SECURITY_CONFIRM_SALT = os.getenv("SECURITY_CONFIRM_SALT")
    SECURITY_RESET_SALT = os.getenv("SECURITY_RESET_SALT")
    SECURITY_LOGIN_SALT = os.getenv("SECURITY_LOGIN_SALT")
    SECURITY_TWO_FACTOR_VALIDITY_SALT = os.getenv("SECURITY_TWO_FACTOR_VALIDITY_SALT")
    SECURITY_US_SETUP_SALT = os.getenv("SECURITY_US_SETUP_SALT")
    # Flask-Security-Too : Registerable
    SECURITY_REGISTERABLE = True
    SECURITY_POST_REGISTER_VIEW = "/login"
    # Flask-Security-Too : Confirmable
    SECURITY_CONFIRMABLE = True
    SECURITY_AUTO_LOGIN_AFTER_CONFIRM = False
    # Flask-Security-Too : Changeable
    SECURITY_CHANGEABLE = True
    SECURITY_POST_CHANGE_VIEW = "/logout"
    # Flask-Security-Too : Recoverable
    SECURITY_RECOVERABLE = True
    SECURITY_POST_RESET_VIEW = "/"

    # Flask-Security-Too : Two-Factor
    SECURITY_TWO_FACTOR = True
    SECURITY_TWO_FACTOR_ENABLED_METHODS = ["email", "authenticator", "sms"]
    SECURITY_TOTP_SECRETS = {1: totp.generate_secret()}
    SECURITY_TOTP_ISSUER = APP_NAME
    SECURITY_PHONE_REGION_DEFAULT = "ID"
    # Flask-Security-Too : Unified Signin
    SECURITY_UNIFIED_SIGNIN = True
    SECURITY_US_ENABLED_METHODS = ["password", "email", "authenticator", "sms"]
    # Flask-Security-Too : Passwordless
    SECURITY_PASSWORDLESS = False
    # Flask-Security-Too : Trackable
    SECURITY_TRACKABLE = True
    # Flask-Security-Too : Login/Logout
    SECURITY_POST_LOGOUT_VIEW = "/login"

    # Flask-Security-Too Translations
    SECURITY_I18N_DOMAIN = "flask_security"
    SECURITY_I18N_DIRNAME = [
        pkg_resources.resource_filename("flask_security", "translations"),
        "/src/flask_amikos/translations",
    ]

    # Flask-SQLAlchemy
    POST_PER_PAGE = 10
    MAX_PER_PAGE = 10
    SQLALCHEMY_RECORD_QUERIES = True
    SLOW_DB_QUERY_TIME = 0.5

    # Flask-Babel
    BABEL_DEFAULT_LOCALE = "id"
    BABEL_DEFAULT_TIMEZONE = "UTC"
    LANGUAGES = ["id", "en"]

    # Flask-WTF
    RECAPTCHA_PARAMETERS = {"hl": "id"}

    # Flask Profiler
    FLASK_PROFILER_USERNAME = os.getenv("FLASK_PROFILER_USERNAME")
    FLASK_PROFILER_PASSWORD = os.getenv("FLASK_PROFILER_PASSWORD")
    FLASK_PROFILER_ENDPOINT = os.getenv("FLASK_PROFILER_ENDPOINT")
    FLASK_PROFILER_DATABASE_URL = os.getenv("FLASK_PROFILER_DATABASE_URL")

    # Flask Talisman
    SELF = "'self'"
    CSP = {
        "default-src": [
            SELF,
            "ghbtns.com",
            "www.gravatar.com",
            "github.githubassets.com",
            "api.github.com",
        ],
        "style-src": [
            SELF,
            "'unsafe-inline'",
            "fonts.googleapis.com",
            "cdn.jsdelivr.net",
            "*.midtrans.com",
            "*.cloudfront.net",
        ],
        "font-src": [
            SELF,
            "fonts.googleapis.com",
            "themes.googleusercontent.com",
            "*.gstatic.com",
            "cdn.jsdelivr.net",
            "*.cloudfront.net",
        ],
        "frame-src": [
            SELF,
            "www.google.com",
            "www.youtube.com",
            "*.midtrans.com",
            "*.cloudfront.net",
            "ghbtns.com",
        ],
        "script-src": [
            SELF,
            "'unsafe-inline'",
            "code.jquery.com",
            "ajax.googleapis.com",
            "*.googleanalytics.com ",
            "*.google-analytics.com",
            "cdnjs.cloudflare.com",
            "*.midtrans.com",
            "cdn.jsdelivr.net",
            "www.google.com",
            "www.gstatic.com",
            "*.cloudfront.net",
        ],
    }

    # Elasticsearch
    ELASTICSEARCH_USERNAME = os.getenv("ELASTICSEARCH_USERNAME")
    ELASTICSEARCH_PASSWORD = os.getenv("ELASTICSEARCH_PASSWORD")

    # Flask-APSchedular
    SCHEDULER_API_ENABLED = True
    SCHEDULER_AUTH = HTTPBasicAuth()
    FLASK_APPSCHEDULER_USERNAME = os.getenv("FLASK_APPSCHEDULER_USERNAME")
    FLASK_APPSCHEDULER_PASSWORD = os.getenv("FLASK_APPSCHEDULER_PASSWORD")

    @staticmethod
    def init_app(app):
        pass


class TestingConfig(Config):
    """Testing configuration."""

    PROFILE = True
    TESTING = True

    WTF_CSRF_ENABLED = False
    WTF_CSRF_CHECK_DEFAULT = False

    RECAPTCHA_PUBLIC_KEY = os.getenv("RECAPTCHA_PUBLIC_KEY")
    RECAPTCHA_PRIVATE_KEY = os.getenv("RECAPTCHA_PRIVATE_KEY")

    SECRET_KEY = os.getenv("SECRET_KEY", secrets.token_hex())
    SECURITY_PASSWORD_SALT = os.getenv("SECURITY_PASSWORD_SALT", secrets.token_hex())

    DEBUG_TB_ENABLED = False

    SECURITY_TOKEN_MAX_AGE = 5  # 5 Seconds
    SECURITY_EMAIL_VALIDATOR_ARGS = {"check_deliverability": False}
    SECURITY_PASSWORD_HASH = "plaintext"
    SQLALCHEMY_DATABASE_URI = SQLITE_TEST
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    LOGIN_DISABLED = True

    CELERY_RESULT_BACKEND = os.getenv("CELERY_RESULT_BACKEND", REDIS_RESULT_BACKEND)
    CELERY_BROKER_URL = os.getenv("CELERY_BROKER_URL", REDIS_BROKER_URL)

    MAIL_SERVER = "localhost"
    MAIL_PORT = 8025
    MAIL_USE_TLS = False
    MAIL_USE_SSL = False
    MAIL_USERNAME = ""
    MAIL_PASSWORD = ""
    MAIL_DEFAULT_SENDER = "no-reply@localhost.com"

    SECURITY_SMS_SERVICE = "Twilio"
    SECURITY_SMS_SERVICE_CONFIG = {
        "ACCOUNT_SID": os.getenv("ACCOUNT_SID"),
        "AUTH_TOKEN": os.getenv("AUTH_TOKEN"),
        "PHONE_NUMBER": os.getenv("PHONE_NUMBER"),
    }

    MIDTRANS_PRODUCTION = False
    MIDTRANS_SERVER_KEY = os.getenv("MIDTRANS_SERVER_KEY")
    MIDTRANS_CLIENT_KEY = os.getenv("MIDTRANS_CLIENT_KEY")

    ELASTICSEARCH_URL = os.getenv("ELASTICSEARCH_URL")

    SENTRY_DSN = os.getenv("SENTRY_DSN")
    TRACE_SAMPLE_RATE = 1.0


class DevelopmentConfig(Config):
    """Development configuration."""

    FLASK_ENV = "development"
    SECRET_KEY = os.getenv("SECRET_KEY", secrets.token_hex())
    SECURITY_PASSWORD_SALT = os.getenv("SECURITY_PASSWORD_SALT", secrets.token_hex())

    DEBUG_TB_PROFILER_ENABLED = True
    DEBUG_TB_TEMPLATE_EDITOR_ENABLED = True
    DEBUG_TB_INTERCEPT_REDIRECTS = True

    RECAPTCHA_PUBLIC_KEY = os.getenv("RECAPTCHA_PUBLIC_KEY")
    RECAPTCHA_PRIVATE_KEY = os.getenv("RECAPTCHA_PRIVATE_KEY")

    SECURITY_TOKEN_MAX_AGE = 900  # 15 Minutes
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", SQLITE_DEV)
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    CELERY_RESULT_BACKEND = os.getenv("CELERY_RESULT_BACKEND", REDIS_RESULT_BACKEND)
    CELERY_BROKER_URL = os.getenv("CELERY_BROKER_URL", REDIS_BROKER_URL)

    MAIL_SERVER = "localhost"
    MAIL_PORT = 8025
    MAIL_USE_TLS = False
    MAIL_USE_SSL = False
    MAIL_USERNAME = ""
    MAIL_PASSWORD = ""
    MAIL_DEFAULT_SENDER = "no-reply@localhost.com"

    SECURITY_SMS_SERVICE = "Twilio"
    SECURITY_SMS_SERVICE_CONFIG = {
        "ACCOUNT_SID": os.getenv("ACCOUNT_SID"),
        "AUTH_TOKEN": os.getenv("AUTH_TOKEN"),
        "PHONE_NUMBER": os.getenv("PHONE_NUMBER"),
    }

    MIDTRANS_PRODUCTION = False
    MIDTRANS_SERVER_KEY = os.getenv("MIDTRANS_SERVER_KEY")
    MIDTRANS_CLIENT_KEY = os.getenv("MIDTRANS_CLIENT_KEY")

    ELASTICSEARCH_URL = os.getenv("ELASTICSEARCH_URL")

    SENTRY_DSN = os.getenv("SENTRY_DSN")
    TRACE_SAMPLE_RATE = 1.0

    WTF_CSRF_SECRET_KEY = os.getenv("WTF_CSRF_SECRET_KEY")


class ProductionConfig(Config):
    """Production configuration."""

    FLASK_ENV = "production"
    SECRET_KEY = os.getenv("SECRET_KEY")
    SECURITY_PASSWORD_SALT = os.getenv("SECURITY_PASSWORD_SALT")

    RECAPTCHA_PUBLIC_KEY = os.getenv("RECAPTCHA_PUBLIC_KEY")
    RECAPTCHA_PRIVATE_KEY = os.getenv("RECAPTCHA_PRIVATE_KEY")

    SECURITY_TOKEN_MAX_AGE = 3600  # 1 Hours
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    CELERY_RESULT_BACKEND = os.getenv("CELERY_RESULT_BACKEND")
    CELERY_BROKER_URL = os.getenv("CELERY_BROKER_URL")

    MAIL_SERVER = "smtp.sendgrid.net"
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = os.getenv("MAIL_USERNAME")
    MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")
    MAIL_DEFAULT_SENDER = "no-reply@amikos.xyz"
    MAIL_ADMINISTRATOR = os.getenv("MAIL_ADMINISTRATOR")
    MAIL_MAX_EMAILS = 600

    SECURITY_SMS_SERVICE = "Twilio"
    SECURITY_SMS_SERVICE_CONFIG = {
        "ACCOUNT_SID": os.getenv("ACCOUNT_SID"),
        "AUTH_TOKEN": os.getenv("AUTH_TOKEN"),
        "PHONE_NUMBER": os.getenv("PHONE_NUMBER"),
    }

    MIDTRANS_PRODUCTION = True
    MIDTRANS_SERVER_KEY = os.getenv("MIDTRANS_SERVER_KEY")
    MIDTRANS_CLIENT_KEY = os.getenv("MIDTRANS_CLIENT_KEY")

    ELASTICSEARCH_URL = os.getenv("ELASTICSEARCH_URL")

    SENTRY_DSN = os.getenv("SENTRY_DSN")
    TRACE_SAMPLE_RATE = 0.25

    WTF_CSRF_SECRET_KEY = os.getenv("WTF_CSRF_SECRET_KEY")

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)
        import logging
        from logging.handlers import SMTPHandler

        credentials = None
        secure = None
        if getattr(cls, "MAIL_USERNAME", None) is not None:
            credentials = (cls.MAIL_USERNAME, cls.MAIL_PASSWORD)
            if getattr(cls, "MAIL_USE_TLS", None):
                secure = ()
        mail_handler = SMTPHandler(
            mailhost=(cls.MAIL_SERVER, cls.MAIL_PORT),
            fromaddr=cls.MAIL_DEFAULT_SENDER,
            toaddrs=[cls.MAIL_ADMINISTRATOR],
            subject=cls.APP_NAME + " Application Error!",
            credentials=credentials,
            secure=secure,
        )
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)


class HerokuConfig(ProductionConfig):
    SSL_REDIRECT = True if os.getenv("DYNO") else False

    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        import logging
        from logging import StreamHandler

        file_handler = StreamHandler()
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

        from werkzeug.middleware.proxy_fix import ProxyFix

        app.wsgi_app = ProxyFix(app.wsgi_app)


ENV_CONFIG_DICT = dict(
    development=DevelopmentConfig,
    testing=TestingConfig,
    production=ProductionConfig,
    heroku=HerokuConfig,
)


def get_config(config_name):
    """Retrieve environment configuration settings."""
    return ENV_CONFIG_DICT.get(config_name, ProductionConfig)
