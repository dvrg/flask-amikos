from flask_sqlalchemy import SQLAlchemy
from flask_security import Security
from flask_security.models import fsqla_v2 as fsqla
from flask_migrate import Migrate
from flask_mail import Mail
from flask_babel import Babel
from flask_debugtoolbar import DebugToolbarExtension
from flask_moment import Moment
from flask_menu import Menu
from flask_breadcrumbs import Breadcrumbs
from flask_profiler import Profiler
from flask_talisman import Talisman
from flask_wtf.csrf import CSRFProtect
from flask_apscheduler import APScheduler

db = SQLAlchemy()
migrate = Migrate()
mail = Mail()
toolbar = DebugToolbarExtension()
security = Security()
babel = Babel(default_domain=security.i18n_domain)
fsqla.FsModels.set_db_info(db)
moment = Moment()
menu = Menu()
breadcrumbs = Breadcrumbs()
profiler = Profiler()
talisman = Talisman()
csrf = CSRFProtect()
scheduler = APScheduler()
