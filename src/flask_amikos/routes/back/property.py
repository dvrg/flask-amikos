from flask import (
    url_for,
    render_template,
    redirect,
    request,
    render_template_string,
)
from flask.globals import current_app
from flask_security import auth_required, roles_required
from flask_breadcrumbs import register_breadcrumb
from flask_babel import _
from src.flask_amikos.routes.back import back as bp
from src.flask_amikos.models.ads import Ads
from src.flask_amikos.forms.back.ads import SearchAdsForm

PREFIX = "property"


def tmpl_show_menu():
    return render_template_string(
        """
        {%- for item in current_menu.children %}
            {% if item.active %}*{% endif %}{{ item.text }}
        {% endfor -%}
        """
    )


@bp.route(f"/{PREFIX}")
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".property", _("Property"))
def property():
    page = request.args.get("page", 1, type=int)
    pagination = Ads.get_all(status=False, page=page)
    property = pagination.items
    search_form = SearchAdsForm()
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
            page_title=PREFIX,
            pagination=pagination,
            property=property,
            search_form=search_form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/search")
@auth_required("session")
@roles_required("Admin")
def property_search():
    search_form = SearchAdsForm()
    if not search_form.validate():
        return redirect(url_for("back.property"))
    page = request.args.get("page", 1, type=int)
    property, total = Ads.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "back.property_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "back.property_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        property=property,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )
