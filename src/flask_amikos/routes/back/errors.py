"""
Routes for handle all error page for Blueprint back only!
"""

from http import HTTPStatus
from flask import render_template
from flask_security import auth_required, roles_required
from flask_babel import _
from src.flask_amikos.routes.back import back as bp


@bp.errorhandler(HTTPStatus.FORBIDDEN)
@auth_required("session")
def forbidden(e):
    """specific for this page templates, the base template must using security base"""
    return render_template(
        bp.__getattribute__("name") + "/401.html", page_title=_("forbidden")
    )


@bp.errorhandler(HTTPStatus.NOT_FOUND)
@auth_required("session")
@roles_required("Admin")
def page_not_found(e):
    return render_template(
        bp.__getattribute__("name") + "/404.html", page_title=_("page not found")
    )


@bp.errorhandler(HTTPStatus.INTERNAL_SERVER_ERROR)
@auth_required("session")
@roles_required("Admin")
def internal_server_error(e):
    return render_template(
        bp.__getattribute__("name") + "/500.html", page_title=_("internal server error")
    )
