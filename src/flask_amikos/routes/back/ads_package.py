from datetime import datetime
from flask import (
    url_for,
    render_template,
    redirect,
    flash,
    request,
    render_template_string,
    current_app,
)
from flask_security import auth_required, roles_required
from flask_breadcrumbs import register_breadcrumb
from flask_babel import _
from src.flask_amikos.routes.back import back as bp
from src.flask_amikos.models.ads_package import AdsPackages
from src.flask_amikos.forms.back.ads_package import (
    CreateAdsPackageForm,
    UpdateAdsPackageForm,
    SearchAdsPackageForm,
)
from src.flask_amikos.util.message import _default_messages
from src.flask_amikos.util.util import day_to_second, second_to_day

PREFIX = "ads_package"


def tmpl_show_menu():
    return render_template_string(
        """
        {%- for item in current_menu.children %}
            {% if item.active %}*{% endif %}{{ item.text }}
        {% endfor -%}
        """
    )


@bp.route(f"/{PREFIX}")
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".ads_package", _("Ads Packages"))
def ads_package():
    page = request.args.get("page", 1, type=int)
    pagination = AdsPackages.get_all(page=page)
    ads_package = pagination.items
    search_form = SearchAdsPackageForm()
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
            page_title=PREFIX,
            pagination=pagination,
            ads_package=ads_package,
            search_form=search_form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/status/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def ads_packages_change_status(public_id):
    data = AdsPackages.find_by_public_id(public_id=public_id)
    if data:
        if not data.is_active():
            data.activate_ads_package()
        elif data.is_active():
            data.deactivate_ads_package()
        flash(
            _default_messages["CHANGE_STATUS"][0],
            _default_messages["CHANGE_STATUS"][1],
        )
        return redirect(url_for("back.ads_package"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.ads_package"))


@bp.route(f"/{PREFIX}/create", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".ads_package_create", _("Create Ads Package"))
def ads_package_create():
    form = CreateAdsPackageForm()
    if form.validate_on_submit():
        new_ads_package = AdsPackages(
            package_name=form.package_name.data,
            price=form.price.data,
            discount_price=form.discount_price.data,
            duration=day_to_second(form.duration.data),
            description=form.description.data,
            status=form.status.data,
            created_at=datetime.now(),
            updated_at=None,
        )
        new_ads_package.create_ads_packages()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.ads_package"))
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "create.html",
            page_title=PREFIX,
            create_ads_package_form=form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/update/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def ads_package_update(public_id):
    check_exist = AdsPackages.find_by_public_id(public_id=public_id)
    form = UpdateAdsPackageForm(check_exist.package_name)
    if check_exist:
        if form.validate_on_submit():
            check_exist.package_name = form.package_name.data
            check_exist.price = form.price.data
            check_exist.discount_price = form.discount_price.data
            check_exist.duration = day_to_second(form.duration.data)
            check_exist.description = form.description.data
            check_exist.status = form.status.data
            check_exist.updated_at = datetime.now()
            check_exist.update_ads_packages()
            flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
            return redirect(url_for("back.ads_package"))
        form.package_name.data = check_exist.package_name
        form.price.data = check_exist.price
        form.discount_price.data = check_exist.discount_price
        form.duration.data = second_to_day(check_exist.duration)
        form.description.data = check_exist.description
        form.status.data = check_exist.status
        return (
            render_template(
                bp.__getattribute__("name") + f"/{PREFIX}/" + "create.html",
                page_title=PREFIX,
                create_ads_package_form=form,
            ),
            tmpl_show_menu(),
        )
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.ads_package"))


@bp.route(f"/{PREFIX}/delete/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def ads_package_delete(public_id):
    check_exist = AdsPackages.find_by_public_id(public_id=public_id)
    if check_exist:
        check_exist.delete_ads_packages()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.ads_package"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.ads_package"))


@bp.route(f"/{PREFIX}/search")
@auth_required("session")
@roles_required("Admin")
def ads_package_search():
    search_form = SearchAdsPackageForm()
    if not search_form.validate():
        return redirect(url_for("back.ads_package"))
    page = request.args.get("page", 1, type=int)
    ads_package, total = AdsPackages.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "back.ads_package_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "back.ads_package_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        ads_package=ads_package,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )
