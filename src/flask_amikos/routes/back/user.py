from flask import (
    url_for,
    render_template,
    redirect,
    flash,
    request,
    render_template_string,
    current_app,
)
from flask_security import auth_required, roles_required, hash_password, current_user
from flask_breadcrumbs import register_breadcrumb
from flask_babel import _
from src.flask_amikos.routes.back import back as bp
from src.flask_amikos.models.user import User
from src.flask_amikos.forms.back.user import (
    ExtendedRegisterForm,
    SearchUserForm,
    ProfileUserFormBack,
)
from src.flask_amikos import user_datastore, db
from src.flask_amikos.util.message import _default_messages

PREFIX = "user"


def tmpl_show_menu():
    return render_template_string(
        """
        {%- for item in current_menu.children %}
            {% if item.active %}*{% endif %}{{ item.text }}
        {% endfor -%}
        """
    )


@bp.route(f"/{PREFIX}")
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".user", _("User"))
def user():
    page = request.args.get("page", 1, type=int)
    pagination = User.get_all(page=page)
    users = pagination.items
    search_form = SearchUserForm()
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
            page_title=PREFIX,
            users=users,
            pagination=pagination,
            ud=user_datastore,
            search_form=search_form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/create", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".user_create", _("Create User"))
def user_create():
    form = ExtendedRegisterForm()
    if form.validate_on_submit():
        """create new user"""
        new_user = user_datastore.create_user(
            email=form.email.data, password=hash_password(form.password.data)
        )
        if form.role.data:
            """if role form exist, so add role to user"""
            user_datastore.add_role_to_user(new_user, form.role.data)
        db.session.commit()
        """ send email confirmation """
        """ MailUtil.send_mail(
            template=render_template("security/email/welcome.html"),
            subject=current_app.config["SECURITY_EMAIL_SUBJECT_REGISTER"],
            recipient=new_user.email,
            sender=current_app.config["SECURITY_EMAIL_SENDER"],
            body="security/email/welcome.txt",
            html="security/email/welcome.html",
            user=new_user,
        ) """

        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.user"))
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "create.html",
            page_title=PREFIX,
            register_user_form=form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/active/<string:email>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def user_change_active(email):
    check_user_exist = user_datastore.find_user(email=email)
    if check_user_exist:
        if check_user_exist.active:
            user_datastore.deactivate_user(check_user_exist)
            db.session.commit()
        else:
            user_datastore.activate_user(check_user_exist)
            db.session.commit()
        flash(
            _default_messages["ACTIVATE_USER"][0], _default_messages["ACTIVATE_USER"][1]
        )
        return redirect(url_for("back.user"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.user"))


@bp.route(f"/{PREFIX}/delete/<string:email>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def user_delete(email):
    check_user_exist = user_datastore.find_user(email=email)
    if check_user_exist:
        user_datastore.delete_user(check_user_exist)
        db.session.commit()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.user"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.user"))


@bp.route(f"/{PREFIX}/reset-access/<string:email>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def user_reset_access(email):
    check_user_exist = user_datastore.find_user(email=email)
    if check_user_exist:
        user_datastore.reset_user_access(check_user_exist)
        db.session.commit()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.user"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.user"))


@bp.route(f"/{PREFIX}/search")
@auth_required("session")
@roles_required("Admin")
def user_search():
    search_form = SearchUserForm()
    if not search_form.validate():
        return redirect(url_for("back.user"))
    page = request.args.get("page", 1, type=int)
    users, total = User.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "back.user_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "back.user_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        users=users,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )


@bp.route("/profile", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def profile():
    form = ProfileUserFormBack()
    user = User.get(current_user.id)
    if form.validate_on_submit():
        user.first_name = form.first_name.data
        user.last_name = form.last_name.data
        user.update_user()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.profile"))
    form.first_name.data = current_user.first_name
    form.last_name.data = current_user.last_name
    form.email.data = current_user.email
    return render_template(
        bp.__getattribute__("name") + "/profile.html",
        page_title=_("profile"),
        profile_form=form,
    )
