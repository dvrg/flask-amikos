from http import HTTPStatus
from flask import (
    url_for,
    render_template,
    redirect,
    request,
    render_template_string,
)
from flask.globals import current_app
from flask_security import auth_required, roles_required
from flask_breadcrumbs import register_breadcrumb
from flask_babel import _
from src.flask_amikos.routes.back import back as bp
from src.flask_amikos.models.transaction import Transaction, StatusTransaction
from src.flask_amikos.forms.back.transaction import SearchTransactionForm
from src.flask_amikos.util.util import (
    check_status_midtrans,
    midtrans_verify_signature_key,
)

PREFIX = "transaction"


def tmpl_show_menu():
    return render_template_string(
        """
        {%- for item in current_menu.children %}
            {% if item.active %}*{% endif %}{{ item.text }}
        {% endfor -%}
        """
    )


@bp.route(f"/{PREFIX}")
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".transaction", _("Transaction"))
def transaction():
    page = request.args.get("page", 1, type=int)
    pagination = Transaction.get_all(
        transaction_status=[
            data.name for data in StatusTransaction.get() if data.name != "created"
        ],
        page=page,
    )
    transaction = pagination.items
    search_form = SearchTransactionForm()
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
            page_title=PREFIX,
            pagination=pagination,
            transaction=transaction,
            search_form=search_form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/search")
@auth_required("session")
@roles_required("Admin")
def transaction_search():
    search_form = SearchTransactionForm()
    if not search_form.validate():
        return redirect(url_for("back.transaction"))
    page = request.args.get("page", 1, type=int)
    transaction, total = Transaction.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "back.transaction_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "back.transaction_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        transaction=transaction,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )


@bp.route(f"/{PREFIX}/status/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def transaction_status(public_id):
    response = check_status_midtrans(parameter_id=public_id)

    # check response code 200, 201, 202, 407
    if (
        int(response["status_code"]) == HTTPStatus.OK
        or int(response["status_code"]) == HTTPStatus.CREATED
        or int(response["status_code"]) == HTTPStatus.ACCEPTED
        or response["status_code"] == "407"
    ):
        transaction_id = response["transaction_id"]
        gross_amount = response["gross_amount"]
        signature_key = response["signature_key"]

        # using real order_id from response
        order_id = response["order_id"]
        status_code = response["status_code"]
        # verify signature key
        verify_signature_key = midtrans_verify_signature_key(
            order_id, status_code, gross_amount
        )
        if signature_key == verify_signature_key:
            transaction = Transaction.find_by_public_id(public_id=public_id)
            return (
                render_template(
                    bp.__getattribute__("name") + f"/{PREFIX}/" + "detail.html",
                    page_title=PREFIX,
                    order_id=transaction.public_id,
                    gross_amount=transaction.ads.ads_cost,
                    transaction_status=transaction.transaction_status,
                    payment_type=transaction.payment_method,
                    transaction_id=transaction_id,
                    transaction_time=transaction.updated_at,
                    mobile_number=transaction.ads.mobile_number,
                    email=transaction.ads.email,
                    ads_packages=transaction.ads.ads_packages,
                    ads_cost=transaction.ads.ads_cost,
                ),
                tmpl_show_menu(),
            )
        else:
            return render_template(
                bp.__getattribute__("name") + "/" + "custome-error.html",
                status_code=401,
                status_message="Invalid Signature Key",
            )
    else:
        return render_template(
            bp.__getattribute__("name") + "/" + "custome-error.html",
            status_code=response["status_code"],
            status_message=response["status_message"],
        )
