from datetime import datetime
from flask import (
    url_for,
    render_template,
    redirect,
    flash,
    request,
    render_template_string,
)
from flask.globals import current_app
from flask_security import auth_required, roles_required
from flask_breadcrumbs import register_breadcrumb
from flask_babel import _
from src.flask_amikos.routes.back import back as bp
from src.flask_amikos.models.ads import Ads
from src.flask_amikos.models.location import Location
from src.flask_amikos.forms.back.ads import ExtendedUpdateAdsForm, SearchAdsForm
from src.flask_amikos.util.message import _default_messages
from src.flask_amikos.util.util import remove_unused_char

PREFIX = "ads"


def tmpl_show_menu():
    return render_template_string(
        """
        {%- for item in current_menu.children %}
            {% if item.active %}*{% endif %}{{ item.text }}
        {% endfor -%}
        """
    )


@bp.route(f"/{PREFIX}")
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".ads", _("Ads"))
def ads():
    page = request.args.get("page", 1, type=int)
    pagination = Ads.get_all(status=True, page=page)
    ads = pagination.items
    search_form = SearchAdsForm()
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
            page_title=PREFIX,
            pagination=pagination,
            ads=ads,
            search_form=search_form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/status/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def ads_change_status(public_id):
    data = Ads.find_by_public_id(public_id=public_id)
    if data:
        if not data.is_active():
            data.activate_ads()
        elif data.is_active():
            data.deactivate_ads()
        flash(
            _default_messages["CHANGE_STATUS"][0],
            _default_messages["CHANGE_STATUS"][1],
        )
        return redirect(url_for("back.ads"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.ads"))


@bp.route(f"/{PREFIX}/update/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def ads_update(public_id):
    check_exist = Ads.find_by_public_id(public_id=public_id)
    form = ExtendedUpdateAdsForm(
        check_exist.brand_name,
        check_exist.mobile_number,
        check_exist.whatsapp_number,
        check_exist.email,
    )
    if check_exist:
        if form.validate_on_submit():
            check_exist.property_type = form.property_type.data
            check_exist.brand_name = form.brand_name.data
            check_exist.gender_of_occupant = form.gender_of_occupant.data
            check_exist.number_bedroom_available = form.number_bedroom_available.data
            check_exist.type_of_bathroom = form.type_of_bathroom.data
            check_exist.number_of_bathroom = form.number_of_bathroom.data
            check_exist.location_id = form.location_close_to.data.id
            check_exist.address = form.address.data
            check_exist.rental_price = remove_unused_char(form.rental_price.data)
            check_exist.duration_of_ease = form.duration_of_ease.data
            check_exist.mobile_number = remove_unused_char(form.mobile_number.data)
            check_exist.whatsapp_number = remove_unused_char(form.whatsapp_number.data)
            check_exist.additional_info = form.additional_info.data
            check_exist.email = form.email.data
            check_exist.feedback = form.feedback.data
            check_exist.accept_policy = form.accept_privacy_policy.data
            check_exist.status = form.status.data
            check_exist.updated_at = datetime.now()

            duration = 0
            cost = 0
            for a in form.ads_packages.data:
                duration = duration + a.duration
                cost = cost + a.discount_price
                check_exist.ads_packages.append(a)

            check_exist.remaining_time = duration
            check_exist.ads_cost = cost

            for a in form.amenities.data:
                check_exist.amenities.append(a)

            check_exist.update_ads()
            flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
            return redirect(url_for("back.ads"))
        form.property_type.data = check_exist.property_type
        form.brand_name.data = check_exist.brand_name
        form.gender_of_occupant.data = check_exist.gender_of_occupant
        form.number_bedroom_available.data = check_exist.number_bedroom_available
        form.type_of_bathroom.data = check_exist.type_of_bathroom
        form.number_of_bathroom.data = check_exist.number_of_bathroom
        form.address.data = check_exist.address
        form.rental_price.data = check_exist.rental_price
        form.duration_of_ease.data = check_exist.duration_of_ease
        form.mobile_number.data = check_exist.mobile_number
        form.whatsapp_number.data = check_exist.whatsapp_number
        form.additional_info.data = check_exist.additional_info
        form.email.data = check_exist.email
        form.feedback.data = check_exist.feedback
        form.accept_privacy_policy.data = check_exist.accept_policy
        form.status.data = check_exist.status
        location_obj = Location.query.get(check_exist.location.id)
        form.location_close_to.data = location_obj
        form.amenities.data = [a for a in check_exist.amenities]
        form.ads_packages.data = [p for p in check_exist.ads_packages]
        return (
            render_template(
                bp.__getattribute__("name") + f"/{PREFIX}/" + "create.html",
                page_title=PREFIX,
                create_ads_form=form,
            ),
            tmpl_show_menu(),
        )
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.ads"))


@bp.route(f"/{PREFIX}/delete/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def ads_delete(public_id):
    check_exist = Ads.find_by_public_id(public_id=public_id)
    if check_exist:
        check_exist.delete_ads()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.ads"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.ads"))


@bp.route(f"/{PREFIX}/search")
@auth_required("session")
@roles_required("Admin")
def ads_search():
    search_form = SearchAdsForm()
    if not search_form.validate():
        return redirect(url_for("back.ads"))
    page = request.args.get("page", 1, type=int)
    ads, total = Ads.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "back.ads_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "back.ads_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        ads=ads,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )
