from datetime import datetime
from flask import (
    url_for,
    render_template,
    redirect,
    flash,
    request,
    render_template_string,
    current_app,
)
from flask_security import auth_required, roles_required
from flask_breadcrumbs import register_breadcrumb
from flask_babel import _
from src.flask_amikos.routes.back import back as bp
from src.flask_amikos.models.location import Location
from src.flask_amikos.forms.back.location import (
    CreateLocationForm,
    UpdateLocationForm,
    SearchLocationForm,
)
from src.flask_amikos.util.message import _default_messages

PREFIX = "location"


def tmpl_show_menu():
    return render_template_string(
        """
        {%- for item in current_menu.children %}
            {% if item.active %}*{% endif %}{{ item.text }}
        {% endfor -%}
        """
    )


@bp.route(f"/{PREFIX}")
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".location", _("Location"))
def location():
    page = request.args.get("page", 1, type=int)
    pagination = Location.get_all(page=page)
    location = pagination.items
    search_form = SearchLocationForm()
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
            page_title=PREFIX,
            pagination=pagination,
            location=location,
            search_form=search_form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/status/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def location_change_status(public_id):
    data = Location.find_by_public_id(public_id=public_id)
    if data:
        if not data.is_active():
            data.activate_location()
        elif data.is_active():
            data.deactivate_location()
        flash(
            _default_messages["CHANGE_STATUS"][0],
            _default_messages["CHANGE_STATUS"][1],
        )
        return redirect(url_for("back.location"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.location"))


@bp.route(f"/{PREFIX}/create", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".location_create", _("Create Location"))
def location_create():
    form = CreateLocationForm()
    if form.validate_on_submit():
        new_location = Location(
            location_name=form.location_name.data,
            status=form.status.data,
            created_at=datetime.now(),
            updated_at=None,
        )
        new_location.create_location()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.location"))
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "create.html",
            page_title=PREFIX,
            create_location_form=form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/update/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def location_update(public_id):
    check_exist = Location.find_by_public_id(public_id=public_id)
    form = UpdateLocationForm(check_exist.location_name)
    if check_exist:
        if form.validate_on_submit():
            check_exist.location_name = form.location_name.data
            check_exist.status = form.status.data
            check_exist.updated_at = datetime.now()
            check_exist.update_location()
            flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
            return redirect(url_for("back.location"))
        form.location_name.data = check_exist.location_name
        form.status.data = check_exist.status
        return (
            render_template(
                bp.__getattribute__("name") + f"/{PREFIX}/" + "create.html",
                page_title=PREFIX,
                create_location_form=form,
            ),
            tmpl_show_menu(),
        )
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.location"))


@bp.route(f"/{PREFIX}/delete/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def location_delete(public_id):
    check_exist = Location.find_by_public_id(public_id=public_id)
    if check_exist:
        check_exist.delete_location()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.location"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.location"))


@bp.route(f"/{PREFIX}/search")
@auth_required("session")
@roles_required("Admin")
def location_search():
    search_form = SearchLocationForm()
    if not search_form.validate():
        return redirect(url_for("back.location"))
    page = request.args.get("page", 1, type=int)
    location, total = Location.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "back.location_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "back.location_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        location=location,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )
