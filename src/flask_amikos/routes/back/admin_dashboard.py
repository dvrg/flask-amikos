from flask import render_template
from flask_security import auth_required, roles_required
from flask_breadcrumbs import register_breadcrumb
from flask_babel import _
from src.flask_amikos.models.user import User
from src.flask_amikos.routes.back import back as bp


PREFIX = "admin-dashboard"


@bp.route("/")
@bp.route(f"/{PREFIX}")
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".", _("Admin Dashboard"))
def admin_dashboard():
    return render_template(
        bp.__getattribute__("name") + "/base_back.html", page_title=_("layout page")
    )


@bp.route("/starter")
@auth_required("session")
@roles_required("Admin")
def starter():
    return render_template(
        bp.__getattribute__("name") + "/starter.html", page_title=_("stater page")
    )
