from datetime import datetime
from flask import (
    url_for,
    render_template,
    redirect,
    flash,
    request,
    render_template_string,
    current_app,
)
from flask_security import auth_required, roles_required
from flask_breadcrumbs import register_breadcrumb
from flask_babel import _
from src.flask_amikos.routes.back import back as bp
from src.flask_amikos.models.amenities import Amenities
from src.flask_amikos.forms.back.amenities import (
    CreateAmenitiesForm,
    UpdateAmenitiesForm,
    SearchAmenitiesForm,
)
from src.flask_amikos.util.message import _default_messages

PREFIX = "amenities"


def tmpl_show_menu():
    return render_template_string(
        """
        {%- for item in current_menu.children %}
            {% if item.active %}*{% endif %}{{ item.text }}
        {% endfor -%}
        """
    )


@bp.route(f"/{PREFIX}")
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".amenities", _("Amenities"))
def amenities():
    page = request.args.get("page", 1, type=int)
    pagination = Amenities.get_all(page=page)
    amenities = pagination.items
    search_form = SearchAmenitiesForm()
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
            page_title=PREFIX,
            pagination=pagination,
            amenities=amenities,
            search_form=search_form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/status/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def amenities_change_status(public_id):
    data = Amenities.find_by_public_id(public_id=public_id)
    if data:
        if not data.is_active():
            data.activate_amenities()
        elif data.is_active():
            data.deactivate_amenities()
        flash(
            _default_messages["CHANGE_STATUS"][0],
            _default_messages["CHANGE_STATUS"][1],
        )
        return redirect(url_for("back.amenities"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.amenities"))


@bp.route(f"/{PREFIX}/create", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".amenities_create", _("Create Amenities"))
def amenities_create():
    form = CreateAmenitiesForm()
    if form.validate_on_submit():
        new_amenities = Amenities(
            amenities_name=form.amenities_name.data,
            description=form.description.data,
            status=form.status.data,
            created_at=datetime.now(),
            updated_at=None,
        )
        new_amenities.create_amenities()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.amenities"))
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "create.html",
            page_title=PREFIX,
            create_amenities_form=form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/update/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def amenities_update(public_id):
    check_exist = Amenities.find_by_public_id(public_id=public_id)
    form = UpdateAmenitiesForm(check_exist.amenities_name)
    if check_exist:
        if form.validate_on_submit():
            check_exist.amenities_name = form.amenities_name.data
            check_exist.description = form.description.data
            check_exist.status = form.status.data
            check_exist.updated_at = datetime.now()
            check_exist.update_amenities()
            flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
            return redirect(url_for("back.amenities"))
        form.amenities_name.data = check_exist.amenities_name
        form.description.data = check_exist.description
        form.status.data = check_exist.status
        return (
            render_template(
                bp.__getattribute__("name") + f"/{PREFIX}/" + "create.html",
                page_title=PREFIX,
                create_amenities_form=form,
            ),
            tmpl_show_menu(),
        )
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.amenities"))


@bp.route(f"/{PREFIX}/delete/<string:public_id>", methods=["GET", "POST"])
@auth_required("session")
@roles_required("Admin")
def amenities_delete(public_id):
    check_exist = Amenities.find_by_public_id(public_id=public_id)
    if check_exist:
        check_exist.delete_amenities()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("back.amenities"))
    else:
        flash(_default_messages["NOT_FOUND"][0], _default_messages["NOT_FOUND"][1])
        return redirect(url_for("back.amenities"))


@bp.route(f"/{PREFIX}/search")
@auth_required("session")
@roles_required("Admin")
def amenities_search():
    search_form = SearchAmenitiesForm()
    if not search_form.validate():
        return redirect(url_for("back.amenities"))
    page = request.args.get("page", 1, type=int)
    amenities, total = Amenities.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "back.amenities_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "back.amenities_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        amenities=amenities,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )
