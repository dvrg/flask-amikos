""" 
Blueprint for backend-page / admin panel of application.
Import here all routes for backend-page / admin panel application.
"""
from flask import Blueprint
from flask_breadcrumbs import default_breadcrumb_root

back = Blueprint(
    "back",
    __name__,
    url_prefix="/back",
)

default_breadcrumb_root(
    back,
    ".",
)

from . import errors
from . import admin_dashboard
from . import role
from . import user
from . import ads_package
from . import amenities
from . import location
from . import property
from . import ads
from . import order
from . import transaction
