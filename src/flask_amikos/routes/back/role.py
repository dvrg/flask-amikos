from flask import render_template, request, current_app, url_for, redirect
from flask_security import auth_required, roles_required
from flask_breadcrumbs import register_breadcrumb
from flask_babel import _
from src.flask_amikos.forms.back.role import SearchRoleForm
from src.flask_amikos.routes.back import back as bp
from src.flask_amikos.models.role import Role

PREFIX = "role"


@bp.route(f"/{PREFIX}")
@auth_required("session")
@roles_required("Admin")
@register_breadcrumb(bp, ".role", _("Role"))
def role():
    page = request.args.get("page", 1, type=int)
    pagination = Role.get_all(page=page)
    roles = pagination.items
    search_form = SearchRoleForm()
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        page_title=PREFIX,
        roles=roles,
        pagination=pagination,
        search_form=search_form,
    )


@bp.route(f"/{PREFIX}/search")
@auth_required("session")
@roles_required("Admin")
def role_search():
    search_form = SearchRoleForm()
    if not search_form.validate():
        return redirect(url_for("back.role"))
    page = request.args.get("page", 1, type=int)
    roles, total = Role.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "back.role_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "back.role_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        roles=roles,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )
