"""
Routes for homepage / front-page. You can place all here or separate by module.
"""
from http import HTTPStatus
from datetime import datetime, timedelta
from flask import render_template, request, flash, url_for, current_app, redirect, jsonify
from flask_security import auth_required, roles_accepted, current_user
from flask_babel import lazy_gettext as _
from flask_sqlalchemy import get_debug_queries
from midtransclient import Snap
from src.flask_amikos.routes.front import front as bp
from src.flask_amikos import csrf
from src.flask_amikos.forms.front.ads import CreateAdsForm, SearchAdsForm
from src.flask_amikos.models.ads import Ads
from src.flask_amikos.models.transaction import Transaction
from src.flask_amikos.util.message import _default_messages
from src.flask_amikos.util.util import (
    remove_unused_char,
    midtrans_verify_signature_key,
    check_status_midtrans,
)
from src.flask_amikos.util.email import send_email


@bp.after_app_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= current_app.config["SLOW_DB_QUERY_TIME"]:
            current_app.logger.warning(
                "Slow query: %s\nParameters: %s\nDuration: %fs\nContext: %s\n"
                % (query.statement, query.parameters, query.duration, query.context)
            )
    return response


PREFIX = "home"


@bp.route("/")
def home_page():
    """
    Merupakan halaman home page / utama dari Amikos.
    """
    page = request.args.get("page", 1, type=int)
    pagination = Ads.get_all(status=True, page=page)
    ads = pagination.items
    search_form = SearchAdsForm()
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        page_title=PREFIX,
        ads=ads,
        pagination=pagination,
        search_form=search_form,
    )


@bp.route("/cari")
def home_ads_search():
    """
    Melakukan pencarian data iklan berdasarkan `brand_name` dan `status`
    dimana hanya menampilkan hasil pencarian iklan dengan `status` True
    """
    search_form = SearchAdsForm()
    if not search_form.validate():
        return redirect(url_for("front.home_page"))
    page = request.args.get("page", 1, type=int)
    ads, total = Ads.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "front.home_ads_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "front.home_ads_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "index.html",
        ads=ads,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=_("Search ") + search_form.query.data,
    )


@bp.route("/iklan", methods=["GET", "POST"])
@auth_required("session")
@roles_accepted("Admin", "Editor")
def home_ads():
    """Merupakan halaman untuk membuat sebuah iklan baru

    Argumen:
        Tidak ada

    Kembalian:
        Menampilkan halaman transaksi dengan parameter berupa
        `public_id` dari iklan
    """
    form = CreateAdsForm(email=current_user.email)
    if form.validate_on_submit():
        new_ads = Ads(
            property_type=form.property_type.data,
            brand_name=form.brand_name.data,
            gender_of_occupant=form.gender_of_occupant.data,
            number_bedroom_available=form.number_bedroom_available.data,
            type_of_bathroom=form.type_of_bathroom.data,
            number_of_bathroom=form.number_of_bathroom.data,
            location_id=form.location_close_to.data.id,
            address=form.address.data,
            rental_price=remove_unused_char(form.rental_price.data),
            duration_of_ease=form.duration_of_ease.data,
            mobile_number=remove_unused_char(form.mobile_number.data),
            whatsapp_number=remove_unused_char(form.whatsapp_number.data),
            additional_info=form.additional_info.data,
            email=form.email.data,
            feedback=form.feedback.data,
            accept_policy=form.accept_privacy_policy.data,
            status=False,
            created_at=datetime.now(),
            updated_at=None,
            user_id=current_user.id,
        )
        duration = 0
        cost = 0
        for a in form.ads_packages.data:
            duration = duration + a.duration
            cost = cost + a.discount_price
            new_ads.ads_packages.append(a)

        new_ads.remaining_time = duration
        new_ads.ads_cost = cost

        for a in form.amenities.data:
            new_ads.amenities.append(a)

        new_ads.create_ads()

        new_transaction = Transaction(
            ads_id=new_ads.id,
            payment_method=None,
            transaction_status="created",
            created_at=datetime.now(),
            updated_at=None,
        )
        new_transaction.create_transaction()

        """
        Send email for user created information
        """
        send_email(
            new_ads.email,
            "Pendaftaran Akun Berhasil!",
            "front/email/user_registration",
            data=new_ads,
        )

        """
        Send email for ads created information
        """
        send_email(
            new_ads.email,
            "Iklan Berhasil Dibuat!",
            "front/email/ads_create",
            data=new_ads,
        )
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(
            url_for("front.home_transaction_detail", public_id=new_transaction.public_id)
        )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "ads.html",
        page_title=PREFIX,
        create_ads_form=form,
    )


@bp.route("/iklan/<string:public_id>")
def home_ads_detail(public_id):
    """
    Menampilkan detail iklan baik status iklan `True` maupun `False` dengan parameter
    `:public_id` dari iklan. Pada halaman ini juga menampilkan iklan terkait dimana
    iklan terkait yang di maksud, di kelompokkan berdasarkan `:location_id`

    Argumen:
        `public_id` dari iklan

    Kembalian:
        Halaman `ads-detail.html`
    """
    check_exist = Ads.find_by_public_id(public_id=public_id)
    if check_exist:
        similiar_around = Ads.find_by_location_close_to(
            location_id=check_exist.location_id, ignore_id=public_id, status=True, page=1
        )
        return render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "ads-detail.html",
            ads_detail=check_exist,
            similiar_around=similiar_around.items,
        )


@bp.route("/transaksi/<string:public_id>")
@auth_required("session")
@roles_accepted("Admin", "Editor")
def home_transaction_detail(public_id):
    """Melakukan proses transaksi dengan Midtrans. Baca lebih lanjut pada dokumentasi midtrans
    https://snap-docs.midtrans.com/#request-body-json-parameter

    Argumen:
        `public_id` dari iklan

    Kembalian:
        Halaman status pembayaran berdasarkan `public_id` dari iklan,
        dimana data status pembayaran langsung diambil dari Midtrans.
    """
    snap = Snap(
        is_production=current_app.config["MIDTRANS_PRODUCTION"],
        server_key=current_app.config["MIDTRANS_SERVER_KEY"],
        client_key=current_app.config["MIDTRANS_CLIENT_KEY"],
    )

    check_exist = Transaction.find_by_public_id(public_id=public_id)

    # check if data with public id is exist
    if check_exist:

        # get response to midtrans API, are the transaction with order_id exist
        response = check_status_midtrans(public_id)

        # if not 200, 201, 202, 407 the response
        if not (
            int(response["status_code"]) == HTTPStatus.OK
            or int(response["status_code"]) == HTTPStatus.CREATED
            or int(response["status_code"]) == HTTPStatus.ACCEPTED
            or response["status_code"] == "407"
        ):

            # create :item_details data based on ads_packages
            def get_items_detail():
                items_detail = []
                for ads_package in check_exist.ads.ads_packages:
                    data = {
                        "id": ads_package.public_id,
                        "price": int(ads_package.discount_price),
                        "quantity": 1,
                        "name": ads_package.package_name,
                        "category": "Iklan",
                        "merchant_name": current_app.config["APP_NAME"],
                    }
                    items_detail.append(data)
                return items_detail

            # create transaction token process
            transaction_token = snap.create_transaction_token(
                {
                    "transaction_details": {
                        "order_id": check_exist.public_id,
                        "gross_amount": int(check_exist.ads.ads_cost),
                    },
                    "item_details": get_items_detail(),
                    "customer_details": {
                        "first_name": check_exist.ads.user.first_name,
                        "last_name": check_exist.ads.user.last_name,
                        "email": check_exist.ads.email,
                        "phone": check_exist.ads.mobile_number,
                        "billing_address": {
                            "first_name": current_user.first_name,
                            "last_name": current_user.last_name,
                            "email": check_exist.ads.email,
                            "phone": check_exist.ads.mobile_number,
                            "address": check_exist.ads.address,
                            "country_code": "IDN",
                        },
                    },
                    "credit_card": {"save_card": True, "secure": True},
                }
            )

            return render_template(
                bp.__getattribute__("name") + f"/{PREFIX}/" + "transaction.html",
                page_title=_("Transaction ") + public_id,
                transaction=check_exist,
                token=transaction_token,
                client_key=snap.api_config.client_key,
            )

        # if the response == 200, 201, 202 or anything else
        # throw in paynment status page to see the payment status
        else:
            return redirect(url_for("front.home_payment_status", order_id=public_id))


@csrf.exempt
@bp.route("/payment/notification", methods=["GET", "POST"])
def home_payment_notification():
    """Callback untuk menerima perubahan status pembayaran dari Midtrans. Informasi lebih lanjut
    https://docs.midtrans.com/en/after-payment/http-notification?id=sample-in-curl

    Argument:

    Kembalian:
        HTTP Status dengan `status_code` 200 (Sukses) ataupun 401 (Invalid Signature)
    """
    # accept request
    response = request.json

    order_id = response["order_id"]
    transaction_status = response["transaction_status"]
    status_code = response["status_code"]
    gross_amount = response["gross_amount"]
    signature_key = response["signature_key"]
    payment_type = response["payment_type"]

    # verify signature
    verify_signature_key = midtrans_verify_signature_key(
        order_id, status_code, gross_amount
    )

    # fraud_status not in all payment_type
    if "fraud_status" in response:
        fraud_status = response["fraud_status"]

    # make sure data is exist
    check_exist = Transaction.find_by_public_id(public_id=order_id)
    if check_exist:
        if signature_key == verify_signature_key:
            set_transaction_status = ""

            if transaction_status == "capture":
                if fraud_status == "challenge":
                    set_transaction_status = "challenge"
                elif fraud_status == "accept":
                    set_transaction_status = "success"

                    # change status ads to true and set start/end date of ads
                    check_exist.ads.status = True
                    start_date = datetime.now()
                    check_exist.ads.start_date = start_date
                    check_exist.ads.end_date = start_date + timedelta(
                        seconds=check_exist.ads.remaining_time
                    )
            elif transaction_status == "settlement":
                set_transaction_status = "success"

                # change status ads to true and set start/end date of ads
                check_exist.ads.status = True
                start_date = datetime.now()
                check_exist.ads.start_date = start_date
                check_exist.ads.end_date = start_date + timedelta(
                    seconds=check_exist.ads.remaining_time
                )
            elif (
                transaction_status == "cancel"
                or transaction_status == "deny"
                or transaction_status == "expire"
            ):
                set_transaction_status = "failure"
            elif transaction_status == "pending":
                set_transaction_status = "pending"

            check_exist.payment_method = payment_type
            check_exist.transaction_status = set_transaction_status
            check_exist.updated_at = datetime.now()
            check_exist.update_transaction()
            return (
                jsonify({"success": True}),
                200,
            )
        return (
            jsonify(
                {
                    "success": False,
                    "status_message": "Invalid Signature Key",
                    "status_code": 401,
                }
            ),
            401,
        )


@csrf.exempt
@bp.route("/payment/status", methods=["GET", "POST"])
@auth_required("session")
@roles_accepted("Admin", "Editor")
def home_payment_status():
    """Fungsi untuk pengecekan status transaksi dari Midtrans. Lebih lanjut tentang `status_code` dari Midtrans :
    https://api-docs.midtrans.com/#status-code

    Argument
        Berupa query parameter bisa `order_id` yang didapatkan dari Midtrans ataupun `id` yang di dapatkan dari Amikos

    Kembalian
        - Jika berhasil / ditemukan `order_id` atau `id` maka tampilkan halaman status pemesanan
        - Jika `signature_key` tidak sesuai, maka halaman Error 401
        - Jika selain itu maka halaman error dengan `status_message` dan `status_code` custome sesuai error
    """
    # get order_id from query parameter from callback
    parameter_id = ""
    if request.args.get("order_id", type=str):
        parameter_id = request.args.get("order_id", type=str)
    elif request.args.get("id", type=str):
        parameter_id = request.args.get("id", type=str)

    # call function to check status order midtrans
    response = check_status_midtrans(parameter_id)

    # check response code 200, 201, 202, 407
    if (
        int(response["status_code"]) == HTTPStatus.OK
        or int(response["status_code"]) == HTTPStatus.CREATED
        or int(response["status_code"]) == HTTPStatus.ACCEPTED
        or response["status_code"] == "407"
    ):
        transaction_id = response["transaction_id"]
        gross_amount = response["gross_amount"]
        signature_key = response["signature_key"]

        # using real order_id from response
        order_id = response["order_id"]
        status_code = response["status_code"]

        # verify signature key
        verify_signature_key = midtrans_verify_signature_key(
            order_id, status_code, gross_amount
        )

        # check signature
        if signature_key == verify_signature_key:
            transaction = Transaction.find_by_public_id(order_id)
            return render_template(
                bp.__getattribute__("name") + f"/{PREFIX}/" + "payment-status.html",
                page_title=_("Payment Status ") + order_id,
                order_id=transaction.public_id,
                gross_amount=transaction.ads.ads_cost,
                transaction_status=transaction.transaction_status,
                payment_type=transaction.payment_method,
                transaction_id=transaction_id,
                transaction_time=transaction.updated_at,
                mobile_number=transaction.ads.mobile_number,
                email=transaction.ads.email,
                ads_packages=transaction.ads.ads_packages,
                ads_cost=transaction.ads.ads_cost,
            )
        else:
            return render_template(
                bp.__getattribute__("name") + "/" + "custome-error.html",
                status_code=401,
                status_message="Invalid Signature Key",
            )
    else:
        return render_template(
            bp.__getattribute__("name") + "/" + "custome-error.html",
            status_code=response["status_code"],
            status_message=response["status_message"],
        )


@bp.route("/riwayat-pembaruan")
def home_change_log():
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "change-log.html"
    )


@bp.route("/pertanyaan")
def home_faq():
    return render_template(bp.__getattribute__("name") + f"/{PREFIX}/" + "faq.html")


@bp.route("/tentang")
def home_about():
    return render_template(bp.__getattribute__("name") + f"/{PREFIX}/" + "about.html")


@bp.route("/kebijakan-iklan")
def home_ads_policy():
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "ads-policy.html"
    )


@bp.route("/kebijakan-penggunaan")
def home_tos():
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "terms-of-service.html"
    )


@bp.route("/kebijakan-privasi")
def home_privacy_policy():
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "privacy-policy.html"
    )


@bp.route("/magic-login")
def home_login_less():
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "/magic-login.html"
    )
