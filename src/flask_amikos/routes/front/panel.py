from http import HTTPStatus
from datetime import datetime
from flask import (
    render_template,
    redirect,
    url_for,
    current_app,
    flash,
    render_template_string,
    request,
)
from flask_security import auth_required, roles_accepted, current_user
from flask_breadcrumbs import register_breadcrumb
from flask_babel import _
from src.flask_amikos.routes.front import front as bp
from src.flask_amikos.forms.front.ads import SearchAdsForm
from src.flask_amikos.forms.front.user import ProfileUserFormFront
from src.flask_amikos.forms.front.transaction import SearchTransactionForm
from src.flask_amikos.models.ads import Ads
from src.flask_amikos.models.transaction import Transaction
from src.flask_amikos.models.user import User
from src.flask_amikos.util.email import send_email
from src.flask_amikos.util.message import _default_messages
from src.flask_amikos.util.util import (
    check_status_midtrans,
    midtrans_verify_signature_key,
)

PREFIX = "panel"


def tmpl_show_menu():
    return render_template_string(
        """
        {%- for item in current_menu.children %}
            {% if item.active %}*{% endif %}{{ item.text }}
        {% endfor -%}
        """
    )


@bp.route(f"/{PREFIX}/")
@auth_required("session")
@roles_accepted("Admin", "Editor")
@register_breadcrumb(bp, ".", _("Partner Dashboard"))
def panel_dashboard():
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "dashboard.html",
            page_title=_("Dashboard"),
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/properti")
@auth_required("session")
@roles_accepted("Admin", "Editor")
@register_breadcrumb(bp, ".panel_property", _("My Property"))
def panel_property():
    """Halaman panel properti merupakan menampilkan data dari model `Ads` dengan status `False`

    Argument:
        `current_user` objek

    Kembalian:
        Menampilkan halaman property (sama dengan penampilan halaman ads)
    """
    page = request.args.get("page", 1, type=int)
    pagination = Ads.get_all(status=False, user=current_user, page=page)
    ads = pagination.items
    search_form = SearchAdsForm()
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "ads.html",
            page_title=_("My Property"),
            ads=ads,
            pagination=pagination,
            search_form=search_form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/properti/cari")
@auth_required("session")
@roles_accepted("Admin", "Editor")
def panel_property_search():
    search_form = SearchAdsForm()
    if not search_form.validate():
        return redirect(url_for("front.panel_ads"))
    page = request.args.get("page", 1, type=int)
    ads, total = Ads.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "front.panel_ads_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "front.panel_ads_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "ads.html",
        ads=ads,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )


@bp.route(f"/{PREFIX}/iklan")
@auth_required("session")
@roles_accepted("Admin", "Editor")
@register_breadcrumb(bp, ".panel_ads", _("My Ads"))
def panel_ads():
    """Halaman panel iklan merupakan menampilkan data dari model `Ads` dengan status `True`
    atau dengan kata lain, menampilkan data Iklan Yang Aktif

    Argument:
        `current_user` objek

    Kembalian:
        Menampilkan halaman iklan (sama dengan penampilan halaman ads)
    """
    page = request.args.get("page", 1, type=int)
    pagination = Ads.get_all(status=True, user=current_user, page=page)
    ads = pagination.items
    search_form = SearchAdsForm()
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "ads.html",
            page_title=_("My Ads"),
            ads=ads,
            pagination=pagination,
            search_form=search_form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/iklan/cari")
@auth_required("session")
@roles_accepted("Admin", "Editor")
def panel_ads_search():
    search_form = SearchAdsForm()
    if not search_form.validate():
        return redirect(url_for("front.panel_ads"))
    page = request.args.get("page", 1, type=int)
    ads, total = Ads.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "front.panel_ads_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "front.panel_ads_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "ads.html",
        ads=ads,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )


@bp.route(f"/{PREFIX}/iklan/beli/<string:public_id>", methods=["GET", "POST"])
def panel_ads_buy(public_id):
    """Fungsi untuk membuat sebuah data transaksi baru dari data iklan yang sama, supaya
    bisa berbeda transaksi untuk data iklan yang sama.

    Argumen:
        `public_id` dari iklan

    Kembalian:
        Menampilkan halaman transaksi untuk proses pembayaran
    """
    ads = Ads.find_by_public_id(public_id=public_id)
    if ads:
        new_transaction = Transaction(
            ads_id=ads.id,
            payment_method=None,
            transaction_status="created",
            created_at=datetime.now(),
            updated_at=None,
        )
        new_transaction.create_transaction()

        """
        Send email for ads created information
        """
        send_email(
            ads.email,
            "Iklan Berhasil Dibuat!",
            "front/email/ads_create",
            data=ads,
        )
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(
            url_for("front.home_transaction_detail", public_id=new_transaction.public_id)
        )


@bp.route(f"/{PREFIX}/transaksi")
@auth_required("session")
@roles_accepted("Admin", "Editor")
@register_breadcrumb(bp, ".panel_transaction", _("Transaction"))
def panel_transaction():
    """Halaman yang menampilkan data dari model `transaction` dengan status `created`

     Argument:
        `current_user` objek

    Kembalian:
        Menampilkan halaman transaksi
    """
    page = request.args.get("page", 1, type=int)
    pagination = Transaction.get_all(
        transaction_status=["created"], user=current_user, page=page
    )
    transaction = pagination.items
    search_form = SearchTransactionForm()
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "transaction.html",
            page_title=_("Transaction"),
            pagination=pagination,
            transaction=transaction,
            search_form=search_form,
        ),
        tmpl_show_menu(),
    )


@bp.route(f"/{PREFIX}/transaksi/<string:public_id>")
@auth_required("session")
@roles_accepted("Admin", "Editor")
def panel_transaction_detail(public_id):
    """Halaman yang menampilkan status transaksi dengan melakukan pengecekan langsung ke Midtrans

    Argument:
        `public_id` dari transaksi yang dibuat oleh Amikos

    Kembalian:
        Menampilkan halaman detail transaksi
    """
    response = check_status_midtrans(parameter_id=public_id)

    # check response code 200, 201, 202, 407
    if (
        int(response["status_code"]) == HTTPStatus.OK
        or int(response["status_code"]) == HTTPStatus.CREATED
        or int(response["status_code"]) == HTTPStatus.ACCEPTED
        or response["status_code"] == "407"
    ):
        transaction_id = response["transaction_id"]
        gross_amount = response["gross_amount"]
        signature_key = response["signature_key"]

        # using real order_id from response
        order_id = response["order_id"]
        status_code = response["status_code"]
        # verify signature key
        verify_signature_key = midtrans_verify_signature_key(
            order_id, status_code, gross_amount
        )
        if signature_key == verify_signature_key:
            transaction = Transaction.find_by_public_id(public_id=public_id)
            return (
                render_template(
                    bp.__getattribute__("name") + f"/{PREFIX}/" + "detail.html",
                    page_title=_("Transaction ") + public_id,
                    order_id=transaction.public_id,
                    gross_amount=transaction.ads.ads_cost,
                    transaction_status=transaction.transaction_status,
                    payment_type=transaction.payment_method,
                    transaction_id=transaction_id,
                    transaction_time=transaction.updated_at,
                    mobile_number=transaction.ads.mobile_number,
                    email=transaction.ads.email,
                    ads_packages=transaction.ads.ads_packages,
                    ads_cost=transaction.ads.ads_cost,
                ),
                tmpl_show_menu(),
            )
        else:
            return render_template(
                bp.__getattribute__("name") + "/" + "custome-error.html",
                status_code=401,
                status_message="Invalid Signature Key",
            )
    else:
        return render_template(
            bp.__getattribute__("name") + "/" + "custome-error.html",
            status_code=response["status_code"],
            status_message=response["status_message"],
        )


@bp.route(f"/{PREFIX}/transaksi/search")
@auth_required("session")
@roles_accepted("Admin", "Editor")
def panel_transaction_search():
    search_form = SearchTransactionForm()
    if not search_form.validate():
        return redirect(url_for("front.panel_transaction"))
    page = request.args.get("page", 1, type=int)
    transaction, total = Transaction.search(
        search_form.query.data,
        page,
        current_app.config["POST_PER_PAGE"],
    )
    next_url = (
        url_for(
            "front.panel_transaction_search",
            query=search_form.query.data,
            page=page + 1,
        )
        if total > page * current_app.config["POST_PER_PAGE"]
        else None
    )
    prev_url = (
        url_for(
            "front.panel_transaction_search",
            query=search_form.query.data,
            page=page - 1,
        )
        if page > 1
        else None
    )
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "transaction.html",
        transaction=transaction,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
        page_title=f"Search {search_form.query.data}",
    )


@bp.route(f"/{PREFIX}/profil", methods=["GET", "POST"])
@auth_required("session")
@roles_accepted("Admin", "Editor")
@register_breadcrumb(bp, ".panel_profile", _("Profile"))
def panel_profile():
    """Menampilkan halaman profil dari pengguna dan juga dapat melakukan perubahan data

    Argument:
        `current_user` objek

    Kembalian:
        Menampilkan halaman profile
    """
    form = ProfileUserFormFront()
    user = User.get(current_user.id)
    if form.validate_on_submit():
        user.first_name = form.first_name.data
        user.last_name = form.last_name.data
        user.update_user()
        flash(_default_messages["SUCCESS"][0], _default_messages["SUCCESS"][1])
        return redirect(url_for("front.panel_profile"))
    form.first_name.data = current_user.first_name
    form.last_name.data = current_user.last_name
    form.email.data = current_user.email
    return (
        render_template(
            bp.__getattribute__("name") + f"/{PREFIX}/" + "profile.html",
            page_title=_("Profile "),
            profile_form=form,
        ),
        tmpl_show_menu(),
    )
