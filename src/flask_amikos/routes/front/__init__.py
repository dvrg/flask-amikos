""" 
Blueprint for front-page of application.
Import here all routes for front-page application.
"""
from flask import Blueprint
from flask_breadcrumbs import default_breadcrumb_root

front = Blueprint("front", __name__, url_prefix="/")
default_breadcrumb_root(front, ".")

from . import errors
from . import home
from . import panel
from . import email
