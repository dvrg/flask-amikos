"""
Routes for handle all error page for Blueprint front only!
"""

from http import HTTPStatus
from flask import render_template
from flask_babel import _
from src.flask_amikos.routes.front import front as bp


@bp.errorhandler(HTTPStatus.FORBIDDEN)
def forbidden(e):
    """specific for this page templates, the base template must using security base"""
    return render_template(
        bp.__getattribute__("name") + "/401-permission-denied.html",
        page_title=_("forbidden"),
    )


@bp.errorhandler(HTTPStatus.NOT_FOUND)
def page_not_found(e):
    return render_template(
        bp.__getattribute__("name") + "/404-not-found.html",
        page_title=_("page not found"),
    )


@bp.errorhandler(HTTPStatus.INTERNAL_SERVER_ERROR)
def internal_server_error(e):
    return render_template(
        bp.__getattribute__("name") + "/500-internal-server-error.html",
        page_title=_("internal server error"),
    )
