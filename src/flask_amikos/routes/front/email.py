from datetime import datetime
from flask import render_template
from . import front as bp

PREFIX = "email"


class User:
    email = "dummy@mail.com"
    mobile_number = "+628989664944"
    password = "dummypassword"
    brand_name = "Dummy Indekos"
    ads_number = "e5b8c80ee7744cd7890e18b20f996f31"
    created_at = datetime.now()


@bp.route(f"/{PREFIX}/")
def email():
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "/base_email.html"
    )


@bp.route(f"/{PREFIX}/user-registration")
def email_user_registration():
    user = User()
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "/user_registration.html", data=user
    )


@bp.route(f"/{PREFIX}/ads-create")
def email_ads_create():
    user = User()
    return render_template(
        bp.__getattribute__("name") + f"/{PREFIX}/" + "/ads_create.html", data=user
    )
