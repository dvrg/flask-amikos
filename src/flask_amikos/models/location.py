from datetime import datetime
from uuid import uuid4
from flask import current_app
from src.flask_amikos.extensions import db
from src.flask_amikos.util.models import SearchableMixin

location_ads = db.Table(
    "ads_location",
    db.Column("location_id", db.Integer, db.ForeignKey("location.id"), primary_key=True),
    db.Column("ads_id", db.Integer, db.ForeignKey("ads.id"), primary_key=True),
)


class Location(SearchableMixin, db.Model):
    __searchable__ = ["location_name"]
    __tablename__ = "location"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(
        db.String(36), nullable=False, unique=True, default=lambda: str(uuid4().hex)
    )
    location_name = db.Column(db.String(128), nullable=False)
    status = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    ads = db.relationship("Ads", backref=db.backref("location", lazy=True))

    def __repr__(self):
        return f"<id:{self.id}, public_id:{self.public_id}, name:{self.location_name}>"

    def is_active(self):
        return self.status

    def activate_location(self):
        if not self.status:
            self.status = True
            self.updated_at = datetime.now()
            db.session.commit()
            return True
        return False

    def deactivate_location(self):
        if self.status:
            self.status = False
            self.updated_at = datetime.now()
            db.session.commit()
            return True
        return False

    @classmethod
    def get_all(cls, status=None, page=None):
        """
        Documentation : https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.BaseQuery.paginate
        """
        if status is None:
            return cls.query.paginate(
                page=page,
                per_page=current_app.config["POST_PER_PAGE"],
                error_out=False,
                max_per_page=current_app.config["MAX_PER_PAGE"],
            )
        else:
            return cls.query.filter_by(status=status).paginate(
                page=page,
                per_page=current_app.config["POST_PER_PAGE"],
                error_out=False,
                max_per_page=current_app.config["MAX_PER_PAGE"],
            )

    @classmethod
    def find_by_public_id(cls, public_id):
        return cls.query.filter_by(public_id=public_id).first()

    @classmethod
    def find_by_location_name(cls, location_name):
        return cls.query.filter_by(location_name=location_name).first()

    @classmethod
    def count(cls, status=None):
        if status is None:
            return cls.query.count()
        else:
            return cls.query.filter_by(status=status).count()

    def create_location(self):
        db.session.add(self)
        db.session.commit()

    def update_location(self):
        db.session.commit()

    def delete_location(self):
        db.session.delete(self)
        db.session.commit()
