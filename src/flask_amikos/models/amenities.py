from datetime import datetime
from uuid import uuid4
from flask import current_app
from src.flask_amikos.extensions import db
from src.flask_amikos.util.models import SearchableMixin

amenities_ads = db.Table(
    "amenities_ads",
    db.Column("amenities_id", db.Integer, db.ForeignKey("amenities.id")),
    db.Column("ads_id", db.Integer, db.ForeignKey("ads.id")),
)


class Amenities(SearchableMixin, db.Model):
    __searchable__ = ["amenities_name"]
    __tablename__ = "amenities"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(
        db.String(36), nullable=False, unique=True, default=lambda: str(uuid4().hex)
    )
    amenities_name = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text, nullable=False)
    status = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)

    def __repr__(self):
        return f"<id:{self.id}, public_id:{self.public_id}, name:{self.amenities_name}, status:{self.status}>"

    def is_active(self):
        return self.status

    def activate_amenities(self):
        if not self.status:
            self.status = True
            self.updated_at = datetime.now()
            db.session.commit()
            return True
        return False

    def deactivate_amenities(self):
        if self.status:
            self.status = False
            self.updated_at = datetime.now()
            db.session.commit()
            return True
        return False

    @classmethod
    def get_all(cls, status=None, page=None):
        if status is None:
            return cls.query.paginate(
                page=page,
                per_page=current_app.config["POST_PER_PAGE"],
                error_out=False,
                max_per_page=current_app.config["MAX_PER_PAGE"],
            )
        else:
            return cls.query.filter_by(status=status).paginate(
                page=page,
                per_page=current_app.config["POST_PER_PAGE"],
                error_out=False,
                max_per_page=current_app.config["MAX_PER_PAGE"],
            )

    @classmethod
    def find_by_public_id(cls, public_id):
        return cls.query.filter_by(public_id=public_id).first()

    @classmethod
    def find_by_amenities_name(cls, amenities_name):
        return cls.query.filter_by(amenities_name=amenities_name).first()

    @classmethod
    def count(cls, status=None):
        if status is None:
            return cls.query.count()
        else:
            return cls.query.filter_by(status=status).count()

    def create_amenities(self):
        db.session.add(self)
        db.session.commit()

    def update_amenities(self):
        db.session.commit()

    def delete_amenities(self):
        db.session.delete(self)
        db.session.commit()
