"""Class definition for Role model."""
from flask import current_app
from src.flask_amikos.extensions import db, fsqla
from src.flask_amikos.util.models import SearchableMixin


class Role(SearchableMixin, db.Model, fsqla.FsRoleMixin):
    __searchable__ = ["name"]
    pass

    @classmethod
    def get_all(cls, page=None):
        """func to gets all data for role"""
        return cls.query.paginate(
            page=page,
            per_page=current_app.config["POST_PER_PAGE"],
            error_out=False,
            max_per_page=current_app.config["MAX_PER_PAGE"],
        )
