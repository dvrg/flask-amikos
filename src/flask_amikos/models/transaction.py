import enum
from datetime import datetime
from uuid import uuid4
from flask import current_app
from sqlalchemy import and_
from src.flask_amikos import db
from src.flask_amikos.util.models import SearchableMixin
from src.flask_amikos.models.user import User
from src.flask_amikos.models.ads import Ads


class StatusTransaction(str, enum.Enum):
    created: str = "Created"
    capture: str = "Capture"
    challenge: str = "Challenge"
    settlement: str = "Settlement"
    success: str = "Success"
    pending: str = "Pending"
    expire: str = "Expire"
    cancel: str = "Cancel"
    deny: str = "Deny"
    failure: str = "Failure"
    refund: str = "Refund"
    chargeback: str = "Charge Back"
    partial_refund: str = "Partial Refund"
    partial_chargeback: str = "Partial Chargeback"
    authorize: str = "Authorize"

    @classmethod
    def get(cls):
        return [i for i in cls]


class PaymentMethod(str, enum.Enum):
    credit_card: str = "Card"
    gopay: str = "GoPay"
    qris: str = "QRIS"
    shopeepay: str = "Shopeepay"
    bank_transfer: str = "Bank Transfer"
    echannel: str = "Mandiri Bill"
    bca_klikpay: str = "BCA Klikpay"
    bca_klikbca: str = "KlikBCA"
    cimb_clicks: str = "CIMB Clicks"
    danamon_online: str = "Danamon Online Banking"
    cstore: str = "CS Store"
    akulaku: str = "Akulaku"
    bri_epay: str = "BRI Epay"

    @classmethod
    def get(cls):
        return [i for i in cls]


class Transaction(SearchableMixin, db.Model):
    __searchable__ = ["public_id", "payment_method", "transaction_status"]
    __tablename__ = "transaction"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(
        db.String(36), nullable=False, unique=True, default=lambda: str(uuid4().hex)
    )
    ads_id = db.Column(db.Integer, db.ForeignKey("ads.id"), nullable=False)
    payment_method = db.Column(db.Enum(PaymentMethod), nullable=True)
    transaction_status = db.Column(db.Enum(StatusTransaction))
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)

    def __repr__(self):
        return f"<public_id:{self.public_id}, ads:{self.ads.brand_name}, status:{self.transaction_status.value}>"

    @classmethod
    def get_all(cls, transaction_status=None, user=None, page=None):
        """
        Documentation : https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.BaseQuery.paginate
        """
        if transaction_status is None:
            return cls.query.paginate(
                page=page,
                per_page=current_app.config["POST_PER_PAGE"],
                error_out=False,
                max_per_page=current_app.config["MAX_PER_PAGE"],
            )
        elif transaction_status:
            if user:
                return (
                    cls.query.filter(cls.transaction_status.in_(transaction_status))
                    .outerjoin(Ads, and_(Ads.id == cls.ads_id))
                    .outerjoin(User, and_(User.id == Ads.user_id))
                    .filter(User.id == user.id)
                ).paginate(
                    page=page,
                    per_page=current_app.config["POST_PER_PAGE"],
                    error_out=False,
                    max_per_page=current_app.config["MAX_PER_PAGE"],
                )
            else:
                return cls.query.filter(
                    cls.transaction_status.in_(transaction_status)
                ).paginate(
                    page=page,
                    per_page=current_app.config["POST_PER_PAGE"],
                    error_out=False,
                    max_per_page=current_app.config["MAX_PER_PAGE"],
                )

    @classmethod
    def find_by_public_id(cls, public_id):
        return cls.query.filter_by(public_id=public_id).first_or_404()

    @classmethod
    def find_by_ads_id(cls, ads_id):
        return cls.query.filter_by(ads_id=ads_id).first_or_404()

    @classmethod
    def find_by_transaction_status(cls, transaction_status, page=None):
        return cls.query.filter_by(transaction_status=transaction_status).paginate(
            page=page,
            per_page=current_app.config["POST_PER_PAGE"],
            error_out=False,
            max_per_page=current_app.config["MAX_PER_PAGE"],
        )

    @classmethod
    def count(cls, transaction_status=None):
        if transaction_status is None:
            return cls.query.count()
        else:
            return cls.query.filter_by(transaction_status=transaction_status).count()

    def update_transaction_status(self, transaction_status):
        for status in StatusTransaction.get():
            if transaction_status in status.name:
                self.transaction_status = transaction_status
                self.updated_at = datetime.now()
                db.session.commit()
                return True
            return False

    def update_payment_method(self, payment_type):
        for payment_method in PaymentMethod.get():
            if payment_type in payment_method.name:
                self.payment_method = payment_type
                self.updated_at = datetime.now()
                db.session.commit()
                return True
            return False

    def create_transaction(self):
        db.session.add(self)
        db.session.commit()

    def update_transaction(self):
        db.session.commit()

    def delete_transaction(self):
        db.session.delete(self)
        db.session.commit()
