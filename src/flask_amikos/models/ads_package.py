from uuid import uuid4
from datetime import datetime
from flask import current_app
from src.flask_amikos.extensions import db
from src.flask_amikos.util.models import SearchableMixin


class AdsPackages(SearchableMixin, db.Model):
    __searchable__ = ["package_name"]
    __tablename__ = "ads_packages"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(
        db.String(36), nullable=False, unique=True, default=lambda: str(uuid4().hex)
    )
    package_name = db.Column(db.String(32), nullable=False)
    price = db.Column(db.Float, nullable=False)
    discount_price = db.Column(db.Float, nullable=False)
    duration = db.Column(db.Integer, nullable=False)
    description = db.Column(db.Text, nullable=False)
    status = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)

    def __repr__(self):
        return f"<id:{self.id}, public_id:{self.public_id}, name:{self.package_name}, price:{self.price}, duration:{self.duration}>"

    def is_active(self):
        return self.status

    def activate_ads_package(self):
        if not self.status:
            self.status = True
            self.updated_at = datetime.now()
            db.session.commit()
            return True
        return False

    def deactivate_ads_package(self):
        if self.status:
            self.status = False
            self.updated_at = datetime.now()
            db.session.commit()
            return True
        return False

    @classmethod
    def get_all(cls, status=None, page=None):
        """
        Documentation : https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.BaseQuery.paginate
        """
        if status is None:
            return cls.query.paginate(
                page=page,
                per_page=current_app.config["POST_PER_PAGE"],
                error_out=False,
                max_per_page=current_app.config["MAX_PER_PAGE"],
            )
        else:
            return cls.query.filter_by(status=status).paginate(
                page=page,
                per_page=current_app.config["POST_PER_PAGE"],
                error_out=False,
                max_per_page=current_app.config["MAX_PER_PAGE"],
            )

    @classmethod
    def find_by_public_id(cls, public_id):
        return cls.query.filter_by(public_id=public_id).first()

    @classmethod
    def find_by_package_name(cls, package_name):
        return cls.query.filter_by(package_name=package_name).first()

    @classmethod
    def count(cls, status=None):
        if status is None:
            return cls.query.count()
        else:
            return cls.query.filter_by(status=status).count()

    def create_ads_packages(self):
        db.session.add(self)
        db.session.commit()

    def update_ads_packages(self):
        db.session.commit()

    def delete_ads_packages(self):
        db.session.delete(self)
        db.session.commit()

    # not finish
    def find_or_create(self, package_name=None):
        if package_name is not None:
            self.package_name = package_name
        return self.find_by_package_name(self.package_name) or self.create_ads_packages(
            self
        )
