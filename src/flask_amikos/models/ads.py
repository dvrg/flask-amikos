from datetime import datetime
import enum
from uuid import uuid4
from flask import current_app
from src.flask_amikos.extensions import db
from src.flask_amikos.models.amenities import amenities_ads
from src.flask_amikos.util.models import SearchableMixin


class PropertyType(str, enum.Enum):
    indekos: str = "Indekos"
    kontrakan: str = "Kontrakan"

    @classmethod
    def get(cls):
        return [i for i in cls]


class GenderOfOccupant(str, enum.Enum):
    he: str = "Pria"
    she: str = "Wanita"
    both: str = "Keduanya"

    @classmethod
    def get(cls):
        return [i for i in cls]


class TypeOfBathroom(str, enum.Enum):
    kmd: str = "Kamar Mandi Dalam"
    kml: str = "Kamar Mandi Luar"
    both: str = "Keduanya"

    @classmethod
    def get(cls):
        return [i for i in cls]


class DurationOfEase(str, enum.Enum):
    d1: str = "Per 1 Bulan"
    d2: str = "Per 3 Bulan"
    d3: str = "Per 6 Bulan"
    d4: str = "Per 12 Bulan"

    @classmethod
    def get(cls):
        return [i for i in cls]


ads_ads_packages = db.Table(
    "ads_ads_packages",
    db.Column("ads_packages_id", db.Integer, db.ForeignKey("ads_packages.id")),
    db.Column("ads_id", db.Integer, db.ForeignKey("ads.id")),
)


class Ads(SearchableMixin, db.Model):
    __searchable__ = ["brand_name", "status"]
    __tablename__ = "ads"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(
        db.String(36), nullable=False, unique=True, default=lambda: str(uuid4().hex)
    )
    property_type = db.Column(db.Enum(PropertyType))
    brand_name = db.Column(db.String(64), nullable=False)
    gender_of_occupant = db.Column(db.Enum(GenderOfOccupant))
    number_bedroom_available = db.Column(db.Integer, nullable=False)
    type_of_bathroom = db.Column(db.Enum(TypeOfBathroom))
    number_of_bathroom = db.Column(db.Integer, nullable=False)
    location_id = db.Column(db.Integer, db.ForeignKey("location.id"), nullable=False)
    address = db.Column(db.Text, nullable=False)
    amenities = db.relationship(
        "Amenities",
        secondary=amenities_ads,
        backref=db.backref("amenities", lazy="dynamic"),
    )
    rental_price = db.Column(db.Float, nullable=False)
    duration_of_ease = db.Column(db.Enum(DurationOfEase))
    mobile_number = db.Column(db.String(18), nullable=False)
    whatsapp_number = db.Column(db.String(18), nullable=False)
    additional_info = db.Column(db.Text, nullable=True)
    ads_packages = db.relationship(
        "AdsPackages",
        secondary=ads_ads_packages,
        backref=db.backref("ads_packages", lazy="dynamic"),
    )
    email = db.Column(db.String(64), nullable=False)
    feedback = db.Column(db.Text, nullable=True)
    accept_policy = db.Column(db.Boolean, nullable=False)
    status = db.Column(db.Boolean, default=False)
    remaining_time = db.Column(db.Integer, nullable=False)
    ads_cost = db.Column(db.Float, nullable=False)
    start_date = db.Column(db.DateTime, nullable=True)
    end_date = db.Column(db.DateTime, nullable=True)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    transaction = db.relationship("Transaction", backref=db.backref("ads", lazy=True))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)

    def __repr__(self):
        return f"<public_id:{self.public_id}, name:{self.brand_name}, type:{self.property_type.value}>"

    def is_active(self):
        return self.status

    def activate_ads(self):
        if not self.status:
            self.status = True
            self.updated_at = datetime.now()
            db.session.commit()
            return True
        return False

    def deactivate_ads(self):
        if self.status:
            self.status = False
            self.updated_at = datetime.now()
            db.session.commit()
            return True
        return False

    # run every 1 day
    @classmethod
    def job_subtraction_ads_remaining_time(cls):
        """
        Update the remaining time of ads with :
        1. query all ads data where have :remaining_time
        2. if ads data exist, for data
        3. subtract :end_date with datetime.now(), save in :time_delta
        4. assign new values for :remaining_time with seconds of :time_delta
        5. add data to db transaction
        6. commit transaction
        """
        current_app.logger.info("Job subtraction_ads_remaining_time Executed.")

        # ambil data hanya :ads yang memiliki remaining time
        ads = cls.query.filter(cls.remaining_time != 0).all()

        # jika :ads ditemukan
        if ads:

            # lakukan perulangan data
            for data in ads:

                # hitung waktu :end_date - waktu hari ini
                remaining_time = data.end_date - datetime.now()

                # jika :remaining_time = 0 atau < 0
                if remaining_time.days <= 0:

                    # maka, non aktifkan :ads
                    data.status = False

                # perbaharui :remaining_time dari hasil perhitungan
                data.remaining_time = remaining_time.seconds

                # perbaharui :updated_at dengan waktu saat ini
                data.updated_at = datetime.now()
                db.session.commit()
                return current_app.logger.info(
                    f"ads_id: {data.public_id}, remaining_time: {remaining_time.days}"
                )
            return current_app.logger.info(
                f"Job subtraction_ads_remaining_time Finish. Job has updated {len(ads)} data."
            )
        # jika :ads tidak ditemukan
        else:
            return current_app.logger.info("No ads data updated.")

    # run every 1 day
    @classmethod
    def job_change_status(cls):
        """
        Merubah status iklan dari aktif menjadi tidak aktif adalah dengan perhitungan jika
        1. :end_date <= waktu hari ini
        2. :remaining_time <= 0
        """
        current_app.logger.info("Job check_ads_end_date Executed")

        # ambil data hanya :ads yang memiliki :status True dan :end_date <= hari ini
        ads = cls.query.filter(cls.status is True, cls.end_date <= datetime.now()).all()

        # jika :ads ditemukan
        if ads:

            # lakukan perulangan data dan ubah status :ads menjadi False, sehingga
            # :ads tidak lagi tampil di halaman utama
            for data in ads:
                data.status = False
                data.updated_at = datetime.now()
                db.session.commit()
                return current_app.logger.info(f"Disable ads with id : {data.public_id}")
            return current_app.logger.info("Job check_ads_end_date Finish")

        # jika :ads tidak ditemukan
        else:
            return current_app.logger.info("No ads data updated.")

    @classmethod
    def get_all(cls, status=None, user=None, page=None):
        """
        Documentation : https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.BaseQuery.paginate
        """
        if status is None:
            return cls.query.paginate(
                page=page,
                per_page=current_app.config["POST_PER_PAGE"],
                error_out=False,
                max_per_page=current_app.config["MAX_PER_PAGE"],
            )
        elif status or not status:
            if user:
                return cls.query.filter_by(status=status, user_id=user.id).paginate(
                    page=page,
                    per_page=current_app.config["POST_PER_PAGE"],
                    error_out=False,
                    max_per_page=current_app.config["MAX_PER_PAGE"],
                )
            else:
                return cls.query.filter_by(status=status).paginate(
                    page=page,
                    per_page=current_app.config["POST_PER_PAGE"],
                    error_out=False,
                    max_per_page=current_app.config["MAX_PER_PAGE"],
                )

    @classmethod
    def find_by_public_id(cls, public_id):
        return cls.query.filter_by(public_id=public_id).first_or_404()

    @classmethod
    def find_by_brand_name(cls, brand_name, status=None):
        return cls.query.filter_by(brand_name=brand_name, status=status).first()

    @classmethod
    def find_by_email(cls, email, status=None):
        return cls.query.filter_by(email=email, status=status).first()

    @classmethod
    def find_by_mobile_number(cls, mobile_number, status=None):
        return cls.query.filter_by(mobile_number=mobile_number, status=status).first()

    @classmethod
    def find_by_whatsapp_number(cls, whatsapp_number, status=None):
        return cls.query.filter_by(whatsapp_number=whatsapp_number, status=status).first()

    @classmethod
    def find_by_property_type(cls, property_type, status=None, page=None):
        return cls.query.filter_by(property_type=property_type, status=status).paginate(
            page=page,
            per_page=current_app.config["POST_PER_PAGE"],
            error_out=False,
            max_per_page=current_app.config["MAX_PER_PAGE"],
        )

    @classmethod
    def find_by_gender_of_occupant(cls, gender_of_occupant, status=None, page=None):
        return cls.query.filter_by(
            gender_of_occupant=gender_of_occupant, status=status
        ).paginate(
            page=page,
            per_page=current_app.config["POST_PER_PAGE"],
            error_out=False,
            max_per_page=current_app.config["MAX_PER_PAGE"],
        )

    @classmethod
    def find_by_type_of_bathroom(cls, type_of_bathroom, status, page=None):
        return cls.query.filter_by(
            type_of_bathroom=type_of_bathroom, status=status
        ).paginate(
            page=page,
            per_page=current_app.config["POST_PER_PAGE"],
            error_out=False,
            max_per_page=current_app.config["MAX_PER_PAGE"],
        )

    @classmethod
    def find_by_location_close_to(cls, location_id, ignore_id, status, page=None):
        return cls.query.filter(
            cls.location_id == location_id,
            cls.public_id != ignore_id,
            cls.status == status,
        ).paginate(
            page=page,
            per_page=current_app.config["POST_PER_PAGE"],
            error_out=False,
            max_per_page=current_app.config["MAX_PER_PAGE"],
        )

    @classmethod
    def find_by_duration_of_ease(cls, duration_of_ease, status, page=None):
        return cls.query.filter_by(
            duration_of_ease=duration_of_ease, status=status
        ).paginate(
            page=page,
            per_page=current_app.config["POST_PER_PAGE"],
            error_out=False,
            max_per_page=current_app.config["MAX_PER_PAGE"],
        )

    @classmethod
    def count(cls, status=None):
        if status is None:
            return cls.query.count()
        else:
            return cls.query.filter_by(status=status).count()

    def create_ads(self):
        db.session.add(self)
        db.session.commit()

    def update_ads(self):
        db.session.commit()

    def delete_ads(self):
        db.session.delete(self)
        db.session.commit()
