"""Class definition for User model."""
from hashlib import md5
from flask import current_app
from sqlalchemy.orm import backref
from src.flask_amikos.extensions import db, fsqla
from src.flask_amikos.util.models import SearchableMixin


class User(SearchableMixin, db.Model, fsqla.FsUserMixin):
    __searchable__ = ["email"]
    pass
    first_name = db.Column(db.String(16), nullable=True)
    last_name = db.Column(db.String(16), nullable=True)
    ads = db.relationship("Ads", backref=db.backref("user", lazy=True))

    # def __init__(self, **kwargs):
    #    """add default role to user"""
    #    super(User, self).__init__(**kwargs)
    #    if self.roles == [] or self.roles is None:
    #        # belum solve
    #        self.roles = "SuperUser"

    def avatar(self, size):
        """func to create avatar based on email"""
        digest = md5(self.email.lower().encode("utf-8")).hexdigest()
        return "https://www.gravatar.com/avatar/{}?id=identicon&s={}".format(digest, size)

    @classmethod
    def get_all(cls, active=None, page=None):
        """func to gets all data for users"""
        if active is None:
            return cls.query.paginate(
                page=page,
                per_page=current_app.config["POST_PER_PAGE"],
                error_out=False,
                max_per_page=current_app.config["MAX_PER_PAGE"],
            )
        else:
            return cls.query.filter_by(active=active).paginate(
                page=page,
                per_page=current_app.config["POST_PER_PAGE"],
                error_out=False,
                max_per_page=current_app.config["MAX_PER_PAGE"],
            )

    @classmethod
    def get(cls, id):
        return cls.query.filter_by(id=id).first_or_404()

    @classmethod
    def count(cls, active=None):
        if active is None:
            return cls.query.count()
        else:
            return cls.query.filter_by(active=active).count()

    def update_user(self):
        db.session.commit()
