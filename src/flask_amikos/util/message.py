from flask_babel import _

_default_messages = {
    "ACTIVATE_USER": (_("User active status has been change"), "success"),
    "CHANGE_STATUS": (_("status has been change"), "success"),
    "SUCCESS": (_("Operation has been success"), "success"),
    "NOT_FOUND": (_("the data you requested is not available or has changed"), "danger"),
}
