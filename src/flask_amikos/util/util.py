import hashlib
import requests
import base64
from flask import current_app
from src.flask_amikos.models.transaction import PaymentMethod


def remove_unused_char(param):
    char_list = ["-", "_", ","]
    for c in char_list:
        param = param.replace(c, "")
    return param


def day_to_second(param):
    return param * 24 * 3600


def second_to_day(param):
    return param / 3600 / 24


def midtrans_verify_signature_key(order_id, status_code, gross_amount):
    signature_key = hashlib.sha512(
        str(
            order_id
            + status_code
            + gross_amount
            + current_app.config["MIDTRANS_SERVER_KEY"]
        ).encode("utf-8")
    ).hexdigest()
    return signature_key


def get_payment_method_value(payment_method_name):
    for payment_name in PaymentMethod.get():
        if payment_method_name in payment_name:
            return payment_name.value


def check_status_midtrans(parameter_id):
    # create token to send in headers
    token = base64.b64encode(
        bytes(current_app.config["MIDTRANS_SERVER_KEY"] + ":", "utf-8")
    )

    # set url of midtrans api
    if current_app.config["MIDTRANS_PRODUCTION"]:
        url = f"https://api.midtrans.com/v2/{parameter_id}/status"
    else:
        url = f"https://api.sandbox.midtrans.com/v2/{parameter_id}/status"

    # set headers
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Basic " + token.decode("utf-8"),
    }

    # make request
    r = requests.get(url=url, headers=headers)

    # the response of request
    response = r.json()

    return response
