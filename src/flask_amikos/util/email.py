from threading import Thread
from flask import current_app, render_template
from flask_mail import Message
from src.flask_amikos import mail

"""Email util for send email using celery"""

""" from src.flask_amikos.util.tasks import celery """
""" from flask_security import MailUtil
from src.flask_amikos.util.tasks import send_flask_mail


class MyMailUtil(MailUtil):
    def send_mail(self, template, subject, recipient, sender, body, html, user, **kwargs):
        send_flask_mail.delay(
            subject=subject,
            sender=sender,
            recipients=recipient,
            body=body,
            html=html,
        )
 """


def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(to, subject, template, **kwargs):
    msg = Message(
        current_app.config["APP_NAME"] + " - " + subject,
        recipients=[to],
    )
    msg.body = render_template(template + ".txt", **kwargs)
    msg.html = render_template(template + ".html", **kwargs)
    Thread(target=send_async_email, args=(current_app._get_current_object(), msg)).start()
