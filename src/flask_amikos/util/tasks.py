"""Tasks util for using celery task"""
""" from celery import Celery
from src.flask_amikos.config import Config


celery = Celery(
    __name__,
    backend=Config.CELERY_RESULT_BACKEND,
    broker=Config.CELERY_BROKER_URL,
) """
""" 

from flask_mail import Message
from src.flask_amikos.extensions import mail
from src.flask_amikos import make_celery

celery = make_celery


@celery.task
def send_flask_mail(**kwargs):
    mail.send(Message(**kwargs))


@celery.task
def add_together(a, b):
    return a + b """
from src.flask_amikos.models.ads import Ads


def ads():
    """
    function jobs for ads module
    """
    Ads.job_subtraction_ads_remaining_time()
    Ads.job_change_status()

    return print("Ads Jobs Executed")
