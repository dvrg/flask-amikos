"""Flask app initialization via factory pattern."""
import sentry_sdk
import logging
from flask import Flask, url_for
from flask_security import SQLAlchemyUserDatastore
from flask_security.signals import user_registered
from werkzeug.middleware.proxy_fix import ProxyFix
from werkzeug.middleware.profiler import ProfilerMiddleware
from flask_babel import format_currency
from elasticsearch import Elasticsearch
from sentry_sdk.integrations.flask import FlaskIntegration
from flask_breadcrumbs import default_breadcrumb_root
from src.flask_amikos.config import get_config
from src.flask_amikos.extensions import (
    babel,
    db,
    mail,
    migrate,
    security,
    toolbar,
    moment,
    menu,
    breadcrumbs,
    profiler,
    talisman,
    csrf,
    scheduler,
)
from src.flask_amikos.models.role import Role
from src.flask_amikos.models.user import User
from src.flask_amikos.forms.front.user import (
    ExtendedRegisterFormFront,
    ExtendedConfirmRegisterFormFront,
)
from src.flask_amikos.util.util import second_to_day
from src.flask_amikos.util.tasks import ads


""" from src.flask_amikos.util.email import MyMailUtil """

""" from celery import Celery """

user_datastore = SQLAlchemyUserDatastore(db, User, Role)


def create_app(config_name):
    """
    Create app
    Docs for Flask-Talisman : https://github.com/GoogleCloudPlatform/flask-talisman
    Docs for Flask-Profiler : https://github.com/muatik/flask-profiler#using-with-different-database-system
    Docs for Flask-APSchedular : https://viniciuschiele.github.io/flask-apscheduler/index.html
    """
    app = Flask(__name__, template_folder="templates", static_folder="static")
    app.config.from_object(get_config(config_name))

    sentry_sdk.init(
        dsn=app.config["SENTRY_DSN"],
        integrations=[FlaskIntegration()],
        traces_sample_rate=app.config["TRACE_SAMPLE_RATE"],
    )

    app.elasticsearch = (
        Elasticsearch(
            [app.config["ELASTICSEARCH_URL"]],
            http_auth=(
                app.config["ELASTICSEARCH_USERNAME"],
                app.config["ELASTICSEARCH_PASSWORD"],
            ),
        )
        if app.config["ELASTICSEARCH_URL"]
        else None
    )

    app.config["flask_profiler"] = {
        "enabled": app.config["DEBUG"],
        "storage": {
            "engine": "sqlalchemy",
            "db_url": app.config["FLASK_PROFILER_DATABASE_URL"],
        },
        "basicAuth": {
            "enabled": True,
            "username": app.config["FLASK_PROFILER_USERNAME"],
            "password": app.config["FLASK_PROFILER_PASSWORD"],
        },
        "ignore": ["^/static/.*"],
        "endpointRoot": app.config["FLASK_PROFILER_ENDPOINT"],
    }

    @scheduler.authenticate
    def authenticate(auth):
        """Check auth."""
        return (
            auth["username"] == app.config["FLASK_APPSCHEDULER_USERNAME"]
            and auth["password"] == app.config["FLASK_APPSCHEDULER_PASSWORD"]
        )

    register_extensions(app)
    register_all_blueprint(app)
    register_context_processor(app)
    register_custome_url(app)

    """ make_celery(app) """

    return app


def register_extensions(app):
    """Register all extension here"""
    db.init_app(app)
    migrate.init_app(app, db)
    mail.init_app(app)
    babel.init_app(app)
    toolbar.init_app(app)
    security.init_app(
        app,
        user_datastore,
        register_form=ExtendedRegisterFormFront,
        confirm_register_form=ExtendedConfirmRegisterFormFront,
    )
    moment.init_app(app)
    ProxyFix(app, x_for=1)
    menu.init_app(app)
    breadcrumbs.init_app(app)
    ProfilerMiddleware(app.wsgi_app, restrictions=25, profile_dir="src/flask_amikos/")
    profiler.init_app(app)
    talisman.init_app(
        app,
        content_security_policy=app.config["CSP"],
    )
    csrf.init_app(app)
    scheduler.init_app(app)
    scheduler.add_job(id="ads", func=ads, trigger="cron", day="*")
    scheduler.start()


def register_custome_signal_handler(app):
    """Function to edit signal handler from FS"""

    @user_registered.connect_via(app)
    def user_registered_sighandler(sender, **extra):
        # Add additional fields to new user - "SuperUser" role
        logging.getLogger(__name__).debug("register handler: %s", extra)
        user = extra.get("user")
        sender.db.add_role_to_user(user, "SuperUser")
        sender.db.commit()


def register_all_blueprint(app):
    """Register all blueprint/module here"""
    from src.flask_amikos.routes.back import back
    from src.flask_amikos.routes.front import front

    app.register_blueprint(back)
    app.register_blueprint(front)

    default_breadcrumb_root(front, ".back")


def register_context_processor(app):
    """Register context processor here"""

    @app.context_processor
    def default_app_context_processor():
        return dict(
            app=app.config["APP_NAME"],
            app_ver=app.config["APP_VERSION"],
            app_author=app.config["APP_AUTHOR_PROFILE"],
            app_logo_src=url_for("static", filename="img/front/logo-blue-black.svg"),
            app_favicon_16_src=url_for(
                "static", filename="img/front/favicon/favicon-16x16.png"
            ),
            app_favicon_32_src=url_for(
                "static", filename="img/front/favicon/favicon-32x32.png"
            ),
            app_favicon_apple_src=url_for(
                "static", filename="img/front/favicon/apple-touch-icon.png"
            ),
            app_site_webmanifest_src=url_for(
                "static", filename="img/front/favicon/site.webmanifest"
            ),
            app_link_src=app.config["APP_SOURCE_CODE"],
            app_whatsapp=app.config["APP_WHATSAPP"],
            app_twitter=app.config["APP_TWITTER"],
            app_facebook=app.config["APP_FACEBOOK"],
            app_instagram=app.config["APP_INSTAGRAM"],
            flask_profiler_url=app.config["FLASK_PROFILER_ENDPOINT"],
            flask_schedular_url="/scheduler/jobs",
        )

    @app.context_processor
    def utility_processor():
        def format_price(number, currency="IDR"):
            """Format all price to IDR"""
            return format_currency(
                number,
                currency,
            )

        def format_duration(param):
            return second_to_day(param)

        return dict(format_price=format_price, format_duration=format_duration)

    @security.context_processor
    def default_security_context_processor():
        return dict(
            app=app.config["APP_NAME"],
            app_ver=app.config["APP_VERSION"],
            app_logo_src=url_for(
                "static", filename="img/front/favicon/favicon-32x32.png"
            ),
            app_link_src=app.config["APP_SOURCE_CODE"],
        )


def register_custome_url(app):
    """register custome url not in module here"""

    """ @babel.localeselector
    def get_locale():
        return request.accept_languages.best_match(app.config["LANGUAGES"])
        # return 'id' """


""" def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config["CELERY_RESULT_BACKEND"],
        broker=app.config["CELERY_BROKER_URL"],
    )

    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
 """
