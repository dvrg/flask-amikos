from flask import request
from flask_wtf import FlaskForm
from wtforms import TextAreaField, BooleanField, SubmitField, StringField
from wtforms.validators import DataRequired, ValidationError
from flask_babel import lazy_gettext as _l
from src.flask_amikos.models.amenities import Amenities


class AmenitiesForm(FlaskForm):
    amenities_name = TextAreaField(_l("amenities name"), validators=[DataRequired()])
    description = TextAreaField(_l("description"))
    status = BooleanField(_l("status"), default="checked", false_values=(False, "false"))


class CreateAmenitiesForm(AmenitiesForm):
    submit = SubmitField(_l("save"))

    def validate_amenities_name(self, amenities_name):
        amenities_name = Amenities.find_by_amenities_name(
            amenities_name=self.amenities_name.data
        )
        if amenities_name is not None:
            raise ValidationError(
                _l(
                    "Amenities name {} already available. Please use another name".format(
                        self.amenities_name.data
                    )
                )
            )


class UpdateAmenitiesForm(AmenitiesForm):
    submit = SubmitField(_l("update"))

    def __init__(self, original_amenities_name, *args, **kwargs):
        super(UpdateAmenitiesForm, self).__init__(*args, **kwargs)
        self.original_amenities_name = original_amenities_name

    def validate_amenities_name(self, amenities_name):
        if amenities_name.data != self.original_amenities_name:
            amenities_name = Amenities.find_by_amenities_name(
                amenities_name=self.amenities_name.data
            )
        if amenities_name is not None:
            raise ValidationError(
                _l(
                    "Amenities name {} already available. Please use another name".format(
                        self.amenities_name.data
                    )
                )
            )


class SearchAmenitiesForm(FlaskForm):
    query = StringField(_l("Search by Amenities Name"), validators=[DataRequired()])
    submit = SubmitField(_l("Search"))

    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "csrf_enabled" not in kwargs:
            kwargs["csrf_enabled"] = False
        super(SearchAmenitiesForm, self).__init__(*args, **kwargs)
