from flask import request
from flask_wtf import FlaskForm
from flask_security import RegisterForm
from wtforms.fields import StringField, SubmitField
from wtforms.validators import DataRequired
from wtforms_sqlalchemy.fields import QuerySelectField
from flask_babel import lazy_gettext as _l
from src.flask_amikos.models.role import Role
from src.flask_amikos.forms.front.user import ProfileUserFormFront


def get_role():
    pagination = Role.get_all(page=1)
    roles = pagination.items
    return roles


class ExtendedRegisterForm(RegisterForm):
    role = QuerySelectField(
        _l("Role"),
        query_factory=get_role,
        get_label="name",
        get_pk=lambda a: a.name,
        blank_text=_l("Select role"),
        allow_blank=True,
    )


class ProfileUserFormBack(ProfileUserFormFront):
    pass


class SearchUserForm(FlaskForm):
    query = StringField(_l("Search by User Email"), validators=[DataRequired()])
    submit = SubmitField(_l("Search"))

    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "csrf_enabled" not in kwargs:
            kwargs["csrf_enabled"] = False
        super(SearchUserForm, self).__init__(*args, **kwargs)
