from flask import request
from flask_wtf import FlaskForm
from wtforms.fields import BooleanField, SubmitField, StringField
from wtforms.validators import DataRequired, ValidationError
from flask_babel import lazy_gettext as _l
from src.flask_amikos.forms.front.ads import AdsForm
from src.flask_amikos.models.ads import Ads


class ExtendedAdsForm(AdsForm):
    status = BooleanField(_l("ads status"))


class ExtendedUpdateAdsForm(ExtendedAdsForm):
    submit = SubmitField(_l("update"))

    def __init__(
        self,
        original_brand_name,
        original_mobile_number,
        original_whatsapp_number,
        original_email,
        *args,
        **kwargs,
    ):
        super(ExtendedUpdateAdsForm, self).__init__(*args, **kwargs)
        self.original_brand_name = original_brand_name
        self.original_mobile_number = original_mobile_number
        self.original_whatsapp_number = original_whatsapp_number
        self.original_email = original_email

    def validate_brand_name(self, brand_name):
        if brand_name.data != self.original_brand_name:
            brand_name = Ads.find_by_brand_name(brand_name=self.brand_name.data)
            if brand_name is not None:
                raise ValidationError(
                    _l(
                        "Brand name {} already available. Please use another name".format(
                            self.brand_name.data
                        )
                    )
                )

    def validate_mobile_number(self, mobile_number):
        if mobile_number.data != self.original_mobile_number:
            mobile_number = Ads.find_by_mobile_number(
                mobile_number=self.mobile_number.data
            )
            if mobile_number is not None:
                raise ValidationError(
                    _l(
                        "Mobile number {} already available. Please use another name".format(
                            self.mobile_number.data
                        )
                    )
                )

    def validate_whatsapp_number(self, whatsapp_number):
        if whatsapp_number.data != self.original_whatsapp_number:
            whatsapp_number = Ads.find_by_whatsapp_number(
                whatsapp_number=self.whatsapp_number.data
            )
            if whatsapp_number is not None:
                raise ValidationError(
                    _l(
                        "WhatsApp number {} already available. Please use another name".format(
                            self.whatsapp_number.data
                        )
                    )
                )

    def validate_email(self, email):
        if email.data != self.original_email:
            email = Ads.find_by_email(email=self.email.data)
            if email is not None:
                raise ValidationError(
                    _l(
                        "Email {} already available. Please use another name".format(
                            self.email.data
                        )
                    )
                )


class SearchAdsForm(FlaskForm):
    query = StringField(_l("Search by Brand Name"), validators=[DataRequired()])

    submit = SubmitField(_l("Search"))

    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "csrf_enabled" not in kwargs:
            kwargs["csrf_enabled"] = False
        super(SearchAdsForm, self).__init__(*args, **kwargs)
