from flask import request
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField
from wtforms.validators import DataRequired, ValidationError
from flask_babel import lazy_gettext as _l
from src.flask_amikos.models.location import Location


class LocationForm(FlaskForm):
    location_name = StringField(_l("location name"), validators=[DataRequired()])
    status = BooleanField(_l("status"), default="checked", false_values=(False, "false"))


class CreateLocationForm(LocationForm):
    submit = SubmitField(_l("save"))

    def validate_location_name(self, location_name):
        location_name = Location.find_by_location_name(
            location_name=self.location_name.data
        )
        if location_name is not None:
            raise ValidationError(
                _l(
                    "Location name {} already available. Please use another name".format(
                        self.location_name.data
                    )
                )
            )


class UpdateLocationForm(LocationForm):
    submit = SubmitField(_l("update"))

    def __init__(self, original_location_name, *args, **kwargs):
        super(UpdateLocationForm, self).__init__(*args, **kwargs)
        self.original_location_name = original_location_name

    def validate_location_name(self, location_name):
        if location_name.data != self.original_location_name:
            location_name = Location.find_by_location_name(
                location_name=self.location_name.data
            )
        if location_name is not None:
            raise ValidationError(
                _l(
                    "Location name {} already available. Please use another name".format(
                        self.location_name.data
                    )
                )
            )


class SearchLocationForm(FlaskForm):
    query = StringField(_l("Search by Location Name"), validators=[DataRequired()])
    submit = SubmitField(_l("Search"))

    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "csrf_enabled" not in kwargs:
            kwargs["csrf_enabled"] = False
        super(SearchLocationForm, self).__init__(*args, **kwargs)
