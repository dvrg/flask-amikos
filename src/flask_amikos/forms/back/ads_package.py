from flask import request
from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    FloatField,
    TextAreaField,
    BooleanField,
    SubmitField,
    IntegerField,
)
from wtforms.validators import DataRequired, ValidationError
from flask_babel import lazy_gettext as _l
from src.flask_amikos.models.ads_package import AdsPackages


class AdsPackageForm(FlaskForm):
    package_name = StringField(_l("package name"), validators=[DataRequired()])
    price = FloatField(_l("normal price"), validators=[DataRequired()])
    discount_price = FloatField(_l("discount price"), validators=[DataRequired()])
    duration = IntegerField(
        _l("duration in day"),
        validators=[DataRequired()],
    )
    description = TextAreaField(_l("description"), validators=[DataRequired()])
    status = BooleanField(_l("status"), default="checked", false_values=(False, "false"))


class CreateAdsPackageForm(AdsPackageForm):
    submit = SubmitField(_l("save"))

    def validate_package_name(self, package_name):
        package_name = AdsPackages.find_by_package_name(
            package_name=self.package_name.data
        )
        if package_name is not None:
            raise ValidationError(
                _l(
                    "Package name {} already available. Please use another name".format(
                        self.package_name.data
                    )
                )
            )


class UpdateAdsPackageForm(AdsPackageForm):
    submit = SubmitField(_l("update"))

    def __init__(self, original_package_name, *args, **kwargs):
        super(UpdateAdsPackageForm, self).__init__(*args, **kwargs)
        self.original_package_name = original_package_name

    def validate_package_name(self, package_name):
        if package_name.data != self.original_package_name:
            package_name = AdsPackages.find_by_package_name(
                package_name=self.package_name.data
            )
            if package_name is not None:
                raise ValidationError(
                    _l(
                        "Package name {} already available. Please use another name".format(
                            self.package_name.data
                        )
                    )
                )


class SearchAdsPackageForm(FlaskForm):
    query = StringField(_l("Search by Ads Package Name"), validators=[DataRequired()])
    submit = SubmitField(_l("Search"))

    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "csrf_enabled" not in kwargs:
            kwargs["csrf_enabled"] = False
        super(SearchAdsPackageForm, self).__init__(*args, **kwargs)
