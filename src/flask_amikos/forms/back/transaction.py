from flask import request
from flask_wtf import FlaskForm
from wtforms.fields import StringField, SubmitField
from wtforms.validators import DataRequired
from flask_babel import lazy_gettext as _l


class SearchTransactionForm(FlaskForm):
    query = StringField(
        _l("Search by Transaction ID/Paymeny Type/Status"), validators=[DataRequired()]
    )

    submit = SubmitField(_l("Search"))

    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "csrf_enabled" not in kwargs:
            kwargs["csrf_enabled"] = False
        super(SearchTransactionForm, self).__init__(*args, **kwargs)
