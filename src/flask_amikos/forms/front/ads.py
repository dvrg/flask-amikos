from flask import request, Markup
from flask_wtf import FlaskForm, RecaptchaField
from wtforms import (
    StringField,
    IntegerField,
    SelectMultipleField,
    SubmitField,
    BooleanField,
)
from wtforms.validators import DataRequired, ValidationError
from wtforms.widgets import CheckboxInput, ListWidget, TextArea
from wtforms.widgets.core import html_params
from wtforms.widgets import NumberInput, TelInput, EmailInput
from wtforms_sqlalchemy.fields import QueryCheckboxField, QuerySelectField
from flask_babel import lazy_gettext as _l, format_currency
from src.flask_amikos.models.ads import (
    PropertyType,
    GenderOfOccupant,
    TypeOfBathroom,
    DurationOfEase,
)
from src.flask_amikos.models.ads import Ads
from src.flask_amikos.models.ads_package import AdsPackages
from src.flask_amikos.models.amenities import Amenities
from src.flask_amikos.models.location import Location


def get_ads_packages():
    pagination = AdsPackages.get_all(status=True)
    ads_packages = pagination.items
    return ads_packages


def get_amenities():
    pagination = Amenities.get_all(status=True)
    amenities = pagination.items
    return amenities


def get_location():
    pagination = Location.get_all(status=True)
    location = pagination.items
    return location


class MyListWidget(ListWidget):
    def __call__(self, field, **kwargs):
        kwargs.setdefault("id", field.id)

        # Grab the li_class keyword passed in as a kwarg.
        # Popping it so that it's not passed in when rendering ul/ol tag.
        li_class = kwargs.pop("li_class") if "li_class" in kwargs else None

        html = ["<{} {}>".format(self.html_tag, html_params(**kwargs))]
        for subfield in field:
            if self.prefix_label:
                # Jamming in li_class here. Can be prettier!
                html.append(
                    f"<li{' '+li_class if li_class else ''}>{subfield.label} {subfield()}</li>"
                )
            else:
                html.append(
                    f"<li{' '+li_class if li_class else ''}>{subfield()} {subfield.label}</li>"
                )
        html.append("</%s>" % self.html_tag)
        return Markup("".join(html))


class MultiCheckboxField(SelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    Iterating the field will produce subfields, allowing custom rendering of
    the enclosed checkbox fields.

    https://wtforms.readthedocs.io/en/2.3.x/specific_problems/?highlight=checkbox#specialty-field-tricks
    """

    widget = ListWidget(prefix_label=False)
    option_widget = CheckboxInput()


class AdsForm(FlaskForm):
    property_type = QuerySelectField(
        _l("Property Type"),
        query_factory=lambda: PropertyType.get(),
        get_label=lambda field: field.value,
        get_pk=lambda field: field.name,
        validators=[DataRequired()],
    )
    brand_name = StringField(_l("Brand Name"), validators=[DataRequired()])
    gender_of_occupant = QuerySelectField(
        _l("Gender of Occupant"),
        query_factory=lambda: GenderOfOccupant.get(),
        get_label=lambda field: field.value,
        get_pk=lambda field: field.name,
        validators=[DataRequired()],
    )
    number_bedroom_available = IntegerField(
        _l("Number of Bedroom"),
        validators=[DataRequired()],
        widget=NumberInput(),
    )
    type_of_bathroom = QuerySelectField(
        _l("Type of Bathroom"),
        query_factory=lambda: TypeOfBathroom.get(),
        get_label=lambda field: field.value,
        get_pk=lambda field: field.name,
        validators=[DataRequired()],
    )
    number_of_bathroom = IntegerField(
        _l("Number of Bathroom"),
        validators=[DataRequired()],
        widget=NumberInput(),
    )
    location_close_to = QuerySelectField(
        _l("Location Close To"),
        query_factory=lambda: get_location(),
        get_label=lambda field: field.location_name,
        get_pk=lambda field: field.public_id,
        validators=[DataRequired()],
    )
    address = StringField(_l("Address"), validators=[DataRequired()])
    amenities = QueryCheckboxField(
        _l("Amenities"),
        query_factory=lambda: get_amenities(),
        get_label=lambda field: Markup(field.amenities_name),
        get_pk=lambda field: field.public_id,
        validators=[DataRequired()],
    )
    rental_price = StringField(_l("Rental Price"), validators=[DataRequired()])
    duration_of_ease = QuerySelectField(
        _l("Duration of Ease"),
        query_factory=lambda: DurationOfEase.get(),
        get_label=lambda field: field.value,
        get_pk=lambda field: field.name,
        validators=[DataRequired()],
    )
    mobile_number = StringField(
        _l("Mobile Number"), validators=[DataRequired()], widget=TelInput()
    )
    whatsapp_number = StringField(_l("WhatsApp Number"), widget=TelInput())
    additional_info = StringField(_l("Additional Information"), widget=TextArea())
    ads_packages = QueryCheckboxField(
        _l("Ads Package"),
        query_factory=lambda: get_ads_packages(),
        get_label=lambda field: Markup(
            field.description
            + " <s>"
            + format_currency(field.price, currency="IDR")
            + "</s> "
            + "<strong>"
            + format_currency(field.discount_price, currency="IDR")
            + "</strong>"
        ),
        get_pk=lambda field: field.id,
        validators=[DataRequired()],
    )
    email = StringField(
        _l("Email"),
        validators=[DataRequired()],
        widget=EmailInput(),
        render_kw={"readonly": True},
    )
    feedback = StringField(_l("Feedback to Us"), widget=TextArea())
    accept_ads_policy = BooleanField(
        _l("📜 I have read the detailed description of the ad package"),
        widget=CheckboxInput(),
    )
    accept_privacy_policy = BooleanField(
        _l("📜 I agree to the terms & conditions of use"),
        widget=CheckboxInput(),
        validators=[DataRequired()],
    )
    accept_policy = MultiCheckboxField(
        choices=[
            ("1", _l("📜 I have read the detailed description of the ad package")),
            (
                "2",
                _l("📜 I agree to the terms & conditions of use"),
            ),
        ],
    )


class CreateAdsForm(AdsForm):
    recaptcha = RecaptchaField()
    submit = SubmitField(_l("placing ad"))

    def validate_brand_name(self, brand_name):
        brand_name = Ads.find_by_brand_name(brand_name=self.brand_name.data)
        if brand_name is not None:
            raise ValidationError(
                _l(
                    "Brand name {} already available. Please use another name".format(
                        self.brand_name.data
                    )
                )
            )

    def validate_mobile_number(self, mobile_number):
        mobile_number = Ads.find_by_mobile_number(mobile_number=self.mobile_number.data)
        if mobile_number is not None:
            raise ValidationError(
                _l(
                    "Mobile number {} already available. Please use another name".format(
                        self.mobile_number.data
                    )
                )
            )

    def validate_whatsapp_number(self, whatsapp_number):
        whatsapp_number = Ads.find_by_whatsapp_number(
            whatsapp_number=self.whatsapp_number.data
        )
        if whatsapp_number is not None:
            raise ValidationError(
                _l(
                    "WhatsApp number {} already available. Please use another name".format(
                        self.whatsapp_number.data
                    )
                )
            )

    def validate_email(self, email):
        email = Ads.find_by_email(email=self.email.data)
        if email is not None:
            raise ValidationError(
                _l(
                    "Email {} already available. Please use another name".format(
                        self.email.data
                    )
                )
            )


class UpdateAdsForm(AdsForm):
    submit = SubmitField(_l("update"))

    def __init__(
        self,
        original_brand_name,
        original_mobile_number,
        original_whatsapp_number,
        original_email,
        *args,
        **kwargs,
    ):
        super(UpdateAdsForm, self).__init__(*args, **kwargs)
        self.original_brand_name = original_brand_name
        self.original_mobile_number = original_mobile_number
        self.original_whatsapp_number = original_whatsapp_number
        self.original_email = original_email

    def validate_brand_name(self, brand_name):
        if brand_name.data != self.original_brand_name:
            brand_name = Ads.find_by_brand_name(brand_name=self.brand_name.data)
            if brand_name is not None:
                raise ValidationError(
                    _l(
                        "Brand name {} already available. Please use another name".format(
                            self.brand_name.data
                        )
                    )
                )

    def validate_mobile_number(self, mobile_number):
        if mobile_number.data != self.original_mobile_number:
            mobile_number = Ads.find_by_mobile_number(
                mobile_number=self.mobile_number.data
            )
            if mobile_number is not None:
                raise ValidationError(
                    _l(
                        "Mobile number {} already available. Please use another name".format(
                            self.mobile_number.data
                        )
                    )
                )

    def validate_whatsapp_number(self, whatsapp_number):
        if whatsapp_number.data != self.original_whatsapp_number:
            whatsapp_number = Ads.find_by_whatsapp_number(
                whatsapp_number=self.whatsapp_number.data
            )
            if whatsapp_number is not None:
                raise ValidationError(
                    _l(
                        "WhatsApp number {} already available. Please use another name".format(
                            self.whatsapp_number.data
                        )
                    )
                )

    def validate_email(self, email):
        if email.data != self.original_email:
            email = Ads.find_by_email(email=self.email.data)
            if email is not None:
                raise ValidationError(
                    _l(
                        "Email {} already available. Please use another name".format(
                            self.email.data
                        )
                    )
                )


class SearchAdsForm(FlaskForm):
    query = StringField(_l("Search by Brand Name"), validators=[DataRequired()])

    submit = SubmitField(_l("Search"))

    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "csrf_enabled" not in kwargs:
            kwargs["csrf_enabled"] = False
        super(SearchAdsForm, self).__init__(*args, **kwargs)
