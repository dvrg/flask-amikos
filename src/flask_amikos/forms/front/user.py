from flask_wtf import FlaskForm
from flask_security import RegisterForm, ConfirmRegisterForm
from wtforms.fields import StringField, SubmitField
from wtforms.validators import DataRequired
from flask_babel import lazy_gettext as _l


class ExtendedRegisterFormFront(RegisterForm):
    first_name = StringField(_l("First Name"), [DataRequired()])
    last_name = StringField(_l("Last Name"), [DataRequired()])


class ExtendedConfirmRegisterFormFront(ConfirmRegisterForm):
    first_name = StringField(_l("First Name"), [DataRequired()])
    last_name = StringField(_l("Last Name"), [DataRequired()])


class ProfileUserFormFront(FlaskForm):
    first_name = StringField(_l("First Name"), [DataRequired()])
    last_name = StringField(_l("Last Name"), [DataRequired()])
    email = StringField(
        _l("Email Address"), [DataRequired()], render_kw={"readonly": True}
    )
    submit = SubmitField(_l("💾 Save Changes"))
